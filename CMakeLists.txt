cmake_minimum_required(VERSION 2.6)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMakeModules/")

set(RESSOURCE_PATH ${CMAKE_SOURCE_DIR}/ressources)

configure_file(
	${CMAKE_SOURCE_DIR}/headers/gui/config.h.cmake
	${CMAKE_SOURCE_DIR}/headers/gui/config.h
)

add_definitions(
    -std=c99
    -W -Wall -Wextra -Wparentheses -Wshadow -Wstrict-prototypes -Wpointer-arith -Wcast-qual -Winline
    -g -O2
    -D_USE_MATH_DEFINES
)

project(caracassonne)

add_subdirectory(documentation)
add_subdirectory(src)
add_subdirectory(src/tad)
add_subdirectory(src/gui)
add_subdirectory(test)
