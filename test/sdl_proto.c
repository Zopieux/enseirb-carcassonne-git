#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_opengl.h>
#include <SDL/SDL_ttf.h>
// #include <GL/gl.h>
// #include <GL/glu.h>
#include <math.h>

#define WIN_WIDTH 1196
#define WIN_HEIGHT 800
#define INTERFACE_HEIGHT 416

#define GRID_RESOLUTION 100

#define RESSOURCE_PATH "../ressources/"
#define SPRITE_COUNT 24
#define SPRITE_COUNT_BY_LINE 12
#define SPRITE_NB_LINES SPRITE_COUNT / SPRITE_COUNT_BY_LINE
#define MAX_TILES 72

#define CAMERA_SCROLL_MARGIN 50
#define CAMERA_SCROLL_ACCELERATION 0.6

#define FRAMERATE 30.0


enum Textures {TEXTURE_TILES, TEXTURE_INTERFACE, TEXTURE_LINE, TEXTURE_LAST};
// enum Land {FIELD_};

typedef struct Position_s {
	int x;
	int y;
} Position;

typedef struct Tile_s {
	int spriteno;
	Position* pos;
	int rotation;
	struct Tile_s* neighboors[4];
	enum Field fields[4];
} Tile;



void draw_image(int width, int height, float texture_tl, float texture_tr, float texture_bl, float texture_br) {

	glBegin( GL_QUADS );

		glTexCoord2f( texture_tl , texture_bl );
		glVertex2i( 0, 0 );

		glTexCoord2f( texture_tr, texture_bl );
		glVertex2i( width, 0);

		glTexCoord2f( texture_tr, texture_br );
		glVertex2i( width, height);

		glTexCoord2f( texture_tl, texture_br );
		glVertex2i( 0, height );


	glEnd();

}


void position_img(int x, int y, int spriteno, int width, int height, int angle) {
	int line = spriteno / SPRITE_COUNT_BY_LINE;

	float \
		text_tl   = (float)(spriteno % SPRITE_COUNT_BY_LINE) / (float)(SPRITE_COUNT_BY_LINE),
		text_tr = (float)((spriteno % SPRITE_COUNT_BY_LINE) + 1) / (float)(SPRITE_COUNT_BY_LINE),
		text_bl   = (float)(line) / (float)(SPRITE_NB_LINES),
		text_br = (float)(line + 1) / (float)(SPRITE_NB_LINES);

	glPushMatrix();
	glLoadIdentity();

	glTranslatef (x + width/2, y + height/2, 0);
	if(angle != 0)
		glRotatef (angle, 0, 0, 1);
	glTranslatef (-width/2, -height/2, 0);

	draw_image(width, height, text_tl, text_tr, text_bl, text_br);

	glPopMatrix();
}


void load_ressource_file(SDL_Surface* surfarray[], GLuint* textures, int textureno, int transparency,  char* fname) {
	SDL_Surface* surface = IMG_Load(fname);
	surfarray[textureno] = surface;

	// Bind the texture object
	glBindTexture( GL_TEXTURE_2D, textures[textureno] );

	// Set the texture's stretching properties
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

	// Edit the texture object's image data using the information SDL_Surface gives us
	if(transparency)
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0,
				  GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels );

	else
		glTexImage2D( GL_TEXTURE_2D, 0, 3, surface->w, surface->h, 0,
				  GL_RGB, GL_UNSIGNED_BYTE, surface->pixels );

	// Free the SDL_Surface only if it was successfully created
	if ( surface ) {
		SDL_FreeSurface( surface );
	}

}


int main(int argc, char *argv[]) {
	SDL_Surface *screen, *message;

	SDL_Rect camera = { 0.0, 0.0, WIN_WIDTH, WIN_HEIGHT };

	// Have OpenGL generate a texture object handle for us
	GLuint textures[TEXTURE_LAST];
	glGenTextures( TEXTURE_LAST, textures );

	SDL_Surface* surfaces[TEXTURE_LAST];

	DisplayedTile displayed_tiles[MAX_TILES];
	Position* available_pos[1000];
	available_pos_count = 0;

	unsigned int display_tiles_num = 0;

	// Slightly different SDL initialization
	if ( SDL_Init(SDL_INIT_VIDEO) != 0 ) {
		printf("Unable to initialize SDL: %s\n", SDL_GetError());
		return 1;
	}

	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 ); // *new*

	screen = SDL_SetVideoMode( WIN_WIDTH, WIN_HEIGHT, 16, SDL_OPENGL /*| SDL_RESIZABLE*/ ); // *changed*
	if ( !screen ) {
		printf("Unable to set video mode: %s\n", SDL_GetError());
		return 1;
	}

	// Set the OpenGL state after creating the context with SDL_SetVideoMode

	glClearColor( 0, 0, 0, 0 );

	glEnable( GL_TEXTURE_2D ); // Need this to display a texture
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	int vpx, vpy;
	vpx = 0; vpy = 0;
	glViewport( vpx, vpy, WIN_WIDTH, WIN_HEIGHT );

	// Load the OpenGL texture
	load_ressource_file(surfaces, textures, TEXTURE_TILES, 0, RESSOURCE_PATH "tileset.jpg");
	load_ressource_file(surfaces, textures, TEXTURE_INTERFACE, 1, RESSOURCE_PATH "interface.png");
	load_ressource_file(surfaces, textures, TEXTURE_LINE, 1, RESSOURCE_PATH "line_marker.png");

	// Load fonts
	SDL_Color textColor = { 255, 20, 20 };
	TTF_Font *font = TTF_OpenFont( "/usr/share/fonts/truetype/ttf-dejavu/DejaVuSansCondensed.ttf", 12 );

	SDL_Event event;

	int i, j, myx, myy, myrotate, mysprite;
	Uint8 *keys;
	long tick;
	keys = SDL_GetKeyState(&i);
	tick = SDL_GetTicks();
	int do_exit = 0;

	myx = 0; myy = 0;
	mysprite = 0;
	myrotate = 0;

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	glOrtho( 0, WIN_WIDTH, WIN_HEIGHT, 0, -1, 1 );

	glMatrixMode( GL_MODELVIEW );
	// glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	int addblock;
	int camera_moving = 0;
	int camera_move_x, camera_move_y;
	float camera_z = 1;
	float cam_velo_x, cam_velo_y, velo_sign;

	unsigned int frames = 0;
	int startTimer, endTimer, deltaTime, globalStartTimer = SDL_GetTicks();
	char fpsbuff[50];

	// int offset_marker_x[4] = {0, TILE_SIZE + 30, TILE_SIZE, -30};
	int offset_marker[4] = {-30, 0, TILE_SIZE + 30, TILE_SIZE};

	// TEST: add a first piece
	DisplayedTile dt, dtsearch;
	Position* pos = malloc(sizeof(Position));

	pos->x = 3; pos->y = 3;
	dt.pos = pos;
	dt.rotation = 0;
	dt.spriteno = 0;
	dt.neighboors[0] = NULL;
	dt.neighboors[1] = NULL;
	dt.neighboors[2] = NULL;
	dt.neighboors[3] = NULL;
	displayed_tiles[display_tiles_num] = dt;
	display_tiles_num++;

	// MAIN LOOP
	while(!do_exit) {

		addblock = 0;
		startTimer = SDL_GetTicks();

		// user input
		while(SDL_PollEvent(&event)) {
			if(event.type == SDL_QUIT)
				do_exit = 1;

			if(keys[SDLK_ESCAPE]) {
				do_exit = 1;
				break;
			}

			if(event.type == SDL_MOUSEBUTTONUP && event.button.button == SDL_BUTTON_RIGHT && camera_moving) {
				fprintf(stderr, "LOL STOP MOVING\n");
				camera_moving = 0;
			}

			if(event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_RIGHT) {
				fprintf(stderr, "OMG RIGHT\n");
				if(camera_moving) {}
				else {
					fprintf(stderr, "I START TO MOVE\n");
					camera_moving = 1;
					camera_move_x = event.motion.x;
					camera_move_y = event.motion.y;
				}
			}

			if(event.type == SDL_MOUSEMOTION) {
				if(camera_moving) {
					camera.x += event.motion.x - camera_move_x;
					camera.y += event.motion.y - camera_move_y;
					camera_move_x = event.motion.x;
					camera_move_y = event.motion.y;
				}

				myx = event.motion.x - camera.x - TILE_SIZE / 2;
				myy = event.motion.y - camera.y - TILE_SIZE / 2;

				myx = myx / TILE_SIZE * TILE_SIZE;
				myy = myy / TILE_SIZE * TILE_SIZE;

				fprintf(stderr, "LOL POS %d %d\n", myx, myy);

				// Move camera at borders
				// cam_velo_x = 0;
				// cam_velo_y = 0;

				/*
				if(event.motion.x <= CAMERA_SCROLL_MARGIN)
					cam_velo_x = - CAMERA_SCROLL_MARGIN + event.motion.x;
				else if(event.motion.x >= WIN_WIDTH - CAMERA_SCROLL_MARGIN)
					cam_velo_x = CAMERA_SCROLL_MARGIN - WIN_WIDTH + event.motion.x;
				else
					cam_velo_x = 0;

				if(event.motion.y <= CAMERA_SCROLL_MARGIN)
					cam_velo_y = - CAMERA_SCROLL_MARGIN + event.motion.y;
				else if(event.motion.y >= WIN_HEIGHT - CAMERA_SCROLL_MARGIN)
					cam_velo_y = CAMERA_SCROLL_MARGIN - WIN_HEIGHT + event.motion.y;
				else
					cam_velo_y = 0;

				cam_velo_x *= CAMERA_SCROLL_ACCELERATION;
				cam_velo_y *= CAMERA_SCROLL_ACCELERATION;
				*/

			}

			if(event.type == SDL_MOUSEBUTTONDOWN) {
				switch(event.button.button) {
					case SDL_BUTTON_WHEELUP: myrotate ++; break;
					case SDL_BUTTON_WHEELDOWN: myrotate --; break;
					case SDL_BUTTON_LEFT:
						addblock = 1;
					break;
				}
			}
			if(myrotate < 0)
				myrotate = 3;
			myrotate %= 4;

			/*
			if(keys[SDLK_DOWN])
				camera.y += 10;
			if(keys[SDLK_UP])
				camera.y -= 10;
			if(keys[SDLK_LEFT])
				camera.x -= 10;
			if(keys[SDLK_RIGHT])
				camera.x += 10;
			*/
			if(keys[SDLK_a])
				camera_z -= 0.01;
			if(keys[SDLK_z])
				camera_z += 0.01;

			if(keys[SDLK_PAGEDOWN]) {
				mysprite++;
				mysprite %= SPRITE_COUNT;
			}

			if(keys[SDLK_PAGEUP]) {
				mysprite--;
				if(mysprite < 0)
					mysprite = SPRITE_COUNT - 1;
			}

		}

		// game logic
		// camera.x -= cam_velo_x;
		// camera.y -= cam_velo_y;

		if(addblock) {
			addblock = 0;
			if(display_tiles_num < MAX_TILES - 1) {
				fprintf(stderr, "I FUCKING ADD A TILE LOL %d\n", display_tiles_num);

				dt.x = myx; dt.y = myy;
				dt.rotation = myrotate;
				dt.spriteno = mysprite;
				dt.neighboors[0] = NULL;
				dt.neighboors[1] = NULL;
				dt.neighboors[2] = NULL;
				dt.neighboors[3] = NULL;

				for(i = 0; i < display_tiles_num; i++) {
					dtsearch = displayed_tiles[i];
					if(dtsearch.x == dt.x && dtsearch.y == dt.y - 1) // up
						dt.neighboors[0] = &dtsearch;
					if(dtsearch.x == dt.x + 1 && dtsearch.y == dt.y) // right
						dt.neighboors[1] = &dtsearch;
					if(dtsearch.x == dt.x && dtsearch.y == dt.y + 1) // bottom
						dt.neighboors[2] = &dtsearch;
					if(dtsearch.x == dt.x - 1 && dtsearch.y == dt.y) // left
						dt.neighboors[3] = &dtsearch;
				}

				displayed_tiles[display_tiles_num] = dt;
				display_tiles_num++;
			}
		}
		// fprintf(stderr, "%d ", myrotate);
		// rendering
		// message = TTF_RenderUTF8_Solid( font, "Hello my tile is ", textColor );

		/* Translate (Move) to the cameras position */
		// Clear the screen before drawing
		glClear( GL_COLOR_BUFFER_BIT );

		// fprintf(stderr, "camera %d %d\n", camera.x, camera.y);
		glMatrixMode( GL_PROJECTION );
		glLoadIdentity();
		glOrtho( 0, WIN_WIDTH, WIN_HEIGHT, 0, -1, 1 );
		glScalef(camera_z, camera_z, 1);
		glTranslatef(1 / camera_z * camera.x, 1 / camera_z * camera.y, 0);

		glMatrixMode( GL_MODELVIEW );

		// TILES
		// markers
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_LINE]);
		for(i = 0; i < display_tiles_num; i++) {
			dt = displayed_tiles[i];

			for(j = 0; j < 4; j++) {
				if(!dt.neighboors[j]) {
					glPushMatrix();
					glTranslatef(dt.x + offset_marker[(j+1)%4], dt.y + offset_marker[j], 0);
					glRotatef(90 * j, 0, 0, 1);
					draw_image(100, 30, 0, 1, 0, 1);
					glPopMatrix();
				}
			}
		}

		// tiles

		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_TILES]);
		// mouse tile
		// glLoadIdentity();
		position_img(myx, myy, mysprite, TILE_SIZE, TILE_SIZE, 90 * myrotate);

		// all tiles
		for(i = 0; i < display_tiles_num; i++) {
			DisplayedTile dt = displayed_tiles[i];
			position_img(dt.x, dt.y, dt.spriteno, TILE_SIZE, TILE_SIZE, 90 * dt.rotation);
		}


		// INTERFACE
		// glLoadIdentity();

		glMatrixMode( GL_PROJECTION );
		glLoadIdentity();
		glOrtho( 0, WIN_WIDTH, WIN_HEIGHT, 0, -1, 1 );
		glScalef(1, 1, 1);
		glTranslatef(0, 0, 0);

		glMatrixMode( GL_MODELVIEW );

		glBindTexture( GL_TEXTURE_2D, textures[TEXTURE_INTERFACE] );
		glPushMatrix();
		glLoadIdentity();
		glTranslatef(0, WIN_HEIGHT - INTERFACE_HEIGHT, 0);
		draw_image(WIN_WIDTH, INTERFACE_HEIGHT, 0, 1, 0, 1);
		glPopMatrix();

		message = TTF_RenderUTF8_Solid( font, "Hello my tile is ", textColor );

		SDL_GL_SwapBuffers();

		++frames;
		endTimer = SDL_GetTicks();

		sprintf(fpsbuff, "FPS: %.1f", (frames / (float)(endTimer - globalStartTimer))*1000);
		SDL_WM_SetCaption(fpsbuff, NULL);


		deltaTime = endTimer - startTimer;

		if(deltaTime < (1000 / FRAMERATE))
			SDL_Delay((1000 / FRAMERATE) - deltaTime);

	}

	// Now we can delete the OpenGL texture and close down SDL
	glDeleteTextures( SPRITE_COUNT, textures );
	TTF_CloseFont( font );
	SDL_Quit();

	return 0;
}
