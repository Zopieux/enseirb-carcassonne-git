#include <stdlib.h>
#include <string.h> // for memcpy
#include <CuTest.h>

#include <common.h>
#include <tad/tile.h>
#include <tad/tiledeck.h>

#define DECK_SIZE 10
#define TEST_ITERATIONS 20


void TileDeck_Test(CuTest* tc) {
	int i;

	Tile* tiles = malloc(DECK_SIZE * sizeof(Tile));
	for(i = 0; i < DECK_SIZE; i++) {
		tiles[i].rotation = 0;
		tiles[i].shape = NULL;
	}

	TileDeck* td = tileDeckInit(tiles, DECK_SIZE);

	CuAssertTrue(tc, tileDeckCapacity(td) == DECK_SIZE);
	CuAssertTrue(tc, tileDeckSize(td) == DECK_SIZE - 1);

	Tile* tile = tileDeckSeek(td);
	CuAssertTrue(tc, tile == &tiles[0]);
	CuAssertTrue(tc, tileDeckSize(td) == DECK_SIZE - 1);

	CuAssertTrue(tc, tileDeckPick(td) == &tiles[1]);
	CuAssertTrue(tc, tileDeckSize(td) == DECK_SIZE - 2);

	tileDeckFree(td);

	// Nouveau TileDeck pour d'autres tests
	td = tileDeckInit(tiles, DECK_SIZE);

	size_t stackSize = DECK_SIZE * sizeof(Tile*);
	Tile** stackCopy = malloc(stackSize);
	memcpy(stackCopy, td->stack, stackSize);

	size_t upTo, j;

	for(upTo = 0; upTo <= DECK_SIZE; upTo++) {
		for(i = 0; i < TEST_ITERATIONS; i++) {
			// reset the stack
			memcpy(td->stack, stackCopy, stackSize);
			// and re-shuffle it
			tileDeckShuffle(td, upTo);
			for(j = 0; j < upTo; j++)
				CuAssertTrue(tc, td->stack[j] == &tiles[j]);
		}
	}
}


CuSuite* TileDeckTest_GetSuite() {
	CuSuite* suite = CuSuiteNew();
	SUITE_ADD_TEST(suite, TileDeck_Test);
	return suite;
}
