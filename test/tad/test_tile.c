#include <stdio.h>
#include <CuTest.h>

#include <common.h>
#include <tad/tile.h>
#include <tad/ugraph.h>

#define TEST_TILE_COUNT 10

static TileShape* shapes;
static Tile* tiles;
static size_t shapeCount, tileCount;


// THIS TEST MUST BE RUN BEFORE ANY OTHER TO LOAD EXAMPLE DATA
void Tile_ShapeDefTest(CuTest* tc) {
	FILE* file = fopen("example_tile_def.data", "r");
	CuAssertPtrNotNull(tc, file);
	tileParseDef(file, &shapeCount, &tileCount, &shapes, &tiles);
	fclose(file);

	CuAssertIntEquals(tc, shapeCount, 3);
	CuAssertIntEquals(tc, tileCount, 6);

	CuAssertIntEquals(tc, shapes[0].uid, 0);
	CuAssertIntEquals(tc, shapes[0].type, TILE_NORMAL);
	CuAssertIntEquals(tc, shapes[0].bonus, 0);
	CuAssertIntEquals(tc, shapes[0].tileCount, 2);
	CuAssertIntEquals(tc, shapes[0].areas[0], CASTLE);
	CuAssertIntEquals(tc, shapes[0].areas[2], ROAD);
	CuAssertIntEquals(tc, shapes[0].areas[4], FARM);
	CuAssertIntEquals(tc, shapes[0].areas[6], CASTLE);

	CuAssertIntEquals(tc, shapes[2].areas[0], CASTLE);
	CuAssertIntEquals(tc, shapes[2].areas[2], CASTLE);
	CuAssertIntEquals(tc, shapes[2].areas[4], CASTLE);
	CuAssertIntEquals(tc, shapes[2].areas[6], ROAD);
	// Check computed corners are OK
	CuAssertIntEquals(tc, shapes[2].areas[1], CASTLE);
	CuAssertIntEquals(tc, shapes[2].areas[3], CASTLE);
	CuAssertIntEquals(tc, shapes[2].areas[5], FARM);
	CuAssertIntEquals(tc, shapes[2].areas[7], FARM);

	CuAssertTrue(tc,  ugraphEdgeExists(shapes[0].connections, NORTH, SOUTH));
	CuAssertTrue(tc,  ugraphEdgeExists(shapes[0].connections, EAST, WEST));
	CuAssertTrue(tc,  ugraphEdgeExists(shapes[0].connections, SOUTH, WEST));
	CuAssertTrue(tc, !ugraphEdgeExists(shapes[0].connections, NORTH, WEST));
	CuAssertTrue(tc, !ugraphEdgeExists(shapes[0].connections, NORTH, EAST));

	CuAssertTrue(tc,  ugraphEdgeExists(shapes[2].connections, NORTH, EAST));
	CuAssertTrue(tc,  ugraphEdgeExists(shapes[2].connections, EAST, SOUTH));
	CuAssertTrue(tc,  ugraphEdgeExists(shapes[2].connections, NORTHWEST, NORTHEAST));
	CuAssertTrue(tc,  ugraphEdgeExists(shapes[2].connections, WEST, SOUTHWEST));


	CuAssertIntEquals(tc, shapes[1].type, TILE_ABBEY);
	CuAssertIntEquals(tc, shapes[1].bonus, 12);
	CuAssertIntEquals(tc, shapes[1].tileCount, 2);
}


void Tile_GetterTest(CuTest* tc) {
	Tile* the_tile = &tiles[0];

	// Test opposite direction
	CuAssertIntEquals(tc, tileGetOppositeGeo(NORTH), SOUTH);
	CuAssertIntEquals(tc, tileGetOppositeGeo(EAST), WEST);
	CuAssertIntEquals(tc, tileGetOppositeGeo(SOUTH), NORTH);
	CuAssertIntEquals(tc, tileGetOppositeGeo(WEST), EAST);

	// Test relative rotation
	tileSetRotation(the_tile, 0, false);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, NORTH), CASTLE);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, EAST), ROAD);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, SOUTH), FARM);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, WEST), CASTLE);

	// Test relative rotation
	tileSetRotation(the_tile, 0, false);

	tileSetRotation(the_tile, 0, true); // shouldn't change anything
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, NORTH), CASTLE);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, EAST), ROAD);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, SOUTH), FARM);
	tileSetRotation(the_tile, 1, true);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, NORTH), CASTLE);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, EAST), CASTLE);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, SOUTH), ROAD);
	tileSetRotation(the_tile, 1, true);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, NORTH), FARM);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, EAST), CASTLE);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, SOUTH), CASTLE);

	tileSetRotation(the_tile, 0, false);
	tileSetRotation(the_tile, -2, true);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, NORTH), FARM);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, EAST), CASTLE);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, SOUTH), CASTLE);
	CuAssertIntEquals(tc, tileGetFieldForGeo(the_tile, WEST), ROAD);

	free(tiles);
}


CuSuite* TileTest_GetSuite() {
	CuSuite* suite = CuSuiteNew();
	SUITE_ADD_TEST(suite, Tile_ShapeDefTest);
	SUITE_ADD_TEST(suite, Tile_GetterTest);
	return suite;
}
