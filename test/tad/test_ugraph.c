#include <CuTest.h>

#include <tad/ugraph.h>


void UGraph_test_all(CuTest* tc) {
	UGraph* g = ugraphInit(4);

	CuAssertTrue(tc, !ugraphEdgeExists(g, 0, 0));
	CuAssertTrue(tc, !ugraphEdgeExists(g, 1, 0));
	CuAssertTrue(tc, !ugraphEdgeExists(g, 0, 1));

	ugraphAddEdge(g, 1, 0);
	CuAssertTrue(tc, !ugraphEdgeExists(g, 0, 0));
	CuAssertTrue(tc, !ugraphEdgeExists(g, 1, 1));
	CuAssertTrue(tc,  ugraphEdgeExists(g, 0, 1));
	CuAssertTrue(tc,  ugraphEdgeExists(g, 1, 0));

	// Ensure path non-existence
	CuAssertTrue(tc,  ugraphPathExists(g, 0, 1));
	CuAssertTrue(tc, !ugraphPathExists(g, 0, 2));
	CuAssertTrue(tc, !ugraphPathExists(g, 0, 3));
	CuAssertTrue(tc, !ugraphPathExists(g, 1, 2));
	CuAssertTrue(tc, !ugraphPathExists(g, 1, 3));
	CuAssertTrue(tc, !ugraphPathExists(g, 2, 3));

	ugraphRemoveEdge(g, 1, 0);
	CuAssertTrue(tc, !ugraphEdgeExists(g, 1, 0));
	CuAssertTrue(tc, !ugraphEdgeExists(g, 0, 1));

	ugraphAddEdge(g, 0, 1);
	CuAssertTrue(tc, !ugraphEdgeExists(g, 0, 0));
	CuAssertTrue(tc, !ugraphEdgeExists(g, 1, 1));
	CuAssertTrue(tc,  ugraphEdgeExists(g, 0, 1));
	CuAssertTrue(tc,  ugraphEdgeExists(g, 1, 0));

	ugraphRemoveEdge(g, 1, 0);
	CuAssertTrue(tc, !ugraphEdgeExists(g, 1, 0));
	CuAssertTrue(tc, !ugraphEdgeExists(g, 0, 1));

	ugraphAddEdge(g, 0, 0);
	ugraphAddEdge(g, 0, 1);
	ugraphAddEdge(g, 1, 2);
	ugraphAddEdge(g, 2, 3);
	CuAssertTrue(tc,  ugraphEdgeExists(g, 0, 0));
	CuAssertTrue(tc,  ugraphEdgeExists(g, 0, 1));
	CuAssertTrue(tc,  ugraphEdgeExists(g, 1, 2));
	CuAssertTrue(tc,  ugraphEdgeExists(g, 2, 3));
	CuAssertTrue(tc, !ugraphEdgeExists(g, 1, 3));
	CuAssertTrue(tc, !ugraphEdgeExists(g, 0, 2));
	CuAssertTrue(tc, !ugraphEdgeExists(g, 0, 3));
	CuAssertTrue(tc, !ugraphEdgeExists(g, 1, 3));

	// Ensure path existence
	CuAssertTrue(tc,  ugraphPathExists(g, 0, 0));
	CuAssertTrue(tc,  ugraphPathExists(g, 0, 1));
	CuAssertTrue(tc,  ugraphPathExists(g, 0, 2));
	CuAssertTrue(tc,  ugraphPathExists(g, 2, 0));
	CuAssertTrue(tc,  ugraphPathExists(g, 0, 3));
	CuAssertTrue(tc,  ugraphPathExists(g, 3, 0));
	CuAssertTrue(tc,  ugraphPathExists(g, 1, 3));
	CuAssertTrue(tc,  ugraphPathExists(g, 3, 1));

	ugraphRemoveEdge(g, 1, 2);
	CuAssertTrue(tc,  ugraphPathExists(g, 0, 1));
	CuAssertTrue(tc,  ugraphPathExists(g, 2, 3));
	CuAssertTrue(tc, !ugraphPathExists(g, 0, 2));
	CuAssertTrue(tc, !ugraphPathExists(g, 0, 3));
	CuAssertTrue(tc, !ugraphPathExists(g, 1, 3));


	// Test tool funcs
	ugraphConnectAll(g);
	for(Vertex i = 0; i < g->vertexCount; i++)
		for(Vertex j = i; j < g->vertexCount; j++)
			CuAssertTrue(tc, ugraphEdgeExists(g, i, j));

	ugraphDisconnectAll(g);
	for(Vertex i = 0; i < g->vertexCount; i++)
		for(Vertex j = i; j < g->vertexCount; j++)
			CuAssertTrue(tc, !ugraphEdgeExists(g, i, j));

	Vertex v = 0;
	ugraphConnectVertex(g, v);

	for(Vertex i = 0; i < g->vertexCount; i++)
		CuAssertTrue(tc, ugraphEdgeExists(g, v, i));

	for(Vertex i = 1; i < g->vertexCount; i++)
		for(Vertex j = i; j < g->vertexCount; j++)
			CuAssertTrue(tc, !ugraphEdgeExists(g, i, j));

	ugraphFree(g);
}


CuSuite* UGraphTest_GetSuite() {
	CuSuite* suite = CuSuiteNew();
	SUITE_ADD_TEST(suite, UGraph_test_all);
	return suite;
}
