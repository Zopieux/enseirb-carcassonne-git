#include <CuTest.h>

#include <common.h>
#include <tad/board.h>
#include <tad/tile.h>


void Board_GetterTest(CuTest* tc) {

}


void Board_PieceAddingTest(CuTest* tc) {

}


CuSuite* BoardTest_GetSuite() {
	CuSuite* suite = CuSuiteNew();
	SUITE_ADD_TEST(suite, Board_GetterTest);
	SUITE_ADD_TEST(suite, Board_PieceAddingTest);
	return suite;
}
