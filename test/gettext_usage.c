#include <stdio.h>
#include <libintl.h>
#include <locale.h>
#define _(str) gettext(str)

#define PROGRAM_NAME "carcassonne"
#define LOCALE_DIRECTORY "/net/i/amacabies/projets/s6/carcassonne/locale"

int main(int argc, char const *argv[]) {

	setlocale(LC_ALL, "en_US.UTF-8");

	bindtextdomain(PROGRAM_NAME, LOCALE_DIRECTORY);
	textdomain(PROGRAM_NAME);

	int c = 42;

	printf(_("Il y a %d pommes dans mon panier !\n"), c);

	c = 1;
	printf(
		ngettext(
			"Il y a %d cheval dans le pré !\n",
			"Il y a %d chevaux dans le pré !\n",
			c
		),
		c
	);

	c = 5;
	printf(
		ngettext(
			"Il y a %d cheval dans le pré !\n",
			"Il y a %d chevaux dans le pré !\n",
			c
		),
		c
	);

	return 0;
}