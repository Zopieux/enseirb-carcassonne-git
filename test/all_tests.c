#include <stdio.h>

#include <CuTest.h>


CuSuite* UtilsTest_GetSuite();
CuSuite* TileTest_GetSuite();
CuSuite* TileDeckTest_GetSuite();
// CuSuite* BoardTest_GetSuite();
CuSuite* UGraphTest_GetSuite();


void
RunAllTests(void) {
	CuString *output = CuStringNew();
	CuSuite* suite = CuSuiteNew();

	CuSuiteAddSuite(suite, UtilsTest_GetSuite());
	CuSuiteAddSuite(suite, TileTest_GetSuite());
	CuSuiteAddSuite(suite, TileDeckTest_GetSuite());
	//CuSuiteAddSuite(suite, BoardTest_GetSuite());
	CuSuiteAddSuite(suite, UGraphTest_GetSuite());

	CuSuiteRun(suite);
	CuSuiteSummary(suite, output);
	CuSuiteDetails(suite, output);
	printf("%s\n", output->buffer);
}


int
main(void) {
	RunAllTests();
	return 0;
}
