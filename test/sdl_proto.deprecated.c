#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_opengl.h>
#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>

#define WIN_WIDTH 1024
#define WIN_HEIGHT 768

#define TILE_SIZE 100

#define SPRITE_COUNT 24
#define TILESET_ID 1
#define MAX_TILES 40

#define CAMERA_SCROLL_MARGIN 50
#define CAMERA_SCROLL_ACCELERATION 0.6

#define FRAMERATE 24.0

void position_img(int x, int y, int spriteno, int width, int height, int angle) {
	float text_x = (float)(spriteno) / (float)(SPRITE_COUNT);
	float text_x_n = (float)(spriteno + 1) / (float)(SPRITE_COUNT);

	glPushMatrix();
	glLoadIdentity();

	glTranslatef (x + width/2, y + height/2, 0);
	glRotatef (angle, 0, 0, 1);
	glTranslatef (-width/2, -height/2, 0);

	glBegin( GL_QUADS );
		// Top-left vertex (corner)
		glTexCoord2f( text_x , 0.0 );
		glVertex2i( 0, 0 );

		// Bottom-left vertex (corner)
		glTexCoord2f( text_x_n, 0.0 );
		glVertex2i( width, 0);

		// Bottom-right vertex (corner)
		glTexCoord2f( text_x_n, 1.0 );
		glVertex2i( width, height);

		// Top-right vertex (corner)
		glTexCoord2f( text_x, 1.0 );
		glVertex2i( 0, height );
	glEnd();

	glPopMatrix();
}

void load_tile_file(SDL_Surface* surfarray[], GLuint* textures, int spriteno, char* fname) {
	GLuint texture;
	SDL_Surface* surface = IMG_Load(fname);
	surfarray[spriteno] = surface;

	/*
	if ( (surface->w & (surface->w - 1)) != 0 ) {
		printf("warning: %s width is not a power of 2\n", fname);
	}

	// Also check if the height is a power of 2
	if ( (surface->h & (surface->h - 1)) != 0 ) {
		printf("warning: %s height is not a power of 2\n", fname);
	}
	*/

	// Bind the texture object
	glBindTexture( GL_TEXTURE_2D, textures[spriteno] );

	// Set the texture's stretching properties
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

	// Edit the texture object's image data using the information SDL_Surface gives us
	glTexImage2D( GL_TEXTURE_2D, 0, 3, surface->w, surface->h, 0,
				  GL_RGB, GL_UNSIGNED_BYTE, surface->pixels );

	// Free the SDL_Surface only if it was successfully created
	if ( surface ) {
		SDL_FreeSurface( surface );
	}

}

typedef struct {
	int spriteno;
	int x; int y;
	int rotation;
}
DisplayedTile;


int main(int argc, char *argv[]) {
	SDL_Surface *screen, *message;

	SDL_Rect camera = { 0.0, 0.0, WIN_WIDTH, WIN_HEIGHT };

	GLuint textures[SPRITE_COUNT];
	SDL_Surface* surfaces[SPRITE_COUNT];

	DisplayedTile displayed_tiles[MAX_TILES];
	unsigned int display_tiles_num = 0;

	// Slightly different SDL initialization
	if ( SDL_Init(SDL_INIT_VIDEO) != 0 ) {
		printf("Unable to initialize SDL: %s\n", SDL_GetError());
		return 1;
	}

	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 ); // *new*

	screen = SDL_SetVideoMode( WIN_WIDTH, WIN_HEIGHT, 16, SDL_OPENGL | SDL_RESIZABLE ); // *changed*
	if ( !screen ) {
		printf("Unable to set video mode: %s\n", SDL_GetError());
		return 1;
	}

	// Set the OpenGL state after creating the context with SDL_SetVideoMode

	glClearColor( 0, 0, 0, 0 );

	glEnable( GL_TEXTURE_2D ); // Need this to display a texture
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Have OpenGL generate a texture object handle for us
	glGenTextures( SPRITE_COUNT, textures );

	int vpx, vpy;
	vpx = 0; vpy = 0;
	glViewport( vpx, vpy, WIN_WIDTH, WIN_HEIGHT );

	// Load the OpenGL texture

	GLuint texture; // Texture object handle
	SDL_Surface *surface; // Gives us the information to make the texture

	/*
	char* fnames[SPRITE_COUNT] = {
		"Cc.1", "CFc.1", "C", "L", "CFc+", "Ccc", "CcRr", "CccR", "RrC",
		"CRRR", "RRRR", "Cc+", "Rr", "RRR", "LR", "CFC.2", "CC.2", "Cccc+",
		"CcRr+", "CRr", "RCr", "CccR+", "RFr", "Ccc+"
	};

	char fbuff[500];
	for(int i = 0; i < SPRITE_COUNT; i++) {
		sprintf(fbuff, "../../ressources/tiles/BA/%s.jpg", fnames[i]);
		fprintf(stderr, "lola? %s\n", fbuff);
		load_tile_file(surfaces, textures, i, fbuff);
	}
	*/
	load_tile_file(surfaces, textures, TILESET_ID, "../../ressources/tiles/tileset.jpg");

	TTF_Font *font = NULL;
	SDL_Color textColor = { 255, 20, 20 };
	font = TTF_OpenFont( "/usr/share/fonts/TTF/DejaVuSansCondensed.ttf", 28 );

	SDL_Event event;

	int i, myx, myy, myrotate, mysprite;
	Uint8 *keys;
	long tick;
	keys = SDL_GetKeyState(&i);
	tick = SDL_GetTicks();
	int do_exit = 0;

	myx = 0; myy = 0;
	mysprite = 0;
	myrotate = 0;

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	glOrtho( 0, WIN_WIDTH, WIN_HEIGHT, 0, -1, 1 );

	glMatrixMode( GL_MODELVIEW );
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture( GL_TEXTURE_2D, textures[TILESET_ID] );

	int addblock;
	float cam_velo_x, cam_velo_y, velo_sign;

	unsigned int frames = 0;
	int startTimer, endTimer, deltaTime, globalStartTimer = SDL_GetTicks();
	char fpsbuff[50];

	while(!do_exit) {

		addblock = 0;
		startTimer = SDL_GetTicks();

		// user input
		while(SDL_PollEvent(&event)) {
			if(event.type == SDL_QUIT)
				do_exit = 1;

			if(keys[SDLK_ESCAPE]) {
				do_exit = 1;
				break;
			}

			if(event.type == SDL_MOUSEMOTION) {
				/* printf("Mouse moved by %d,%d to (%d,%d)\n",
					   event.motion.xrel, event.motion.yrel,
					   event.motion.x, event.motion.y);
				*/
				// myx = event.motion.x / 100 * 100;
				// myy = event.motion.y / 100 * 100;
				myx = event.motion.x - camera.x - TILE_SIZE / 2;
				myy = event.motion.y - camera.y - TILE_SIZE / 2;
				myx = myx / 100 * 100;
				myy = myy / 100 * 100;

				// Move camera at borders
				// cam_velo_x = 0;
				// cam_velo_y = 0;

				if(event.motion.x <= CAMERA_SCROLL_MARGIN)
					cam_velo_x = - CAMERA_SCROLL_MARGIN + event.motion.x;
				else if(event.motion.x >= WIN_WIDTH - CAMERA_SCROLL_MARGIN)
					cam_velo_x = CAMERA_SCROLL_MARGIN - WIN_WIDTH + event.motion.x;
				else
					cam_velo_x = 0;

				if(event.motion.y <= CAMERA_SCROLL_MARGIN)
					cam_velo_y = - CAMERA_SCROLL_MARGIN + event.motion.y;
				else if(event.motion.y >= WIN_HEIGHT - CAMERA_SCROLL_MARGIN)
					cam_velo_y = CAMERA_SCROLL_MARGIN - WIN_HEIGHT + event.motion.y;
				else
					cam_velo_y = 0;

				cam_velo_x *= CAMERA_SCROLL_ACCELERATION;
				cam_velo_y *= CAMERA_SCROLL_ACCELERATION;

			}

			if(event.type == SDL_MOUSEBUTTONDOWN) {
				switch(event.button.button) {
					case SDL_BUTTON_WHEELUP: myrotate ++; break;
					case SDL_BUTTON_WHEELDOWN: myrotate --; break;
					case SDL_BUTTON_LEFT:
						addblock = 1;
					break;
				}
			}
			if(myrotate < 0)
				myrotate = 3;
			myrotate %= 4;

			/*
			if(keys[SDLK_DOWN])
				camera.y += 10;
			if(keys[SDLK_UP])
				camera.y -= 10;
			if(keys[SDLK_LEFT])
				camera.x -= 10;
			if(keys[SDLK_RIGHT])
				camera.x += 10;
			*/

			if(keys[SDLK_PAGEDOWN]) {
				mysprite++;
				mysprite %= SPRITE_COUNT;
			}

			if(keys[SDLK_PAGEUP]) {
				mysprite--;
				if(mysprite < 0)
					mysprite = SPRITE_COUNT - 1;
			}

		}

		// game logic
		camera.x -= cam_velo_x;
		camera.y -= cam_velo_y;

		if(addblock) {
			addblock = 0;
			if(display_tiles_num < MAX_TILES - 1) {
				fprintf(stderr, "I FUCKING ADD A TILE LOL %d\n", display_tiles_num);

				DisplayedTile dt;
				dt.x = myx; dt.y = myy;
				dt.rotation = myrotate;
				dt.spriteno = mysprite;

				displayed_tiles[display_tiles_num] = dt;

				display_tiles_num++;
			}
		}
		// fprintf(stderr, "%d ", myrotate);
		// rendering
		// message = TTF_RenderUTF8_Solid( font, "Hello my tile is ", textColor );

		/* Translate (Move) to the cameras position */
		// Clear the screen before drawing
		glClear( GL_COLOR_BUFFER_BIT );

		// fprintf(stderr, "camera %d %d\n", camera.x, camera.y);
		glMatrixMode( GL_PROJECTION );
		glLoadIdentity();
		glOrtho( 0, WIN_WIDTH, WIN_HEIGHT, 0, -1, 1 );
		glTranslatef(camera.x, camera.y, 0);

		glMatrixMode( GL_MODELVIEW );

		glLoadIdentity();

		// glLoadIdentity();

		// Bind the texture to which subsequent calls refer to

		glMatrixMode( GL_MODELVIEW );

		// mouse tile
		// glBindTexture( GL_TEXTURE_2D, textures[mysprite] );
		position_img(myx, myy, mysprite, TILE_SIZE, TILE_SIZE, 90 * myrotate);

		// all tiles
		for(i = 0; i < display_tiles_num; i++) {
			DisplayedTile dt = displayed_tiles[i];
			position_img(dt.x, dt.y, dt.spriteno, TILE_SIZE, TILE_SIZE, 90 * dt.rotation);
		}

		// glPopMatrix();

		SDL_GL_SwapBuffers();

		++frames;
		endTimer = SDL_GetTicks();

		sprintf(fpsbuff, "FPS: %.1f", (frames / (float)(endTimer - globalStartTimer))*1000);
		SDL_WM_SetCaption(fpsbuff, NULL);


		deltaTime = endTimer - startTimer;

		if(deltaTime < (1000 / FRAMERATE))
			SDL_Delay((1000 / FRAMERATE) - deltaTime);

	}

	// Now we can delete the OpenGL texture and close down SDL
	glDeleteTextures( SPRITE_COUNT, textures );
	TTF_CloseFont( font );
	SDL_Quit();

	return 0;
}
