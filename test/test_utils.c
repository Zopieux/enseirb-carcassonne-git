#include <stdio.h>
#include <CuTest.h>

#include <utils.h>

#define TEST_ITERATIONS 1000


void Utils_test_randomInt(CuTest* tc) {
	int a = -100, b = 100;
	int x;

	randomSeed();

	CuAssertIntEquals(tc, randomInt(0, 0), 0);
	CuAssertIntEquals(tc, randomInt(-1, -1), -1);
	CuAssertIntEquals(tc, randomInt(10, 10), 10);

	for(int i = 0; i < TEST_ITERATIONS; i++) {
		x = randomInt(a, b);
		CuAssertTrue(tc, a <= x && x <= b);
	}
}


void Utils_test_randomSizet(CuTest* tc) {
	size_t a = 10, b = 100;
	size_t x;

	randomSeed();

	CuAssertIntEquals(tc, randomSizet(0, 0), 0);
	CuAssertIntEquals(tc, randomSizet(1, 1), 1);
	CuAssertIntEquals(tc, randomSizet(10, 10), 10);

	for(int i = 0; i < TEST_ITERATIONS; i++) {
		x = randomSizet(a, b);
		CuAssertTrue(tc, a <= x && x <= b);
	}
}


void Utils_test_modulo(CuTest* tc) {
	int i, j;

	for(i = 0; i < 10; i++) {
		for(j = 1; j < 10; j++) {
			CuAssertIntEquals(tc, modulo(i, j), i % j);
		}
	}

	CuAssertIntEquals(tc, modulo(-1, 2), 1);
	CuAssertIntEquals(tc, modulo(-2, 2), 0);
	CuAssertIntEquals(tc, modulo(-3, 2), 1);
	CuAssertIntEquals(tc, modulo(-4, 2), 0);
}


CuSuite* UtilsTest_GetSuite() {
	CuSuite* suite = CuSuiteNew();
	SUITE_ADD_TEST(suite, Utils_test_randomInt);
	SUITE_ADD_TEST(suite, Utils_test_randomSizet);
	SUITE_ADD_TEST(suite, Utils_test_modulo);
	return suite;
}
