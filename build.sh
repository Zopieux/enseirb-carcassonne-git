#!/bin/bash

HERE=`pwd`
BUILD_DIR="build"
MAIN_DIR="src"
BINARY="carcassonne"

function announce {
    echo -ne "\n[== CARCASSONNE BUILD ==] $1\n\n"
}

mkdir "$BUILD_DIR" 2>/dev/null
cd "$BUILD_DIR"

announce "Now building projet..."
cmake ..

announce "Now compiling project..."
make

if [ $? -ne 0 ]
then
    announce "Make failed. Exiting."
    exit $!
fi

announce "Copying main binary..."
cp "$HERE/$BUILD_DIR/$MAIN_DIR/$BINARY" "$HERE"
