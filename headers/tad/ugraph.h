#ifndef _UGRAPH_H_
#define _UGRAPH_H_

/*
TAD de graphe non orienté.
*/

#include <stdlib.h>
#include <stdbool.h>

typedef struct UGraph_s {
	size_t vertexCount;
	bool* edgeMatrix;
} UGraph;

typedef unsigned int Vertex;

/**
 * @brief  Initalise un graphe avec un nombre donné de sommets et aucune arête.
 * @param  size_t Le nombre de sommets.
 * @return Le graphe vide.
 */
UGraph* ugraphInit(size_t);

/**
 * @brief  Libère la mémoire allouée pour le graphe.
 * @param  UGraph*
 */
void ugraphFree(UGraph*);

/**
 * @brief  Retourne le nombre de sommets du graphe.
 * @param  Ugraph*
 * @return size_t
 */
size_t ugraphSize(UGraph*);

/**
 * @brief  Ajoute une arête au graphe. Si elle existe déjà, rien ne se passe.
 * @param  UGraph* Le graphe à modifier.
 * @param  Vertex  Un premier sommet.
 * @param  Vertex  Un second sommet.
 */
void ugraphAddEdge(UGraph*, Vertex, Vertex);

/**
 * @brief  Retire une arête d'un graphe. Si elle n'existait pas, rien ne
 *         se passe.
 * @param  UGraph* Le graphe à modifier.
 * @param  Vertex  Un premier sommet.
 * @param  Vertex  Un second sommet.
 */
void ugraphRemoveEdge(UGraph*, Vertex, Vertex);

/**
 * @brief  Vérifie qu'il existe une arête entre deux sommets.
 * @param  UGraph* Le graphe à consulter.
 * @param  Vertex  Un premier sommet.
 * @param  Vertex  Un second sommet.
 * @return bool
 */
bool ugraphEdgeExists(UGraph*, Vertex, Vertex);


/**
 * @brief  Vérifie qu'il existe un chemin entre deux sommets.
 * @param  UGraph* Le graphe à consulter.
 * @param  Vertex  Un premier sommet.
 * @param  Vertex  Un second sommet.
 * @return bool
 */
bool ugraphPathExists(UGraph*, Vertex, Vertex);

/**
 * @brief  Créé des arêtes entre tous les sommets.
 * @param  UGraph* Le graphe à modifier.
 **/
void ugraphConnectAll(UGraph*);

/**
 * @brief  Retire toutes les arêtes du graphe.
 * @param  UGraph* Le graphe à modifier.
 **/
void ugraphDisconnectAll(UGraph*);

/**
 * @brief  Connecte le sommet donné à tous les autres, lui-même compris.
 * @param  UGraph* Le graphe à modifier.
 * @param  Vertex  Le sommet à connecter.
 **/
void ugraphConnectVertex(UGraph*, Vertex);

/**
 * @brief  Déconnecte le sommet donné de tous les autres.
 * @param  UGraph* Le graphe à modifier.
 * @param  Vertex  Le sommet à isoler.
 **/
void ugraphDisconnectVertex(UGraph*, Vertex);

#endif
