#ifndef _PLAYER_H_
#define _PLAYER_H_

/*
TAD pour les joueurs.
*/

/**
 * @brief Associe le nom du joueur à son numéro identifiant.
 * @param size_t L'identifiant du joueur.
 * @param char* Le nom du joueur.
 **/
char* playerGetName(size_t playerId);

/**
 * @brief Associe la couleur (r, g, b) des pions du joueur à son numéro identifiant.
 * @param size_t L'identifiant du joueur.
 * @param float* r La composante rouge de la couleur du joueur, modifiée par 
 *        effet de bord.
 * @param float* r La composante verte de la couleur du joueur, modifiée par 
 *        effet de bord.
 * @param float* r La composante bleue de la couleur du joueur, modifiée par 
 *        effet de bord.
 **/
void playerGetColor(size_t playerId, float* r, float* g, float* b);

/**
 * @brief Associe la couleur (r, g, b) du texte du joueur à son numéro identifiant.
 * @param size_t L'identifiant du joueur.
 * @param float* r La composante rouge de la couleur du joueur, modifiée par 
 *        effet de bord.
 * @param float* r La composante verte de la couleur du joueur, modifiée par 
 *        effet de bord.
 * @param float* r La composante bleue de la couleur du joueur, modifiée par 
 *        effet de bord.
 **/
void playerGetTextColor(size_t playerId, float* r, float* g, float* b);
#endif
