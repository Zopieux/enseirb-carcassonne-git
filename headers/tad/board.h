#ifndef _TAD_BOARD_H_
#define _TAD_BOARD_H_

#include <stdlib.h>
#include <stdbool.h>
#include <glib.h>

#include <common.h>
#include <tad/tile.h>


typedef struct Board_s {
	// map of int position to Tile*
	GHashTable* tileMap;
	// set of int position
	GHashTable* availablePos;
	// map of int position to rotation flags (1, 2, 4, 8)
	GHashTable* currentAvailablePos;
	// set of tiles
	GHashTable* unfinishedAbbey;
	//set of tiles, useful at the end of the game
	GHashTable* pawnPositions;
	size_t onBoardTiles;
	size_t maxTiles;
} Board;

// Helper for callbacks
typedef struct UArgBaT_s {
	Board* board;
	Tile* tile;
} UArgBaT;


/**
 * @brief  Crée un plateau de jeu pouvant contenir jusqu'à maxTiles tuiles.
 * @param  size_t Nombre de tuiles jouables au total (typiquement calculé dans
 *                tile/parseTileDef)
 * @return Board* Le plateau de jeu initialisé.
 **/
Board* boardInit(size_t);

/**
 * @brief  Retourne le nombre de tuiles déjà posées.
 * @param  Board*
 * @return unsigned int
 **/
unsigned int boardPlayedTileCount(const Board*);

/**
 * @brief  Déduit les coordonnées x, y à partir de leur représentation interne.
 * @param  Board*
 * @param  int    La représentation interne
 * @param  int*   x
 * @param  int*   y
 */
void boardIndexToPos(const Board*, int index, int* x, int* y);

/**
 * @brief  Retourne la tuile posée en (x, y).
 * @param  Board*
 * @param  int    x
 * @param  int    y
 * @return Tile*
 */
Tile* boardGetTileAtPos(const Board* b, int x, int y);

/**
 * @brief  Ajoute une tuile au plateau de jeu.
 * @param  Board* Plateau de jeu.
 * @param  Tile*  Tuile à ajouter.
 * @param  int    Position de la tuile à ajouter sur l'axe des abscisses.
 * @param  int    Position de la tuile à ajouter sur l'axe des ordonnées.
 * @return bool   true si la tuile a bien été ajoutée, false sinon.
 **/
bool boardAddTile(Board*, Tile* tile, int x, int y);

/**
 * @brief  Met à jour les positions disponibles autour de (x,y)
 * @param  Board* Plateau de jeu.
 * @param  int    Abscisse de la tuile autour de laquelle se fait la mise à jour.
 * @param  int    Ordonnée de la tuile autour de laquelle se fait la mise à jour.
 **/
void boardUpdateAvailablePosAround(Board* b, int x, int y);

/**
 * @brief  Met à jour les positions disponibles (pour chaque rotation)
 *         pour la tuile donnée.
 * @param  Board*
 * @param  Tile*
 */
void boardUpdateCurrentAvailablePos(Board* b, Tile* currentTile);




//GHashTable* boardCountField(Board* b, Tile* T, unsigned int c, GHashTable * visitedTile, GHashTable * visitedCastle);


/**
 * @brief  Détermine si l'emplacement (x,y) du plateau de jeu est disponible.
 * @param  Board* Plateau de jeu.
 * @param  int    Abscisse de l'emplacement.
 * @param  int    Ordonnée de l'emplacement.
 * @return bool   true si l'emplacement est disponible, false sinon.
 **/
bool boardPositionIsAvailable(const Board*, int x, int y);

/**
 * @brief  Détermine si la tuile tile peut être placée à l'emplacement (x,y)
 *         sur le plateau de jeu.
 * @param  Board* Plateau de jeu.
 * @param  int    Abscisse de l'emplacement.
 * @param  int    Ordonnée de l'emplacement.
 * @param  Tile*  Tuile à tester.
 * @return bool   true si l'emplacement est disponible, false sinon.
 **/
bool boardPieceFits(const Board*, int x, int y, Tile* tile);

/**
 * @brief  Calcule les index des voisins de l'emplacement (x,y).
 * @param  int*   Tableau des index des voisins, modifié par effet
 *                de bord.
 * @param  Board* Plateau de jeu.
 * @param  int    Abscisse de l'emplacement.
 * @param  int    Ordonnée de l'emplacement.
 **/
void boardGetAdjacentTilesPos(int* posTiles, const Board* b, int x, int y);

/**
 * @brief  Calcule les index des tuiles aux coins de l'emplacement (x,y).
 * @param  int*   Tableau des index des voisins, modifié par effet
 *                de bord.
 * @param  Board* Plateau de jeu.
 * @param  int    Abscisse de l'emplacement.
 * @param  int    Ordonnée de l'emplacement.
 **/
void boardGetDiagonalTilesPos(int* posTiles, const Board* b, int x, int y);

/**
 * @brief  Donne un tableau des pointeurs vers les tuiles placées aux
 *         emplacements voisins de l'emplacement (x,y).
 * @param  Tile** Tableau des pointeurs des tuiles.
 *                de bord.
 * @param  Board* Plateau de jeu.
 * @param  int    Abscisse de l'emplacement.
 * @param  int    Ordonnée de l'emplacement.
 **/
void boardGetAdjacentTiles(Tile**, const Board*, Tile*);

/**
 * @brief  Donne un tableau des pointeurs vers les tuiles placées aux
 *         coins de l'emplacement (x,y).
 * @param  Tile** Tableau des pointeurs des tuiles.
 *                de bord.
 * @param  Board* Plateau de jeu.
 * @param  int    Abscisse de l'emplacement.
 * @param  int    Ordonnée de l'emplacement.
 **/
void boardGetDiagonalTiles(Tile**, const Board*, Tile*);

/**
 * @brief  Détermine si les abbayes jusqu'ici inachevées sont terminées, et
 *         met à jour les scores le cas échéant.
 * @param  Board* Plateau de jeu.
 **/
void checkUnfinishedAbbey(Board* b, size_t* nbPoints, size_t* nbPans, bool endGame);

/**
 * @brief Propage l'information contenue dans la cellule donnée en parcourant le
 *        graphe interne de la tuile donnée.
 * @param Tile* La tuile à mettre à jour.
 * @param TileCell* La cellule de référence, dont on souhaite propager l'information.
 **/
void propagIntern(Tile* tile, TileCell* c);

/**
 * @brief Propage l'information contenue dans la tuile donnée vers les tuiles
 *        connectées du plateau de jeu donné en parcourant celui-ci.
 * @param Board* Le plateau de jeu.
 * @param Tile* La tuile à partir de laquelle on propage.
 * @param GHashTable* L'ensemble des tuiles déjà visitées.
 **/
void propag(const Board* b, Tile* tile, GHashTable* visited);

/**
 * @brief  Vérifie si une structure est terminée (cités, routes).
 **/
bool fieldIsOver(Board*, Tile* tile, int position, GHashTable* visited, int* points, bool suppPawn, int field);

/**
 * @brief Propage l'information de la tuile donnée vers ses voisins.
 * @param Board* Le plateau de jeu.
 * @param Tile* La tuile à partir de laquelle on propage.
 **/
void actuTile(const Board *b, Tile* tile);

/**
 * @brief Détermine le(s) gagnant(s) du nombre donné de points engendrés par
 *        la cellule et met à jour les scores et les nombres globaux de
          partisans des joueurs concernés.
 * @param TileCell* La cellule concernée.
 * @param size_t* nbPawns Le tableau des nombres de partisans de chaque joueur.
 * @param size_t* nbPoints Le tableau des scores de chaque joueur.
 * @param int Le nombre de points engendrés par la cellule.
 **/
void playerCalculatePoints(TileCell* cell, size_t* nbPawns, size_t* nbPoints, int points);

/**
 * @brief Retire les pions posés sur la tuile donnée dans les villes, dans les
 *        cellules d'indices impairs.
 * @param Board* Le plateau de jeu.
 * @param Tile* La tuile d'où l'on souhaite retirer les pions.
 **/
void suppressPawn(Board* b,Tile* tile);

/**
 * @brief Libère proprement la mémoire allouée pour le plateau de jeu.
 * @param Board* Le plateau de jeu.
 **/
void boardFree(Board*);

#endif
