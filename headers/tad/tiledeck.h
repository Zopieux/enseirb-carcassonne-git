#ifndef _TILE_DECK_H_
#define _TILE_DECK_H_

/*
TAD pour une pioche de tuiles.
*/

#include <stdlib.h>
#include <stdbool.h>

#include <common.h>
#include <tad/tile.h>


typedef struct TileDeck_s {
	Tile** stack;
	size_t size;
	size_t head;
} TileDeck;


/**
 * @brief  Crée et remplit la pioche avec le tableau de Tile* donné en paramètre.
 * @param  size_t La taille de la pioche.
 * @return TileDeck* La pioche créée.
 **/
TileDeck* tileDeckInit(Tile*, size_t);

/**
 * @brief  Retourne le nombre de tuiles dans la pioche initiale.
 * @param  TileDeck* La pioche.
 * @return size_t    Le nombre de tuiles initiales.
 **/
size_t tileDeckCapacity(const TileDeck*);

/**
 * @brief  Retourne le nombre de tuiles dans la pioche.
 * @param  TileDeck* La pioche.
 * @return size_t    Le nombre de tuiles restantes.
 **/
size_t tileDeckSize(const TileDeck*);

/**
 * @brief  Mélange la pioche en place, en laissant les premières tuiles
 *         inchangées de place.
 * @param  TileDeck* La pioche.
 * @param  size_t    Le nombre de tuiles en tête qui doivent rester inchangées.
 **/
void tileDeckShuffle(TileDeck*, size_t);

/**
 * @brief  Pioche (retire) et retourne la première tuile de la pioche.
 * @param  TileDeck* La pioche.
 * @return Tile*     La tuile piochée.
 **/
Tile* tileDeckPick(TileDeck*);

/**
 * @brief  Retourne la prochaine tuile piochée sans la retirer.
 * @param  TileDeck* La pioche.
 * @return Tile*     La tuile piochée.
 **/
Tile* tileDeckSeek(const TileDeck*);


void tileDeckFree(TileDeck*);

/**
 * @brief  Retourne un entier indiquant si la pioche est vide.
 * @param  TileDeck* La pioche
 * @return bool
 **/
bool tileDeckEmpty(TileDeck*);


#endif
