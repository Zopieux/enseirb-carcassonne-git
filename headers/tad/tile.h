#ifndef _TILE_H_
#define _TILE_H_

/*
TAD pour les tuiles (pièces composant la carte).
*/

#include <stdio.h>
#include <stdbool.h>

#include <common.h>
#include <tad/ugraph.h>

typedef struct TileShape_s {
	enum TILE_TYPE type;
	enum AREA_TYPE areas[CELL_GEO_LAST];
	unsigned int bonus;
	unsigned int uid;
	size_t tileCount;
	UGraph* connections;
	UGraph* farmAndCity;
} TileShape;

typedef struct TileCell_s {
	size_t playerPawnCount[NB_PLAYERS_MAX];
	size_t hasPawnOfPlayer;
	size_t cptPoint;
	size_t position;
} TileCell;

typedef struct Tile_s {
	TileShape* shape;
	TileCell cells[CELL_GEO_LAST];
	int rotation;
	int x, y;
	size_t playerOnSpecial;
} Tile;


/**
 * @brief  Initialise une tuile déjà allouée, à partir d'une TileShape.
 * @param  Tile*      Pointeur vers l'espace alloué pour la tuile.
 * @param  TileShape* La forme dont la tuile est issue.
 * @return Tile*      La tuile passée en paramètre.
 **/
Tile* tileInit(Tile*, TileShape*);

/**
 * @brief  Retourne la shape d'une tuile.
 * @param  Tile*      La tuile.
 * @return TileShape* La forme dont elle est issue.
 **/
TileShape* tileGetShape(const Tile*);

/**
 * @brief  Lecture du fichier de définition des tuiles et construction de
 *         celles-ci.
 * @param  FILE*      Buffer d'où lire les définitions (cf. format dans
 *                    ressources/tile_def.format).
 * @param size_t*     Pointeur vers le nombre de shapes (formes uniques).
 * @param size_t*     Pointeur vers le nombre de tuiles (toutes formes).
 * @param TileShape** Pointeur vers le tableau contenant les TileShape.
 * @param Tile**      Pointeur vers le tableau contenant les Tile.
 **/
void tileParseDef(FILE*, size_t*, size_t*, TileShape**, Tile**);

/**
 * @brief  Met à jour la rotation inhérente à une tuile.
 * @param  Tile* La tuile à modifier.
 * @param  int   La rotation de 90° horaire (entre 0 et 3) à appliquer.
 * @param  bool  Vrai si la rotation est relative, faux si absolue.
 **/
void tileSetRotation(Tile*, int rot, bool relative);

/**
 * @brief  Retourne la position opposée à la direction donnée
 *         (eg. NORTH -> SOUTH).
 * @param  CELL_GEO La direction dont on veut l'opposée.
 * @return CELL_GEO La direction opposée.
 **/
enum CELL_GEO tileGetOppositeGeo(enum CELL_GEO);

/**
 * @brief  Lecture du type de terrain d'une tuile pour une direction donnée.
 * @param  Tile*     La tuile à lire.
 * @param  CELL_GEO  La direction où se trouve le terrain.
 * @return AREA_TYPE Le type de terrain lu.
 **/
enum AREA_TYPE tileGetFieldForGeo(const Tile*, enum CELL_GEO);

/**
 * @brief  Obtention de la cellule dans une tuile pour une direction donnée.
 * @param  Tile*     La tuile à lire.
 * @param  CELL_GEO  La direction où se trouve le terrain.
 * @return TileCell* La cellule recherchée.
 **/
TileCell* tileGetCellForGeo(const Tile*, enum CELL_GEO);

/**
 * @brief  Écrit les trois cellules bordant la tuile donnée dans la direction
 *         donnée.
 * @param  TileCell** Pointeur vers un tableau de trois TileCell*.
 * @param  Tile*      Tuile concernée.
 * @param  CELL_GEO   Direction.
 * @param  bool       Vrai si l'ordre des trois cellules doit être inversé.
 **/
void tileGetSideCellsForGeo(TileCell**, const Tile*, enum CELL_GEO, bool miror);

/**
 * @brief  Vérifie que la première tuile peut être posée à côté de la seconde
 *         tuile, pour la direction donnée.
 * @param  Tile*     La première tuile.
 * @param  Tile*     La seconde tuile.
 * @param  CELL_GEO  La direction où se situe la seconde tuile par rapport à la
 *                   première.
 * @return bool
 **/
bool tileSideFits(const Tile* first, const Tile* other, enum CELL_GEO);

/**
 * @brief  Vérifie si un péon peut être posé sur une direction donnée.
 **/
bool tileCanAddPawnAtGeo(Tile* tile, size_t nbPlayers, enum CELL_GEO);

/**
 * @brief  Vérifie si un péon peut être posé sur n'importe quelle des directions
 *         d'une tuile.
 **/
bool tileCanAddPawn(Tile* tile, size_t nbPlayers);

/**
 * @brief  Vérifie qu'au sein d'une tuile, deux directions sont connexes.
 **/
bool tileGeoAreConnected(Tile* tile, enum CELL_GEO a, enum CELL_GEO b);

bool tileGeoTabConnections(Tile* tile, enum CELL_GEO tabGeo[TILE_NB_NEIGHBOURS], enum CELL_GEO geo);

#endif
