#ifndef _GUI_RESSOURCE_H_
#define _GUI_RESSOURCE_H_

#include <stdbool.h>
#include <GL/gl.h>
#include <SDL/SDL_ttf.h>

/**
 * @brief  Charge une texture.
 * @param  GLuint* Pointer vers le GLuint correspondant.
 * @param  char*   Chemin vers le fichier.
 * @param  bool    Vrai si l'image possède un canal alpha (eg. PNG).
 * @return bool    Vrai si le chargement est un succès.
 **/
bool guiResLoadTexture(GLuint*, const char*, const bool);

/**
 * @brief  Charge une police TrueType.
 * @param  char* Chemin vers le .ttf.
 * @param  int   Taille du texte en points.
 **/
TTF_Font* guiResLoadFont(const char* fontName, int ptSize);

/**
 * @brief  Libère une police chargée en mémoire.
 * @param  TTF_Font*
 **/
void guiResFreeFont(TTF_Font* font);

/**
 * @brief  Dessine un texte avec la police donnée, dans la couleur donnée,
 *         et met à jour w et h avec les dimensions du texte dessiné.
 * @param  TTF_Font* Police à utiliser.
 * @param  SDL_Color Couleur à utiliser.
 * @param  int*      Pointeur vers la largeur du texte.
 * @param  int*      Pointeur vers la hauteur du texte.
 * @param  char*     Texte à dessiner.
 **/
void guiResRenderText(TTF_Font*, const SDL_Color, int* w, int* h, const char* text);

#endif
