#ifndef _GUI_UTILS_H_
#define _GUI_UTILS_H_

/**
 * @brief  Dessine les lignes nécessaires à la construction d'une grille.
 * @param  uint Espacement des lignes.
 * @param  uint Largeur de l'affichage.
 * @param  uint Longueur de l'affichge.
 **/
void drawGrid(int resolution, int width, int height);

/**
 * @brief  Dessine un rectangle texturé.
 **/
void guiDrawQuad(int width, int height, float texture_tl, float texture_tr, float texture_bl, float texture_br);

/**
 * @brief  Dessine un texte à l'écran.
 **/
void guiDrawText(int textW, int textH);

#endif
