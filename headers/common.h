#ifndef _COMMON_H_
#define _COMMON_H_

/*
Definition des constantes utilisées dans tout le projet.
*/

#ifndef KEY_ESCAPE
#define KEY_ESCAPE 27
#endif

#define NB_PLAYERS_MIN 2
#define NB_PLAYERS_MAX 6
// Nombre de pions posables par chaque joueur
#define NB_PAWN 7
#define AREA_NB_NEIGHBOURS 4
#define TILE_NB_NEIGHBOURS 4
#define TILE_SIDE_NB_CELLS 3
#define PLAYER_EMPTY NB_PLAYERS_MAX + 1

enum AREA_TYPE {
	FARM=0, CASTLE, ROAD, ABBEY, RIVER, CROSS, AREA_TYPE_LAST
};

enum TILE_TYPE {
	TILE_NORMAL=0, TILE_ABBEY, TILE_TYPE_LAST
};

enum CELL_GEO {
	NORTH=0, NORTHEAST, EAST, SOUTHEAST,
	SOUTH, SOUTHWEST, WEST, NORTHWEST,
	CELL_GEO_LAST
};

static const enum CELL_GEO
indexToSideGeo[4] = {NORTH, EAST, SOUTH, WEST};

static const enum CELL_GEO
indexToDiagGeo[4] = {NORTHEAST, SOUTHEAST, SOUTHWEST, NORTHWEST};

static const enum CELL_GEO
indexToBothGeo[CELL_GEO_LAST] = {
	NORTH, EAST, SOUTH, WEST,
	NORTHEAST, SOUTHEAST, SOUTHWEST, NORTHWEST
};

static const int guiTileCellOffset[CELL_GEO_LAST + 1][2] = {
	{0, -1}, {1, -1}, {1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0}, {-1, -1},
	{0, 0} // CELL_GEO_LAST -> center
};

#endif
