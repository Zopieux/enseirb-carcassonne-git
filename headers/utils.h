#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdlib.h>
#include <glib.h>

// Helpers
#define GFOREACH(item, list) for(GList *__glist = list; __glist && (item = __glist->data, true); __glist = __glist->next)


/**
 * @brief  Initialise le générateur pseudo-aléatoire avec une graine temporelle.
 **/
void randomSeed(void);

/**
 * @brief  Computes a random (signed) integer between a and b, inclusive.
 * @param  a int Lower bound
 * @param  b int Upper bound
 * @return The said random integer.
 **/
int randomInt(int, int);

/**
 * @brief  Computes a random size_t between a and b, inclusive.
 * @param  a size_t Lower bound
 * @param  b size_t Upper bound
 * @return The said random size_t.
 **/
size_t randomSizet(size_t, size_t);

/**
 * @brief  Effectue un modulo avec un résultat prévisible pour les négatifs, càd
 *         x % m est dans [-(m - 1), m - 1]
 *         Exemple : -1 % 2 = 1 et -2 % 2 = 0
 * @param  int L'entier sur lequel effectuer le modulo.
 * @param  int Le modulo lequel est faite l'opération.
 * @return int
 **/
int modulo(int x, int m);

/**
 * @brief  Calcule la puissance de deux la plus proche (par valeur supérieure)
 *         de l'entier donné.
 * @param  int
 ** @return int
 */
int powerToSup(int);

/**
 * @brief  Maximum de deux entiers.
 **/
int intMax(int, int);

/**
 * @brief  Minimum de deux entiers.
 **/
int intMin(int, int);

#endif
