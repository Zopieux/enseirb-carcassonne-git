#ifndef _COLORS_H_
#define _COLORS_H_

unsigned int colorRGB(unsigned char r, unsigned char g, unsigned char b);
unsigned char colorGetRValue(unsigned int color);
unsigned char colorGetGValue(unsigned int color);
unsigned char colorGetBValue(unsigned int color);

void colorRGBtoHSL(unsigned int color, unsigned int* h, unsigned int* s, unsigned int* l);
unsigned int colorHSLtoRGB(const unsigned int h, const unsigned int s, const unsigned int l);
unsigned int colorBrighten(unsigned int color, unsigned int amount);
unsigned int colorDarken(unsigned int color, unsigned int amount);

#endif
