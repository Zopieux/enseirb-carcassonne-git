\subsection{Structures et types abstraits de données}
Notons que les structures sont définies la plupart du temps dans les \emph{headers} plutôt que dans
l'implémentation. Ce choix est discutable car cela expose le fonctionnement interne des TAD.
Ceci dit, pouvoir accéder directement aux membres des structures est plus pratique en terme de
rapidité de développement: il est inutile d'écrire des \emph{getters} et \emph{setters} pour
chaque membre. Bien entendu, avec davantage de temps, il aurait fallu privilégier la séparation
entre API et fonctionnement interne pour faciliter les tests et rendre le code plus clair.

\subsubsection*{Le graphe non orienté: \texttt{ugraph.c}}
Un graphe non orienté possède un nombre fixé de nœuds (\texttt{vertexCount})
représentés par leur numérotation (de $0$ à $\texttt{vertexCount} - 1$).
Pour représenter les arêtes existant entre les sommets, nous avons décidé
d'utiliser une matrice d'adjacence, c'est-à-dire une matrice triangulaire de
booléens, de taille $\texttt{vertexCount} \times \texttt{vertexCount}$, définie par
$M[i,j]:=(i,j)\in E$ avec nécessairement $i < j$ (choix arbitraire).
Cette représentation est intéressante car elle permet, en temps constant,
d'ajouter et supprimer une arête et de consulter la présence d'une arête.

\begin{verbatim}
typedef struct UGraph_s {
    size_t vertexCount;
    bool* edgeMatrix;
} UGraph;

typedef unsigned int Vertex;
\end{verbatim}

L'axiomatique suivante est respectée (si $n \geq 1$):
\begin{itemize}
\item $\texttt{ugraphSize}(\texttt{ugraphInit}(n)) = n$
\item $\forall e \in \llbracket 0, n-1 \rrbracket, \texttt{ugraphEdgeExists}(\texttt{ugraphInit}(n), e) = faux$
\item $\forall e \in \llbracket 0, n-1 \rrbracket, \texttt{ugraphEdgeExists}(\texttt{ugraphAddEdge}(\texttt{ugraphInit}(n), e), e) = vrai$
\item $\forall e \in \llbracket 0, n-1 \rrbracket, \forall v \in \llbracket 0, n -1 \rrbracket \setminus \{e\},\\ \texttt{ugraphEdgeExists}(\texttt{ugraphAddEdge}(\texttt{ugraphInit}(n), e), v) = faux$
\item S'il existe dans $G$ un chemin entre les sommets $e$ et $v$, $\texttt{ugraphPathExists(G, e, v) = vrai}$
\end{itemize}

La fonction \texttt{ugraphPathExists}, qui teste l'existence d'un chemin entre deux arêtes, utilise un simple
parcours en style itératif, avec l'aide d'un ensemble (\emph{hashtable}) de sommets visités et d'une file de
sommets en attente de traitement.

\subsubsection*{Le plateau de jeu: \texttt{board.c}}
\begin{verbatim}
typedef struct Board_s {
    GHashTable* tileMap;
    GHashTable* availablePos;
    GHashTable* currentAvailablePos;
    GHashTable* unfinishedAbbey;
    GHashTable* pawnPositions;
    size_t onBoardTiles;
    size_t maxTiles;
} Board;
\end{verbatim}

Le plateau de jeu contenant énormément de méthodes, nous détaillerons uniquement l'utilité
de chacun des membres de la structure \texttt{Board}.

\begin{itemize}
\item \texttt{tileMap} est une table de hachage entre un entier représentant des coordonnées $(x, y$) et une tuile (\texttt{Tile*}). C'est le moyen d'accéder aux nœuds du graphe représentant le plateau de jeu.
\item \texttt{availablePos} est un \emph{cache} des positions où le joueur peut placer la tuile
actuellement en main. Il permet à l'interface graphique de suggérer les emplacements valides à l'utilisateur.
La table de hachage est utilisée ici comme un ensemble de positions: les clés sont des entiers
distincts représentant des coordonnées et les valeurs sont sans importance.
\item \texttt{currentAvailablePos} est un \emph{cache} des rotations valides de la tuile en cours, pour chaque
position de \texttt{availablePos}. Les clés sont des entiers représentant des coordonnées et les valeurs sont
des \emph{flags}, c'est-à-dire une somme de puissances de deux avec $2^0 \rightarrow \text{rotation 0°}$,
$2^1 \rightarrow \text{rotation 90°}$,
$2^2 \rightarrow \text{rotation 180°}$ et $2^3 \rightarrow \text{rotation 270°}$. Si pour une position $p$ donnée,
aucune rotation de la tuile n'est valide, alors \texttt{currentAvailablePos} contiendra $p \rightarrow 0$.
Si pour une position $q$, les rotations $90°$ et $180°$ sont valides, alors \texttt{currentAvailablePos} contiendra
$q \rightarrow 2^1 + 2^2 = 6$.
\item \texttt{unfinishedAbbey} est un \emph{cache} des tuiles de type abbaye qui ne sont pas (encore)
entourées de huit tuiles. Cela permet d'itérer uniquement sur les abbayes non terminées au moment de
compter les points (après l'ajout de chaque tuile). La table de hachage est là encore utilisée comme un ensemble de tuiles distinctes.
\item \texttt{pawnPositions} est un \emph{cache} des pions restant sur le plateau de jeu en fin de partie. Il permet alors de compter les points de ces pions rapidement, en récupérant leur position, puis en leur appliquant le traitement nécessaire.
\item \texttt{onBoardTiles} contient le nombre de tuiles déjà posées sur le plateau. Cette information pourrait
être calculée à partir de la taille de la pioche mais cela pose des problèmes, notamment lorsque des tuiles
sont éliminées de la pioche parce qu'elles ne peuvent pas être posées sur aucun emplacement au moment où
elles sont piochées\footnote{Cette situation est rare mais possible, par exemple pour les tuiles
dont tous les bords sont de type ``cité'': si aucune autre tuile déjà posée n'expose de cité, il est impossible de la poser.}.
\item \texttt{maxTiles} contient le nombre de tuiles au départ du jeu, tel que déterminé par le \emph{parser} de 
définitions.
\end{itemize}

\subsubsection*{La forme, la tuile et la cellule: \texttt{tile.c}}
\texttt{CELL\_GEO\_LAST} est le nombre de cellules au sein d'une tuile, en l'occurrence 8.

\begin{verbatim}
typedef struct TileShape_s {
    enum TILE_TYPE type;
    enum AREA_TYPE areas[CELL_GEO_LAST];
    unsigned int bonus;
    unsigned int uid;
    size_t tileCount;
    UGraph* connections;
    UGraph* farmAndCity;
} TileShape;
\end{verbatim}
Cette structure est la version symbolique des formes qui sont décrites dans le fichier de définition des tuiles (cf. partie sur le \emph{parser}).
Le graphe \texttt{farmAndCity} n'est pas utilisé; il aurait dû servir à relier les villes et les champs au sein
d'une tuile afin de compter les points en fin de partie\footnote{En effet, les règles stipulent qu'un joueur remporte des points pour chaque ville terminée qui est adjacente à un de ses champs.}, mais nous n'avons pas eu
le temps d'implémenter cette règle.


\begin{verbatim}
typedef struct Tile_s {
    TileShape* shape;
    TileCell cells[CELL_GEO_LAST];
    int rotation;
    int x, y;
    size_t playerOnSpecial;
} Tile;
\end{verbatim}
Là encore, les \texttt{Tile*} sont directement issues du \emph{parser} qui crée \texttt{tileCount} tuiles pour
chaque forme. Les champs \texttt{x} et \texttt{y} ont été
rajoutés à la structure originale; ils ne sont \emph{a priori} pas utiles puisque déductibles de
\texttt{tileMap} mais s'avèrent faciliter l'écriture en C de certains algorithmes.

Dans le cas où la tuile est de type non-normale (par exemple, ``abbaye''), le champ \texttt{playerOnSpecial} contient un identifiant de joueur ou la valeur spéciale \texttt{PLAYER\_EMPTY}. Elle permet de déterminer
si un joueur a placé un pion sur une tuile spéciale pour exécuter les traitements appropriés.

\begin{verbatim}
typedef struct TileCell_s {
    size_t playerPawnCount[NB_PLAYERS_MAX];
    size_t hasPawnOfPlayer;
    size_t cptPoint;
    size_t position;
} TileCell;
\end{verbatim}
\begin{figure}[h!]
\centering
\includegraphics[scale=0.8]{figs/cells}
\end{figure}
Le nombre de pions pour chaque joueur est contenu dans \texttt{playerPawnCount}. Ce nombre de pions est à comprendre en terme d'\og influence \fg{}, pas de présence physique. En d'autre termes, si un joueur a posé un
pion physique sur une cellule $a$ d'une tuile $A$, et que ce champ s'étend jusqu'à la cellule $b$ d'une autre tuile $B$ (rond rouges), alors $a$ et $b$ contiendront le même \texttt{playerPawnCount}. La présence physique d'un pion est
représentée par \texttt{hasPawnOfPlayer} qui contient soit un identifiant de joueur, soit \texttt{PLAYER\_EMPTY} dans
le cas où aucun joueur n'a posé de pion sur cette cellule. Pour reprendre l'exemple, $a.\texttt{hasPawnOfPlayer}$ sera
l'identifiant d'un joueur alors que $b.\texttt{hasPawnOfPlayer}$ sera \texttt{PLAYER\_EMPTY} (icône de pion sur $a$, pas sur $b$).


\subsubsection*{La pioche de tuiles: \texttt{tiledeck.c}}
\begin{verbatim}
typedef struct TileDeck_s {
    Tile** stack;
    size_t size;
    size_t head;
} TileDeck;
\end{verbatim}
La pioche de tuiles est une simple pile qui présente la particularité d'être mélangeable.
Elle suit donc l'axiomatique classique d'une pile (\texttt{seek}, \texttt{stack}, \texttt{unstack}),
à quoi il faut rajouter la fonction de mélange sur-place (\texttt{shuffle}) qui ordonne aléatoirement les tuiles.

Notre implémentation travaille sur un tableau de pointeurs vers des \texttt{Tile*}.
