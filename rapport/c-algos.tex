\subsection{Détail de certains algorithmes}

Il est intéressant de s'arrêter sur l'implémentation de quelques programmes.
Nous avons choisi de présenter le \emph{parser} de définition des tuiles ainsi
que certains algorithmes propres au jeu et qui ont été abordés dans la partie
algorithmique.

\subsubsection*{\emph{Parser} de définition des tuiles}
L'élément de base de Carcassonne est la tuile, ce petit carré de carton
qui comporte un certain nombre de terrains différents.
Pour faciliter la définition de ces données importantes, nous avons choisi,
plutôt que d'écrire \emph{en dur} une structure C par forme, d'inventer
un format simple pour représenter chaque forme puis développer un \emph{parser}
qui lit ce fichier externe et crée les tuiles en conséquence.
Cette fonction est intéressante puisqu'elle touche à l'\emph{Input/Output} tout
en utilisant un système de machine à états (automate).

Le format du fichier est décrit précisément dans \texttt{ressources/tile\_def.format}.
Il s'agit d'un format texte ASCII plutôt basique qui privilégie la facilité de
traitement à la lisibilité par un humain. La figure \ref{fig:tileshape-ascii}
illustre ce format sur la tuile initiale du jeu.

\begin{figure}[ht]
\hspace*{-9em}
\centering
\begin{minipage}{0.25\textwidth}
    \centering
    \includegraphics[scale=0.35]{figs/starttile}
\end{minipage}
\begin{minipage}{0.64\textwidth}
    \centering
	\begin{verbatim}
4       # il y a quatre exemplaires de cette forme
N 0     # c'est une carte « normale » et sans bonus (0)
CRFR    # du nord à l'ouest dans le sens horaire, les terrains sont
        # Cité, Route, champ (Farm), Route
4       # il y a quatre arêtes dans le graphe qui suit
EW      # est et ouest sont reliés
nw      # nord-est et nord-ouest sont reliés
Se      # sud et sud-est sont reliés
Ss      # sud et sud-ouest sont reliés
	\end{verbatim}
\end{minipage}
\caption{Exemple d'une définition de forme}
\label{fig:tileshape-ascii}
\end{figure}

Le \emph{parser} lit le fichier par blocs de $N$ caractères avec la fonction \texttt{fgets}.
Il est possible d'insérer des commentaires avec le caractère \#, dans ce cas
la fonction ignore la ligne. Si une ligne est plus grande que les $N$ caractères,
le \emph{parser} ignore ce qu'il lit jusqu'à trouver un marqueur de fin de ligne.
Il n'y a donc pas de risque de faire échouer la fonction avec un \emph{buffer
overflow}.
Pour être robuste contre les erreurs de syntaxe, la fonction utilise un principe
d'automate (cf. figure \ref{fig:parser-automata}). Chaque information (nombre d'exemplaires, type et bonus, etc.) correspond
à un état de lecture qui possède un état suivant (par exemple, le suivant de
\og type et bonus \fg{} est \og type des terrains \fg{}). Si par malheur le
\emph{parser} lit une information qui ne correspond à l'état théorique dans
lequel il devrait se trouver, il annonce une erreur adaptée au contexte et s'arrête.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\textwidth]{figs/tiledefs}
	\caption{Fonctionnement du \emph{parser} de formes}
	\label{fig:parser-automata}
\end{figure}

\subsubsection*{Vérification de la pose d'une tuile}
Nous pouvons à présent comparer un des algorithmes de la partie précédente avec la réalisation effective en C. Prenons le problème \emph{peutPoserTuile}, qui correspond dans l'implémentation à la fonction \emph{boardPieceFits}.
\begin{lstlisting}
bool
boardPieceFits(const Board* b, int x, int y, Tile* tile) {
    if(!boardPositionIsAvailable(b, x, y))
		return false;

	tile->x = x;
	tile->y = y;

	Tile* adjacentTiles[TILE_NB_NEIGHBOURS];
	boardGetAdjacentTiles(adjacentTiles, b, tile);

	for(int i = 0; i < TILE_NB_NEIGHBOURS; i++) {
		if(!tileSideFits(tile, adjacentTiles[i], indexToSideGeo[i]))
			return false;
	}

	return true;
}
\end{lstlisting}

On peut observer rapidement que la structure est la même, constituée en premier lieu d'un test permettant de savoir si un emplacement est disponible (avec \emph{boardPositionIsAvailable}, puis d'une boucle sur les 4 voisins de la tuile, stockés au préalable dans un tableau de tuiles. Le dernier test de compatibilité entre deux tuiles est réalisé grâce à la fonction \emph{tileSideFits}.
