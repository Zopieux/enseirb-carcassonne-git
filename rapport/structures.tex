\subsection{Structures utilisées}

Cette partie définit les structures abstraites utilisées pour représenter les objets et concepts du jeu de Carcassonne.

\subsubsection*{Définitions préliminaires}

Certaines des définitions qui suivent sont tirées des polycopiés des cours "Structures arborescentes" et "Graphes et algorithmes" de M. Lapoire Denis ainsi que du polycopié du cours "Algorithmique et Structures de Données" de Mme Desainte-Catherine Myriam.

Un \srefdef{type abstrait de données} est un objet qui se situe entre les objets mathématiques très abstraits et les objets informatiques concrets (tels que les \sref{tableau}x qui correspondent simplement à des zones mémoires). Un type abstrait de données est destiné à être implémenté dans le code d'un programme.

Un \srefdef{ensemble} est un \sref{type abstrait de données} qui contient des \emph{éléments} (qui peuvent être de n'importe quel type). Soit $e_1$, $e_2$ et $e_3$ trois éléments constituant l'ensemble $E$, alors on note $E = \{e_1, e_2, e_3\}$. Ici, $e_2$ appartient à $E$, ce que l'on note $e_2 \in E$. L'unique ensemble qui ne contient aucun élément est noté $\emptyset$. La \emph{cardinalité} d'un ensemble $E$ correspond au nombre d'éléments appartenant à $E$. Une \emph{paire} est un ensemble de cardinalité 2. Un ensemble est dit \emph{infini} lorsque sa cardinalité est égale à $+\infty$.

Le \srefdef{produit cartésien} de deux ensembles $A$ et $B$ est l'ensemble noté $A \times B$ ayant pour éléments tous les couples $(a, b)$ avec $a \in A$ et $b \in B$.

Une \srefdef{relation binaire} $r$ définie sur $A$ et $B$ est une partie du produit cartésien $A \times B$.

Une \srefdef{fonction} $f$ d'un ensemble $A$ à un ensemble $B$ est une \sref{relation binaire} $r \subseteq A \times B$ telle que pour tout élément $a \in A$, il existe un unique élément $b \in B$ tel que $(a, b) \in f$. L'appartenance $(a, b) \in f$ est notée $f(a) = b$.

Une \srefdef{bijection} $f$ d'un ensemble $A$ à un ensemble $B$ est une fonction de $A$ à $B$ telle que, pour tout élément $b \in B$, il existe un unique élément $a \in A$ tel que $f(a) = b$.

Une \srefdef{séquence} à valeurs dans un ensemble $A$ est une fonction de la forme $s : \mathbb{N}^{*} \rightarrow A$ ou de la forme $s : [1, i] \rightarrow A$ avec $i \in \mathbb{N}$. Dans ce dernier cas, la séquence $s$ peut être notée $(s(1), ..., s(i))$. L'entier $i$, qui peut être nul, est la longueur de $s$. La séquence vide, de longueur 0, est notée $()$.

Un \srefdef{$n$-uplet} (avec $n \in \mathbb{N}$) est une séquence de longueur $n$. Un \emph{couple} est un 2-uplet, un \emph{triplet} est un 3-uplet et un \emph{quadruplet} est un 4-uplet.

Un \srefdef{tableau} est une séquence finie d'éléments de même type permettant un accès direct à chaque élément en fonction de son indice.

Un \srefdef{graphe simple non orienté} $G$ est un triplet $(V, E, f)$ où :
\begin{itemize}
    \item $V$ est un ensemble de \emph{sommets} ;
    \item $E$ est un ensemble d'\emph{arêtes} ;
    \item $f$ est une fonction qui associe à chaque arête $e \in E$ une paire de sommets $\{s, t\}$ de $V$ : on appelle \emph{extrémités} de $e$ les sommets $s$ et $t$.
    \item Pour tous sommets $s$ et $t$, il existe au plus une arête $e \in E$ telle que $f(e) = \{s,t\}$.
\end{itemize}

Dans un graphe non orienté $G = (V, E, f)$, deux sommets $s \in V$ et $t \in V$ sont \srefdef{adjacents} si et seulement s'il existe une arête $e \in E$ telle que $f(e) = \{s, t\}$.

Un \srefdef{chemin} dans un graphe $G = (V, E, f)$ non orienté est une séquence $w$ de la forme $(s_1, e_1, ..., e_l, s_{l+1})$ où, pour tout $i \in [1, l]$, $e_i$ est une arête ayant pour extrémités les sommets $s_i$ et $s_{i+1}$. On dit que le chemin va de $s_1$ à $s_{l+1}$. L'entier $l$, aussi noté $|w|$, est appelé \emph{longueur} de $w$. Une \emph{boucle} dans $G$ est un chemin $(s, e, s)$ avec $s \in V$ et $e \in E$.

Soit un graphe $G = (V, E, f)$ non orienté. On dit qu'un sommet $t \in V$ est \srefdef{accessible} à partir d'un sommet $s \in V$ s'il existe un chemin allant de $s$ à $t$ dans $G$. Un graphe est dit \srefdef{connexe} si, pour tout couple de sommets $(s, t) \in V^2$, $t$ est accessible à partir de $s$.

On appellera \srefdef{coordonnées} tout couple $(x, y) \in \mathbb{Z}^2$.

Une \srefdef{grille infinie} $G$ est un graphe $(V, E, f)$ tel que :
\begin{itemize}
    \item $V$ et $E$ sont de cardinalité infinie ;
    \item Chaque sommet $s \in V$ est identifié par ses coordonnées $(x, y)$ : on dit que $s$ est de coordonnées $(x, y)$ ;
    \item $E$ est construit de façon à ce que, pour tout sommet $s \in V$ de coordonnées $(x, y)$, les arêtes $e_1$, $e_2$, $e_3$ et $e_4$ telles que $f(e_1) = \{s, b_1\}$, $f(e_2) = \{s, b_2\}$, $f(e_3) = \{s, b_3\}$ et $f(e_4) = \{s, b_4\}$ appartiennent à $E$ si et seulement si les sommets $b_1$, $b_2$, $b_3$ et $b_4$ sont respectivement de coordonnées $(x+1, y)$, $(x-1, y)$, $(x, y-1)$ et $(x, y+1)$.
\end{itemize}

Une \srefdef{chaîne de caractères} est une séquence $s$ à valeurs dans l'ensemble des caractères (un caractère est un signe d'écriture, par exemple $'a'$ ou $'*'$) et de longueur $i$ définie. En pratique, la chaîne de caractères $(s(1), ..., s(i))$ est notée $"s(1)...s(i)"$ : par exemple, "abcde" est la chaîne de caractères $('a', 'b', 'c', 'd', 'e')$.

Un \srefdef{pion} est une entité autonome qui appartiendra à un \sref{joueur} et qui pourra être posé sur une \sref{tuile}.

Un \srefdef{joueur} est un triplet $(n, E_{Pions}, s)$ où :
\begin{itemize}
    \item $n$ est une \sref{chaîne de caractères} qui identifie le joueur, dite \emph{nom} du joueur ;
    \item $E_{Pions}$ est un ensemble de \sref{pion}s appartenant au joueur ;
    \item $s \in \mathbb{N}$ est un nombre, que l'on appellera \emph{score} du joueur.
\end{itemize}

%est une chaîne de caractères contenant son nom. Exemple: $j_1 = ``Nabila"$

Une \srefdef{pioche} est une séquence à valeurs dans un ensemble de \sref{tuile}s.

On note
$\mathcal{D}_4 \overset{def}{=} \left\{\text{nord}, \text{ouest}, \text{sud}, \text{est}\right\}$ l'ensemble des quatre directions de la rosace des vents et
$\mathcal{D}_8 \overset{def}{=} \mathcal{D}_4 \cup \left\{\text{nord-ouest}, \text{sud-ouest}, \text{sud-est}, \text{nord-est}\right\}$ l'ensemble $\mathcal{D}_4$ complété des directions intermédiaires.

On note $\mathcal{K}$ l'ensemble des types de terrain que peut comporter une \sref{forme}. Dans notre implémentation, $\mathcal{K} \overset{def}{=} \left\{\text{champ}, \text{route}, \text{cité}\right\}$.


\subsubsection*{Partie de Carcassonne}
Une \srefdef{partie} de Carcassone est un 8-uplet $(P, \mathcal{F}, \mathcal{T}, d_P, s_J, j, s_P)$ où
\begin{itemize}
\item $P$ est un \sref{plateau} de jeu;
\item $\mathcal{F}$ est un ensemble de \sref{forme}s de tuiles; on note $N_F = \textrm{Card}(\mathcal{F})$;
\item $\mathcal{T}$ est un ensemble de \sref{tuile}s; on note $N_T = \textrm{Card}(\mathcal{T})$;
\item $d_P$ est une fonction $\mathcal{F} \rightarrow \mathbb{N}$ (une distribution) associant à chaque \sref{forme} le nombre de \sref{tuile}s qui sont de cette forme;
\item $s_J$ est une séquence de \sref{joueur}s de longueur $N_J$;
\item $j \in \llbracket 1, N_J \rrbracket$ représente l'indice dans $s_J$ du joueur dont c'est le tour;
\item $s_P$ est une \sref{pioche}.
\end{itemize}

Dans le jeu original, $N_F = 21$ et $N_T = 72$.

\begin{algorithm}[H]
\Deb{
$P \leftarrow \texttt{créerPlateauVide}()$\\
$\left(\mathcal{F}, \mathcal{T}, d_P\right) \leftarrow \texttt{lireFormesEtTuilesDepuisConfiguration}()$
$s_J \leftarrow \texttt{lireJoueursDepuisConfiguration}()$
$s_P \leftarrow \texttt{créerSéquenceAléatoireDepuisEnsemble}(\mathcal{T})$\\
$\texttt{ajouterTuile}\left(P, \texttt{premierÉlément}(s_P)\right)$\\
$j \leftarrow 1$
}
\caption{Initialisation d'une \sref{partie}}
\end{algorithm}

%\begin{alltt}
%Fonctions extérieures :
%\begin{itemize}
%\item lireFormesEtTuilesDepuisConfiguration 
%\end{itemize}
%\end{alltt}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{Le plateau de jeu}
Il s'agit de formaliser le support (table) sur lequel les joueurs posent les \sref{tuiles} qu'ils piochent.

Notre implémentation se concentre sur un plateau à deux dimensions, plat. Nous étudierons par la suite comment cette limitation peut être levée afin de concevoir des plateaux plus ésotériques, comme une sphère ou un tore.

Le \srefdef{plateau} est un graphe simple non orienté sans boucle $P = (V_P, E_P, f_P, g_P)$ où
\begin{itemize}
\item $V_P$ est un ensemble infini de \emph{sommets};
\item $E_P$ est un ensemble infini d'\emph{arêtes} entre deux sommets, construit suivant:
\begin{displaymath}
\begin{array}{rl}
    \forall (x, y, x^\prime, y^\prime) \in \mathbb{Z}^4,
    &
        \left(f_P(x, y), f_P(x^\prime, y^\prime)\right) \in E_P \\
    %
    \Longleftrightarrow &
    %
    \left[x^\prime = x \text{ et } y^\prime = y \pm 1\right]
    \text{ou}
    \left[y^\prime = y \text{ et } x^\prime = x \pm 1\right]
\end{array}
\end{displaymath}
\item $f_P$ est une \sref{bijection} $\mathbb{Z}^2 \rightarrow V_P$ associant à des \sref{coordonnées} $(x, y)$ le sommet situé à cette position;
\item $g_P$ est une fonction $V_P \rightarrow \mathcal{T} \cup \{\varepsilon\}$ associant à un sommet soit la \sref{tuile} correspondante, soit $\varepsilon$ dans le cas où cette tuile n'existe pas; notons que $g_P^{-1}(\varepsilon)$ est un ensemble infini et $g_P^{-1}(t) = \emptyset$ si $t$ n'est pas une tuile posée.
\end{itemize}

\begin{figure}[ht]
\centering
\begin{minipage}{0.49\textwidth}
    \centering
    \includegraphics[scale=1]{figs/boardmap}
\end{minipage}
\begin{minipage}{0.49\textwidth}
    \centering
    \begin{tikzpicture}[scale=0.8,darkstyle/.style={draw,fill=gray!20},empty/.style={circle,draw,preaction={fill, white},pattern=north east lines}]
        \newcommand{\tilenode}[3]{\node [darkstyle] at (1.2 * #1, 1.2 * #2) { #3 }}
      \foreach \x in {0,...,5}
          \draw (\x*1.2, -1) [dashed] to (\x*1.2, 0)
                (\x*1.2, 6) [dashed] to (\x*1.2, 7)
                (0, \x*1.2) [dashed] to (-1, \x*1.2)
                (6, \x*1.2) [dashed] to (7, \x*1.2) ;

      \foreach \x in {0,...,5}
        \foreach \y in {0,...,5}
           {\pgfmathtruncatemacro{\label}{\x - 5 *  \y +21}
           \node [empty]  (\x\y) at (1.2*\x,1.2*\y) { };}

      \foreach \x in {0,...,5}
        \foreach \y [count=\yi] in {0,...,4}
          \draw (\x\y)--(\x\yi) (\y\x)--(\yi\x) ;


    \tilenode{2}{4}{$1$};
    \tilenode{1}{4}{$2$};
    \tilenode{1}{3}{$3$};
    \tilenode{3}{4}{$4$};
    \tilenode{4}{4}{$5$};
    \tilenode{4}{3}{$6$};
    \tilenode{1}{2}{$7$};
    \tilenode{2}{2}{$8$};

    \end{tikzpicture}
\end{minipage}
\caption{Exemple d'un plateau de jeu ; les tuiles posées sont numérotées dans l'ordre où elles ont été jouées ;
on a $f_P(0,0) = 1$, $f_P(2, -1) = 6$, etc.}
\label{fig:boardmap}

\end{figure}

La notion de coordonnées qui apparaît dans $f_P$ implique que le plateau soit constitué d'\emph{emplace-ments} repérés par des coordonnées. Le plateau peut donc être vu comme une grille infinie dont certaines cellules contiennent des \sref{tuile}s et dont les autres sont vides (cf. figure \ref{fig:boardmap}).

\begin{function}[H]
\caption{créerPlateauVide()}
$V_P \leftarrow \texttt{ensembleInfini}()$\\
$E_P \leftarrow \texttt{construireArêtesDeGrille}()$
\end{function}

\begin{algorithm}[H]
\Sortie{Foo}
\Deb{
$V_P \leftarrow \texttt{ensembleInfini}()$\\
$E_P \leftarrow \texttt{construireArêtesDeGrille}()$
}
\caption{\texttt{créerPlateauVide}, initialisation d'un \sref{plateau}}
\end{algorithm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{La forme de tuile}

La \srefdef{forme} de tuile, que l'on se contentera d'appeler \emph{forme}, est un quadruplet $F = \left(t, b, f_F, g_F\right)$ définissant le motif (dessin) de chaque tuile, où
\begin{itemize}
\item $t \in \left\{\text{normal}, \text{abbaye}\right\}$ détermine le type de la forme;
\item $b \in \mathbb{N}$ détermine le bonus-points, possiblement nul, des cités de la forme;
\item $f_F$ est une fonction $\mathcal{D}_8 \rightarrow \mathcal{K}$ associant à chaque \emph{morceau} de la forme son type de terrain;
\item $g_F$ est un graphe simple non-orienté sans boucle $(V_F, E_F)$, dit \emph{graphe interne}, où les sommets sont les éléments de $\mathcal{D}_8$; ce
graphe représente les liens entre \sref{cellule}s au sein de la tuile. Par simplicité, on minimisera le nombre d'arêtes (cf. note ci-après).
\end{itemize}

Les figures \ref{fig:tileshape1} et \ref{fig:tileshape2} donnent deux exemples de formes.

\begin{figure}[ht]
\centering
\begin{minipage}{0.24\textwidth}
    \centering
    \includegraphics[scale=0.5]{figs/starttile}
\end{minipage}
\begin{minipage}{0.65\textwidth}
    \centering
    \begin{displaymath}
    \left(
    \text{normal},
    \hspace{1em}0,
    \hspace{1em}\begin{array}{rcl}
        \text{nord} & \rightarrow & \text{cité} \\
        \text{nord-ouest} & \rightarrow & \text{champ} \\
        \text{ouest} & \rightarrow & \text{route} \\
        \text{sud-ouest} & \rightarrow & \text{champ} \\
        \text{sud} & \rightarrow & \text{champ} \\
        \text{sud-est} & \rightarrow & \text{champ} \\
        \text{est} & \rightarrow & \text{route} \\
        \text{nord-est} & \rightarrow & \text{champ}
    \end{array},
    \hspace{1em}\begin{tikzpicture}[scale=0.8,darkstyle/.style={circle,draw,fill=gray!20},empty/.style={circle,draw,fill=white},baseline=(O.base)]
        \coordinate(O) at (1.2, 1.2);

        \node [empty]  (00) at (1.2*0,1.2*0) { };
        \node [empty]  (10) at (1.2*1,1.2*0) { };
        \node [empty]  (20) at (1.2*2,1.2*0) { };
        \node [empty]  (01) at (1.2*0,1.2*1) { };
        \node [empty]  (21) at (1.2*2,1.2*1) { };
        \node [empty]  (02) at (1.2*0,1.2*2) { };
        \node [empty]  (12) at (1.2*1,1.2*2) { };
        \node [empty]  (22) at (1.2*2,1.2*2) { };

        \draw (01)--(21) (00)--(10) (10)--(20);
        \path (02) edge [bend right] (22);
        % \path (00) edge [bend left] (20);

    \end{tikzpicture}\right)
    \end{displaymath}
\end{minipage}
\caption{Exemple d'une \sref{forme} de tuile.}
\label{fig:tileshape1}
\end{figure}

\begin{figure}[ht]
\centering
\begin{minipage}{0.24\textwidth}
    \centering
    \includegraphics[scale=0.5]{figs/abbeytile}
\end{minipage}
\begin{minipage}{0.65\textwidth}
    \centering
    \begin{displaymath}
    \left(
    \text{abbaye},
    \hspace{1em}0,
    \hspace{1em}\begin{array}{rcl}
        \text{nord} & \rightarrow & \text{champ} \\
        \text{nord-ouest} & \rightarrow & \text{champ} \\
        \text{ouest} & \rightarrow & \text{champ} \\
        \text{sud-ouest} & \rightarrow & \text{champ} \\
        \text{sud} & \rightarrow & \text{route} \\
        \text{sud-est} & \rightarrow & \text{champ} \\
        \text{est} & \rightarrow & \text{champ} \\
        \text{nord-est} & \rightarrow & \text{champ}
    \end{array},
    \hspace{1em}\begin{tikzpicture}[scale=0.8,empty/.style={circle,draw,fill=white},baseline=(O.base)]
        \coordinate(O) at (1.2, 1.2);

        \node [empty]  (00) at (1.2*0,1.2*0) { };
        \node [empty]  (10) at (1.2*1,1.2*0) { };
        \node [empty]  (20) at (1.2*2,1.2*0) { };
        \node [empty]  (01) at (1.2*0,1.2*1) { };
        \node [empty]  (21) at (1.2*2,1.2*1) { };
        \node [empty]  (02) at (1.2*0,1.2*2) { };
        \node [empty]  (12) at (1.2*1,1.2*2) { };
        \node [empty]  (22) at (1.2*2,1.2*2) { };

        \draw (00) -- (01) -- (02) -- (12) -- (22) -- (21) -- (20);
        % \draw (12) -- (00);
        % \draw (12) -- (20);
        % \path (00) edge [bend right] (20);
        % \path (20) edge [bend right] (22);
        % \path (22) edge [bend right] (02);
        % \path (02) edge [bend right] (00);
        % \draw (00) -- (22) -- (01) -- (12) -- (21) -- (02) -- (20) (01) -- (21) -- (00) (01) -- (20);

    \end{tikzpicture}\right)
    \end{displaymath}
\end{minipage}
\caption{Autre exemple d'une \sref{forme} de tuile.}
\label{fig:tileshape2}
\end{figure}

\paragraph{Note sur la minimisation du nombre d'arêtes}
Prenons l'exemple de la figure \ref{fig:tileshape1}. Son \emph{graphe interne} est, pour rappel,
\begin{tikzpicture}[scale=0.3,empty/.style={circle,draw,fill=white,minimum size=5pt,inner sep=0pt},baseline=(O.base)]
    \coordinate(O) at (1.2, 1.2);

    \node [empty]  (00) at (1.2*0,1.2*0) {};
    \node [empty]  (10) at (1.2*1,1.2*0) {};
    \node [empty]  (20) at (1.2*2,1.2*0) {};
    \node [empty]  (01) at (1.2*0,1.2*1) {};
    \node [empty]  (21) at (1.2*2,1.2*1) {};
    \node [empty]  (02) at (1.2*0,1.2*2) {};
    \node [empty]  (12) at (1.2*1,1.2*2) {};
    \node [empty]  (22) at (1.2*2,1.2*2) {};

    \draw (01)--(21) (00)--(10) (10)--(20);
    \path (02) edge [bend right] (22);
    % \path (00) edge [bend left] (20);
\end{tikzpicture}. Pour autant, il n'existe pas d'arête entre les deux sommets
extrêmes du bas alors qu'ils appartiennent au même terrain. On aurait donc pu ajouter
l'arête manquante afin de produire
\begin{tikzpicture}[scale=0.3,empty/.style={circle,draw,fill=white,minimum size=5pt,inner sep=0pt},baseline=(O.base)]
    \coordinate(O) at (1.2, 1.2);

    \node [empty]  (00) at (1.2*0,1.2*0) {};
    \node [empty]  (10) at (1.2*1,1.2*0) {};
    \node [empty]  (20) at (1.2*2,1.2*0) {};
    \node [empty]  (01) at (1.2*0,1.2*1) {};
    \node [empty]  (21) at (1.2*2,1.2*1) {};
    \node [empty]  (02) at (1.2*0,1.2*2) {};
    \node [empty]  (12) at (1.2*1,1.2*2) {};
    \node [empty]  (22) at (1.2*2,1.2*2) {};

    \draw (01)--(21) (00)--(10) (10)--(20);
    \path (02) edge [bend right] (22);
    \path (00) edge [bend left] (20);
\end{tikzpicture},
mais cela est inutile: pour tester si deux sommets sont sur le même terrain,
il suffit de tester non pas l'existence d'une \emph{arête} mais d'un \emph{chemin}.
En procédant ainsi, le calcul des terrains est certes plus couteux mais
économise l'ajout d'un nombre important d'arêtes dans le fichier de définition
des \sref{forme}s.
Pour l'exemple \ref{fig:tileshape2}, on passe alors de
\begin{tikzpicture}[scale=0.3,empty/.style={circle,draw,fill=white,minimum size=5pt,inner sep=0pt},baseline=(O.base)]
    \coordinate(O) at (1.2, 1.2);

    \node [empty]  (00) at (1.2*0,1.2*0) { };
    \node [empty]  (10) at (1.2*1,1.2*0) { };
    \node [empty]  (20) at (1.2*2,1.2*0) { };
    \node [empty]  (01) at (1.2*0,1.2*1) { };
    \node [empty]  (21) at (1.2*2,1.2*1) { };
    \node [empty]  (02) at (1.2*0,1.2*2) { };
    \node [empty]  (12) at (1.2*1,1.2*2) { };
    \node [empty]  (22) at (1.2*2,1.2*2) { };

    \draw (00) -- (01) -- (02) -- (12) -- (22) -- (21) -- (20);
    \draw (12) -- (00);
    \draw (12) -- (20);
    \path (00) edge [bend right] (20);
    \path (20) edge [bend right] (22);
    \path (22) edge [bend right] (02);
    \path (02) edge [bend right] (00);
    \draw (00) -- (22) -- (01) -- (12) -- (21) -- (02) -- (20) (01) -- (21) -- (00) (01) -- (20);
\end{tikzpicture}
à
\begin{tikzpicture}[scale=0.3,empty/.style={circle,draw,fill=white,minimum size=5pt,inner sep=0pt},baseline=(O.base)]
    \coordinate(O) at (1.2, 1.2);

    \node [empty]  (00) at (1.2*0,1.2*0) { };
    \node [empty]  (10) at (1.2*1,1.2*0) { };
    \node [empty]  (20) at (1.2*2,1.2*0) { };
    \node [empty]  (01) at (1.2*0,1.2*1) { };
    \node [empty]  (21) at (1.2*2,1.2*1) { };
    \node [empty]  (02) at (1.2*0,1.2*2) { };
    \node [empty]  (12) at (1.2*1,1.2*2) { };
    \node [empty]  (22) at (1.2*2,1.2*2) { };

    \draw (00) -- (01) -- (02) -- (12) -- (22) -- (21) -- (20);
\end{tikzpicture}
grâce à cette considération. Bien-sûr, il n'y a pas unicité de la représentation
(on aurait pu utiliser
\begin{tikzpicture}[scale=0.3,empty/.style={circle,draw,fill=white,minimum size=5pt,inner sep=0pt},baseline=(O.base)]
    \coordinate(O) at (1.2, 1.2);

    \node [empty]  (00) at (1.2*0,1.2*0) { };
    \node [empty]  (10) at (1.2*1,1.2*0) { };
    \node [empty]  (20) at (1.2*2,1.2*0) { };
    \node [empty]  (01) at (1.2*0,1.2*1) { };
    \node [empty]  (21) at (1.2*2,1.2*1) { };
    \node [empty]  (02) at (1.2*0,1.2*2) { };
    \node [empty]  (12) at (1.2*1,1.2*2) { };
    \node [empty]  (22) at (1.2*2,1.2*2) { };

    \draw (00) -- (01) (00) -- (12) (00) -- (22) (00) -- (21);
    \path (00) edge [bend left] (02);
    \path (00) edge [bend right] (20);

\end{tikzpicture}
dans l'exemple \ref{fig:tileshape2}) mais cela ne pose aucun problème.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection*{La tuile}

La \srefdef{tuile} représente le \og morceau \fg{} de carte que les \sref{joueur}s piochent et posent sur le \sref{plateau}.
Elle est définie par le triplet $T = \left(F, r, \mathcal{C}\right)$ où
\begin{itemize}
\item $F \in \mathcal{F}$ est la \sref{forme} dont est issue la \sref{tuile};
\item $r \in \{0,\frac{\pi}{2},\pi,\frac{3\pi}{2}\}$ est l'orientation de la pièce, par rapport à la \sref{forme} dont elle est issue;
\item $\mathcal{C}$ est la séquence des 8 \sref{cellule}s constitutives de la tuile.
\end{itemize}


\subsubsection*{La cellule}

La \srefdef{cellule} est un \emph{morceau} de \sref{tuile}. Chaque \sref{tuile} est en effet divisée
en huit \emph{cellules} qui contient des informations utiles à la gestion des
points et des \sref{pion}s. Le détail de ces informations n'a pas d'intérêt algorithmique
en soi, mais ces informations ont la particularité importante de devoir être
\emph{propagées} à chaque ajout d'une \sref{tuile}. C'est la raison d'être
des algorithmes de propagation abordés dans la suite de ce document.


\subsubsection*{L'action}

Une \srefdef{action} est un 5-uplet $A = (j, n, s, T, c)$, où
\begin{itemize}
    \item $j$ est le \sref{joueur} qui a donné lieu à l'action ;
    \item $n \in \{ajoutTuile, ajoutPion\}$ détermine la \srefdef{nature de l'action} : $n$ vaut $ajoutTuile$ si l'on souhaite ajouter une \sref{tuile} au \sref{plateau}, ou $ajoutPion$ si l'on souhaite ajouter un \sref{pion} sur une tuile ;
	\item $s \in V_P$ (avec $V_P$ l'ensemble de sommets du \sref{plateau}) un sommet cible de l'action ;
    \item $T = (F, r, \mathcal{C}) \in \mathcal{T}$ la tuile à ajouter dans le cas où $n = ajoutTuile$ ; si $n = ajoutPion$, $T = g_p(s)$ la tuile sur laquelle poser le pion ;
    \item $c \in [0, 7]$ est l'indice de la cellule (dans la séquence $\mathcal{C}$) sur laquelle le pion est posé si $n = ajoutPion$ ; si $n = ajoutTuile$, $c$ a une valeur quelconque.
\end{itemize}

% \begin{alltt}
% Structure de cellule:
% \begin{itemize}
% \item un tableau d'entier donnant le nombre de pions de chaque joueur présents dans le terrain dont fait partie la cellule;
% \item un entier indiquant si la cellule fait partie d'une ville terminée;
% \item un entier donnant la taille du terrain dont fait partie la cellule.
% \end{itemize}
% \end{alltt}

% Cela permet ainsi, en  créant une nouvelle structure pour ces cellules, de savoir exactement où se trouve un pion et de délimiter les régions. En effet, lorsqu'un joueur pose un pion sur un terrain (une route par exemple) aucun autre joueur ne peut en poser sur la même route. Cependant il peut arriver qu'en ajoutant une tuile, deux terrains auparavant différents sur lesquels il y a un pion soient reliés (deux portions de route qui n'en forment plus qu'une). Afin de savoir combien de pions sont présents sur une région, on effectue une propagation de l'information à toute la zone. Cela facilitera le comptage des points et permet également, avec une faible complexité en temps, de savoir si un joueur peut poser ou non l'un de ses pions sur une cellule. Nous avons donc fait le choix de représenter les pions présents sur une cellule par un tableau d'autant de cases que le nombre de joueurs et contenant le nombre de pions que chaque joueur possède sur le terrain auquel la cellule appartient.

% %%% DES DESSINS DOIVENT ETRE INTEGRES DANS CETTE PARTIE. PROBABLEMENT UN POUR EXPLIQUER LE PLATEAU DE JEU, ET UN POUR LA REPRESENTATION D'UNE TUILE %%%


% \subsubsection*{Les tuiles voisines (ou adjacentes)}
% Le plateau de jeu étant une grille, une tuile a au maximum quatres voisins. Ceux-ci sont obtenues grâce à une fonction associée à la grille qui à partir d'un couple de coordonnées donne les tuiles adjacentes. Nous définissons alors quatre orientations pour différencier les voisins d'une tuile: Nord, Est, Sud et Ouest.

% %A modifier
% \subsubsection*{Une orientation}
% Une orientation est une direction qui permet de faire référence directement à l'un des voisins d'une tuile. On définit également l'orientation opposée à une orientation: l'orientation opposée de Nord est Sud, et inversement, et l'orientation opposée d'Est est Ouest, et inversement.
% Les différentes orientations seront représentées par des entiers allant de 0 pour le Nord à 3 pour l'Ouest.
