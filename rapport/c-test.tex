\subsection{Tests unitaires et débogage}

Tout projet conséquent se doit d'être séparé en modules distincts qui occupent
chacun une fonction précise (cf. partie \emph{aspect modulaire}). Pour
assurer le bon fonctionnement du programme dans sa globalité, il est vital
de garantir que chaque module, pris séparément, est fonctionnel.
C'est la raison d'être des tests unitaires.

\subsubsection*{Le cas de l'interface graphique}
Pour ce projet, nous avons essayé d'écrire des tests unitaires pour tous les
modules sauf l'interface graphique. Cette dernière est en effet difficile à
tester puisqu'elle met en jeu une interaction avec l'utilisateur (clavier,
souris) et qu'il est de toute façon très difficile, voire impossible, de définir
concrètement des tests de validité d'une interface graphique, à moins de tester
par exemple la couleur de quelques pixels.

Il est finalement bien plus utile et efficace de \og tester \fg{} une interface
graphique en la manipulant en temps réel et en vérifiant certains comportements
basiques comme la réponse aux événements (clics, touches clavier) ainsi que
le rendu graphique en général.

\subsubsection*{Tests unitaires avec CuTest}
Pour faciliter l'écriture des tests unitaires, nous avons choisi d'utiliser
un \emph{framework} minimaliste, \texttt{CuTest}, qui permet d'uniformiser
l'écriture et de produire un rapport d'exécution compréhensible et détaillé (cf. figure
\ref{fig:testreport}), ce qui améliore l'efficacité à corriger les problèmes.

\begin{figure}[ht]
\centering
\begin{verbatim}
...F...
There was 1 failure:
1) Tile_ShapeDefTest: /home/alex/projects/carcassonne/test/tad/test_tile.c:22:
   expected <3> but was <2>

!!!FAILURES!!!
Runs: 7 Passes: 6 Fails: 1
\end{verbatim}
\caption{Exemple de test unitaire échoué}
\label{fig:testreport}
\end{figure}

\subsubsection*{Débogage et utilisation mémoire}
Tout au long du développement, nous avons fait appel au débogueur GDB
afin d'analyser la pile d'appels des fonctions qui ne se comportaient pas
comme espéré. Cet outil permet en effet d'afficher proprement les valeurs
prises par les arguments des fonctions et donc de détecter certaines absurdités.
Si l'interface de GDB peut paraître assez peu intuitive aux premiers abords,
cet outil se révèle vital pour détecter la cause de certain bugs.
Typiquement, nous avons pu corriger des problèmes de lectures hors tableau ou de
variables non initialisées qui prennent des valeurs aléatoires (les avertissements
du compilateur ne suffisent pas dans certains cas).

L'utilisation de Valgrind s'est avérée très ardue à cause de la partie graphique
du projet. En effet, nous ne contrôlons pas le fonctionnement de SDL et
OpenGL et le rapport des fuites mémoires détectées par Valgrind se retrouve
inondé de références aux fonctions internes de ces deux bibliothèques, rendant
difficile le repérage des fuites mémoires propres à notre implémentation.

Dans tous les cas, il est certain que, de par sa nature même, le programme est voué
à allouer une certaine quantité de mémoire à son lancement, pour finalement
en libérer l'intégralité lors de sa fermeture. Bien entendu, certains algorithmes
vont allouer davantage de mémoire (typiquement, les explorations de graphes
demandent de mémoriser les nœuds visités) mais
nous nous sommes assurés de libérer cette mémoire à la sortie de la fonction.

\paragraph{Allocation statique}
Il est utile de noter que notre programme utilise majoritairement de l'allocation
mémoire dynamique. C'est tout à fait normal: nous avons fait le choix de ne pas
initialiser statiquement les formes et les tuiles pour permettre de les configurer
facilement dans un fichier externe. Par conséquent, nous ne connaissons pas
leur nombre \emph{a priori} et devons utiliser l'allocation dynamique.

Les allocations statiques ne sont finalement utilisées que pour des objets
tels que des buffers de texte ou des tableaux dont la taille est fixée à la
compilation. Typiquement, le nombre de joueurs maximum est fixé dans un
\texttt{\#define} et nous utilisons des tableaux statiquement alloués à cette
taille maximale.
