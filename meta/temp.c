

                                /* DEBUT DES VERSIONS DE ROAD IS OVER */

bool roadIsOver(Board* b, Tile* tile, int position, GHashTable* visited){
  enum CELL_GEO geo, oppositeGeo;
  TileCell* c[TILE_SIDE_NB_CELLS];
  TileCell* oc[TILE_SIDE_NB_CELLS];
  int i, j, dir,test;

  Tile* neighbours[TILE_NB_NEIGHBOURS];

  boardGetAdjacentTiles(neighbours, b, tile);
  for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++){
    if(neighbours[dir] != NULL){
      geo = indexToSideGeo[dir];
      oppositeGeo = tileGetOppositeGeo(geo);

      if(priority(tileGetFieldForGeo(neighbours[dir], oppositeGeo)) == 1){
	c[0] = tileGetCellForGeo(tile,geo);
	//test =tileGetGeoIndex(tile,geo);
	oc[0] = tileGetCellForGeo(neighbours[dir],oppositeGeo);
	for(i = 0; i < NB_PLAYERS_MAX; i++){
	  c[0]->playerPawnCount[i] += oc[0]->playerPawnCount[i];
	}
	c[0]->cptPoint += oc[0]->cptPoint;

	propagIntern(tile, c[0]);
      }
    }
  }
}

bool roadIsOver(Board* b, Tile* tile, int position, GHashTable* visited){
  g_hash_table_add(visited,tile);
  bool end;
  TileCell * cellInit = tileGetCellForGeo(tile, position);
  Tile *opposite = tileGetOppositeTile(b, tile, position);
  int oppositeGeo = tileGetOppositeGeo(position);
  if (opposite == NULL) {
    return false;
  }
  else if (g_hash_table_lookup(visited,opposite)==NULL){
    end = roadIsOverBis(b, opposite, oppositeGeo, visited);
    TileCell* oppositeCell = tileGetCellForGeo(opposite,oppositeGeo);
    cellInit->cptPoint += oppositeCell->cptPoint;
    if(! end){
      return false;
    }
  }
  return true;
}

bool roadIsOverBis(Board* b, Tile* tile, int position, GHashTable* visited){
  g_hash_table_add(visited,tile);
  bool end;
  int i;
  int flag = 1;
  int positionTab[AREA_TYPE_LAST] = {position,-1,-1,-1};//[AREA_TYPE-LAST];
  //TileCell *cellPosition;

  for(i = 0; i < CELL_GEO_LAST; i++){
    if(tileGetFieldForGeo(tile,i) == ROAD){
	if(!tileGeoAreConnected(tile,positionTab[0],i)){
	  positionTab[flag]=i;
	  flag++;
	}
      }
  }
  for(i = 1; i < AREA_TYPE_LAST; i++){
    if(positionTab[i] == -1)
      ;
    else {
      TileCell * cellInit = tileGetCellForGeo(tile, positionTab[i]);
      Tile *opposite = tileGetOppositeTile(b, tile, positionTab[i]);
      int oppositeGeo = tileGetOppositeGeo(positionTab[i]);
      if(opposite == NULL){
	return false;
      }
      else if(g_hash_table_lookup(visited,opposite)==NULL){
	end = roadIsOverBis(b,opposite,oppositeGeo,visited);
	TileCell* oppositeCell = tileGetCellForGeo(opposite, oppositeGeo);
	cellInit->cptPoint += oppositeCell->cptPoint;
	if(! end){
	  return false;
	}
      }
    }
  }
  return true;
}

bool roadIsOver(Board* b, Tile* tile, GHashTable* visited){
  enum CELL_GEO geo[AREA_TYPE_LAST];
  enum CELL_GEO oppositeGeo[AREA_TYPE_LAST];

  int i,dir;
  int test = 0;
  int j =0;
  bool end;

  g_hash_table_add(visited, tile);
  Tile* neighbours[TILE_NB_NEIGHBOURS];

  if(tile != NULL){
    boardGetAdjacentTiles(neighbours, b, tile);

    for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++){
      geo[dir] = indexToSideGeo[dir];
      oppositeGeo[dir] = tileGetOppositeGeo(geo[dir]);

      if(tileGetFieldForGeo(tile,geo[dir]) != ROAD){
	geo[dir] = 10;
	oppositeGeo[dir] = 10;
	neighbours[dir] = NULL;
      }
    }
    while(test == 0){
      if(j >= AREA_TYPE_LAST){
	test = -1;
	break;
      }
      else if(geo[j] == 10){
	j++;
      }
      else if(geo[j] != 10){
	test = 1;
	break;
      }
    }
    if(test == -1){
      return false;
    }
    for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++){
      if(geo[dir]!=10){
	if(!tileGeoAreConnected(tile,geo[test],geo[dir])){
	  geo[dir] = 10;
	  oppositeGeo[dir] = 10;
	  neighbours[dir] = NULL;
	}
      }
    }

    for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir ++){
      if(geo[dir] == 10 || g_hash_table_lookup(visited,tile) != NULL){
	continue;
      }
      else if(geo[dir] != 10 && neighbours[dir] == NULL){
	return false;
      }
      else{
	TileCell* cellInit = tileGetCellForGeo(tile,geo[dir]);
	TileCell* oppositeCell = tileGetCellForGeo(neighbours[dir], oppositeGeo[dir]);
	end = roadIsOver(b,neighbours[dir],visited);
	cellInit->cptPoint += oppositeCell->cptPoint;
	if(!end){
	  return false;
	}
	else{
	  return true;
	}
      }
    }
    return false;
  }
}


bool roadIsOver(Board* b, Tile* tile, TileCell* cell, GHashTable* visited){

  g_hash_table_add(visited,tile);
  int i;
  bool end;
  TileCell* cellRoad[2];
  cellRoad[0] = cell;
  cellRoad[1] = NULL;

  for(i = 0; i < CELL_GEO_LAST; i ++){
    if(tileGeoAreConnected(tile,cell->position,i)){
      cellRoad[1] = tileGetCellForGeo(tile,i);
    }
  }

  for(i = 0; i < 2; i++){
    if(cellRoad[i] != NULL){
      Tile* oppositeTile = tileGetOppositeTile(b, tile, cellRoad[i]->position);
      if(oppositeTile == NULL){
	return false;
      }
      enum CELL_GEO oppositeGeo;
      oppositeGeo = tileGetGeoIndex(oppositeTile,tileGetOppositeGeo(cell->position));
      TileCell* oppositeCell = tileGetCellForGeo(oppositeTile, oppositeGeo);
      if(g_hash_table_lookup(visited,oppositeTile) == NULL){
	end = roadIsOver(b, oppositeTile, oppositeCell, visited);
	cell->cptPoint += oppositeCell->cptPoint;
	return end;
      }
      return true;
    }
  }
}

                                      /*FIN DES VERSIONS DE ROAD IS OVER*/



                                      /*DEBUT DES VERSIONS DE GUIROADISOVER */

void guiRoadIsOver(Board* b,Tile* tile){

  bool flagRoadOver = false;
  int j;

  enum CELL_GEO geo[TILE_NB_NEIGHBOURS];
  int dir;
  for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++){
    geo[dir] = indexToSideGeo[dir];
    if(tileGetFieldForGeo(tile,geo[dir]) != ROAD){
      geo[dir] = 10;
    }
    else{
      j = dir;
    }
  }
  for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++){
    if(geo[dir] != 10){
      if(tileGeoAreConnected(tile,j,geo[dir])){
	  geo[dir] = 10;
	}
    }
  }

  for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++){
    if(geo[dir] != 10){
      GHashTable* visited = g_hash_table_new(g_direct_hash, g_direct_equal);
      flagRoadOver = roadIsOver(b,tile,tileGetCellForGeo(tile,geo[dir]),visited);
      g_hash_table_destroy(visited);
      if(flagRoadOver){
	playerCalculatePoints(tileGetCellForGeo(tile,geo[dir]), nbPawns, nbPoints);
	for(int test =0; test < 3; test++){
	  fprintf(stderr,"PLAYER %d POINTS : %d\n", test, nbPoints[test]);
	}
      }
    }
  }
}

void guiRoadIsOver(Board* b,Tile* tile){
  bool flagRoadOver = false;
  GHashTable* visited = g_hash_table_new(g_direct_hash, g_direct_equal);

  flagRoadOver = roadIsOver(b,tile,visited);
  if(flagRoadOver){
    printf("\n\nOUAIS\n\n");
  }
}
void guiRoadIsOver(Board* b,Tile* tile){
  bool flagRoadOver = false;
  int i;
  int flag = 0;
  int position[AREA_TYPE_LAST] = {-1,-1,-1,-1};//[AREA_TYPE-LAST];
  //TileCell *cellPosition;

  for(i = 0; i < CELL_GEO_LAST; i++){
    if(tileGetFieldForGeo(tile,i) == ROAD){
      if(flag != 0){
	if(!tileGeoAreConnected(tile,position[0],i)){
	  position[flag]=i;
	  flag++;
	}
      }
      else{
	position[0]=i;
	flag++;
      }
    }
  }

  for(i = 0; i < AREA_TYPE_LAST; i++){
    if(position[i] == -1)
      ;
    else{
      GHashTable* visited = g_hash_table_new(g_direct_hash, g_direct_equal);
      flagRoadOver = roadIsOver(b,tile,position[i],visited);
      g_hash_table_destroy(visited);
      if(flagRoadOver)
	playerCalculatePoints(tile,position[i],nbPawns,nbPoints);
    }
  }
}

      if(position1 != -1){
	if(tileGeoAreConnected(tile,position1, i)){
	  //printf("\nHEY\n");
	  ;
	}
	else{
	  GHashTable* visited = g_hash_table_new(g_direct_hash, g_direct_equal);
	  cellPosition = tileGetCellForGeo(tile, i);
	  position1 = i;
	  //printf("\nSALUT\n");
	  //position1 = tileGetGeoIndex(tile,position1);
	  flagRoadOver = roadIsOver(b, tile, cellPosition, visited);
	  g_hash_table_destroy(visited);
	  if(flagRoadOver){
	    playerCalculatePoints(tile, cellPosition, nbPawns, nbPoints);
	  }
	}
      }
      else{
	GHashTable* visited = g_hash_table_new(g_direct_hash, g_direct_equal);
	cellPosition = tileGetCellForGeo(tile,i);
	position1 = i;
	//position1 = tileGetGeoIndex(tile, position1);
	//printf("\nPOSITION1 %d\n",position1);
	flagRoadOver = roadIsOver(b, tile, cellPosition, visited);
	g_hash_table_destroy(visited);
	if(flagRoadOver){
	  playerCalculatePoints(tile, cellPosition, nbPawns, nbPoints);
	}
      }
    }
  }
}



                                                                   /*COMPTEUR DE CHAMPS*/

GHashTable* boardCountField(Board* b, Tile* T, unsigned int c, GHashTable * visitedTile, GHashTable * visitedCastle){
  if (g_hash_table_contains(visitedTile,T)){
    return visitedCastle;
  }
  g_hash_table_add(visitedTile,T);
  int bound[CELL_GEO_LAST];
  for (int i=0;i<  CELL_GEO_LAST;++i){
    bound[i]=0;
  }

  int j=0;
  int i = 0;
  for (i=0;i< CELL_GEO_LAST;++i){
    if (ugraphPathExists(T->shape->connections,i,c)) {
      bound[j]=i;
      j++;
    }

  }
  i=0;

  int castleInd[j*CELL_GEO_LAST];
  TileCell* castle[j*CELL_GEO_LAST];
  int l=0;
  int jj = j;
  for (jj=j;jj>0;--jj) {
    for (int k=0; k < CELL_GEO_LAST ; ++k) {
      if (ugraphPathExists(T->shape->farmAndCity,bound[jj-1],k)){
	castle[l]=&T->cells[k];
	castleInd[l]=k;
	l++;
      }
    }
  }
  int ll;
  for (ll=l ; ll>0 ; ll--){
    if(g_hash_table_contains(visitedCastle,castle[ll])){
      continue;
    }
    if(T->cells[castleInd[ll]].finishedCityId) {
      g_hash_table_add(visitedCastle,castle[ll]);
    }
  }

  Tile* neighbours[TILE_NB_NEIGHBOURS];
  boardGetAdjacentTiles(neighbours,b,T);
  for(int dir=0;dir<TILE_NB_NEIGHBOURS;i++){
    enum CELL_GEO geo = indexToSideGeo[dir];
    enum CELL_GEO Oppositegeo=tileGetOppositeGeo(geo);
    if(priority(tileGetFieldForGeo(neighbours[dir],Oppositegeo))==1){
      continue;
    }

    for(j ; j>0 ; ++j) {
      TileCell* sideCells[3];
      tileGetSideCellsForGeo(sideCells, T, indexToSideGeo[dir]);
      for(int l=0 ; l< TILE_SIDE_NB_CELLS ; l++){
	//TileCell* c2=tileGetCellForGeo(neighbours[dir],tileGetOppositeGeo(geo));
	int c2 = tileGetIntCellForGeo(neighbours[dir],tileGetOppositeGeo(geo));
	boardCountField(b, neighbours[dir], c2, visitedTile, visitedCastle);
      }
    }
  }
  return visitedCastle;
}


                                            /*ACTUTILEBIS et PROPAGBIS*/

void propagBis(const Board* b, Tile* tile, GHashTable* visited){
  int dir;
  Tile* neighbours[TILE_NB_NEIGHBOURS];
  //GHashTable* newVisited = g_hash_table_new(g_direct_hash, g_direct_equal);
  if(tile != NULL){
    boardGetAdjacentTiles(neighbours, b, tile);
    //actuTileBis(b,tile, visited);
    for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir ++){
      if(neighbours[dir] == NULL || g_hash_table_lookup(visited,neighbours[dir]) != NULL){
	continue;
      }
      actuTileBis(b,neighbours[dir],visited);
    }
    g_hash_table_add(visited,tile);
    for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir ++){
      if(neighbours[dir] == NULL || g_hash_table_lookup(visited,neighbours[dir]) != NULL){
	continue;
      }
      propagBis(b, neighbours[dir], visited);
    }
  }
}


void
actuTileBis(const Board* b, Tile* tile, GHashTable* visited){
  enum CELL_GEO geo, oppositeGeo;
  TileCell* c[TILE_SIDE_NB_CELLS];
  TileCell* oc[TILE_SIDE_NB_CELLS];
  int i, j, dir,test;

  Tile* neighbours[TILE_NB_NEIGHBOURS];

  boardGetAdjacentTiles(neighbours, b, tile);
  for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++) {
    if(neighbours[dir] != NULL && g_hash_table_lookup(visited,neighbours[dir]==NULL)) {
	geo = indexToSideGeo[dir];
	oppositeGeo = tileGetOppositeGeo(geo);

	if(priority(tileGetFieldForGeo(neighbours[dir], oppositeGeo)) == 1) {

	  c[0] = tileGetCellForGeo(tile, geo);
	  //test =tileGetGeoIndex(tile,geo);
	  oc[0] = tileGetCellForGeo(neighbours[dir], oppositeGeo);
	  for(i = 0; i < NB_PLAYERS_MAX; i++) {
	    c[0]->playerPawnCount[i] += oc[0]->playerPawnCount[i];
	  }
	  c[0]->cptPoint += oc[0]->cptPoint;
	  propagIntern(tile, c[0]);

	} else {

	  //printf("\n\n BIEN \n\n");
	  tileGetSideCellsForGeo(c, tile, geo, false);
	  tileGetSideCellsForGeo(oc, neighbours[dir], oppositeGeo, true);

	  for(i = 0; i < TILE_SIDE_NB_CELLS; i++){
	    for(j = 0; j < NB_PLAYERS_MAX; j++){
	      c[i]->playerPawnCount[j] += oc[i]->playerPawnCount[j];
	    }
	    propagIntern(tile,c[i]);
	  }

	} // endif priority cases
      }
  }
