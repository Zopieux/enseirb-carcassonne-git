Pb général : mise à jour suite à action

  E :
    - plateau P
    - action A

  S : le plateau Q issu de l'action A sur P


Procédure miseAJourSuiteAAction(P : plateau, A : action):
  miseAJourTuile(P, A);
  miseAJourPropagation(P, sommetAssocié(A), ensembleVide());
Fin


Procédure miseAJourPropagation(P : plateau, S : sommet, visités : Ensemble de sommets):
  ajouter(S, visités);

  Pour i de 0 à 3 :
    Si existeVoisin(P, S, i) :
      voisin <- calculerVoisin(P, S, i);
      Si non(appartient(voisin, visités)) :
	traitement(P, S, Sv);
	miseAJourPropagation(P, Sv, visités);
      FinSi
    FinSi
  FinPour

Fin


Procédure miseAJourTuile(P : plateau, A : action) :
  
Fin


En bref :
  Plateau (jeu) :
    - graphe (Vg, Eg, ...TODO) (+ coordonnées ?)
    - f : Vg -> orientation (nord, sud, est, ouest)
    - g : Vg -> formeTuile
    - h : Vg -> stats (TODO ?)
  
  Données : P, A
  
  Q = miseAJourSuiteAAction(P, A)
  R = miseAJourTuile(P, A)
  
  Si s = sommetConcerné(A) :
    * R equivalent(s) Q
    * et pour tout t dans Vr \ s, R equivalent(s) P
  
  R equivalent(s) P <=> pour tout e € Vr, f_R(e) = f_P(e) et g_R(e) = g_P(e) etc.
  
  Q est le plateau issu de P après l'action A