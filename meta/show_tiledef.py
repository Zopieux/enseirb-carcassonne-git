from collections import namedtuple
from Tkinter import *
from pprint import pprint


Tile = namedtuple('Tile', 'count type bonus north est south west connnections')
DOT_RAD = 3
PER_ROW = 12
TILE_SIZE = 100
MARGIN = 4
FIELD_WIDTH = 46
ROAD_WIDTH = 16
PADDING = 4
CMARGIN = 20
CODING = 'NnEeSsWw'
COLORS = {
    'C': '#b67b2e',
    'R': '#fbfbfb',
    'F': '#37c169',
}
# Hahaha. Sorry I've never been good at geometry.
POINTS = (
    ((MARGIN, 0), (TILE_SIZE - MARGIN, 0), (FIELD_WIDTH + MARGIN, FIELD_WIDTH), (TILE_SIZE - FIELD_WIDTH - MARGIN, FIELD_WIDTH)),
    ((TILE_SIZE, MARGIN), (TILE_SIZE, TILE_SIZE - MARGIN), (TILE_SIZE - FIELD_WIDTH, FIELD_WIDTH + MARGIN), (TILE_SIZE - FIELD_WIDTH, TILE_SIZE - FIELD_WIDTH - MARGIN)),
    ((TILE_SIZE - MARGIN, TILE_SIZE), (MARGIN, TILE_SIZE), (TILE_SIZE - FIELD_WIDTH - MARGIN, TILE_SIZE - FIELD_WIDTH), (FIELD_WIDTH + MARGIN, TILE_SIZE - FIELD_WIDTH)),
    ((0, TILE_SIZE - MARGIN), (0, MARGIN), (FIELD_WIDTH, TILE_SIZE - FIELD_WIDTH - MARGIN), (FIELD_WIDTH, FIELD_WIDTH + MARGIN)),
)
ROADS = (
    (TILE_SIZE / 2 - ROAD_WIDTH / 2, 0, TILE_SIZE / 2 + ROAD_WIDTH / 2, TILE_SIZE / 2 - ROAD_WIDTH),
    (TILE_SIZE, TILE_SIZE / 2 - ROAD_WIDTH / 2, TILE_SIZE - TILE_SIZE / 2 + ROAD_WIDTH, TILE_SIZE / 2 + ROAD_WIDTH / 2),
    (TILE_SIZE / 2 - ROAD_WIDTH / 2, TILE_SIZE, TILE_SIZE / 2 + ROAD_WIDTH / 2, TILE_SIZE - TILE_SIZE / 2 + ROAD_WIDTH),
    (0, TILE_SIZE / 2 - ROAD_WIDTH / 2, TILE_SIZE / 2 - ROAD_WIDTH, TILE_SIZE / 2 + ROAD_WIDTH / 2),
)
ROAD_INNER = (
    ((TILE_SIZE / 2 - ROAD_WIDTH / 2, TILE_SIZE / 2 - ROAD_WIDTH), (TILE_SIZE / 2 + ROAD_WIDTH / 2, TILE_SIZE / 2 - ROAD_WIDTH)),
    ((TILE_SIZE / 2 + ROAD_WIDTH, TILE_SIZE / 2 - ROAD_WIDTH / 2), (TILE_SIZE / 2 + ROAD_WIDTH, TILE_SIZE / 2 + ROAD_WIDTH / 2)),
    ((TILE_SIZE / 2 + ROAD_WIDTH / 2, TILE_SIZE / 2 + ROAD_WIDTH), (TILE_SIZE / 2 - ROAD_WIDTH / 2, TILE_SIZE / 2 + ROAD_WIDTH)),
    ((TILE_SIZE / 2 - ROAD_WIDTH, TILE_SIZE / 2 + ROAD_WIDTH / 2), (TILE_SIZE / 2 - ROAD_WIDTH, TILE_SIZE / 2 - ROAD_WIDTH / 2))
)
CENTERS = (
    (TILE_SIZE / 2, CMARGIN),
    (TILE_SIZE - CMARGIN, TILE_SIZE / 2),
    (TILE_SIZE / 2, TILE_SIZE - CMARGIN),
    (CMARGIN, TILE_SIZE / 2),
)
CORNERS = (
    (TILE_SIZE / 4, TILE_SIZE / 4),
    (TILE_SIZE - TILE_SIZE / 4, TILE_SIZE / 4),
    (TILE_SIZE - TILE_SIZE / 4, TILE_SIZE - TILE_SIZE / 4),
    (TILE_SIZE / 4, TILE_SIZE - TILE_SIZE / 4),
)
# Laughs end here.


def parse_tiles(fname):
    tiles = []
    data = open(fname)

    tcount = int(data.readline())

    for i in range(tcount):
        assert data.readline() == '\n'
        c = int(data.readline())
        t, bonus = data.readline().split(' ', 1)
        bonus = int(bonus)
        n, e, s, w = data.readline().strip()
        cc = int(data.readline())
        conn = []
        for j in range(cc):
            conn.append(list(data.readline().strip()))

        tiles.append(Tile(c, t, bonus, n, e, s, w, conn))

    data.close()
    return tiles


def draw_field(canvas, field, direction):
    if field == 'C':
        canvas.create_polygon(
            POINTS[direction],
            fill=COLORS['C'],
            outline=COLORS['C'],
        )

    if field == 'R':
        bbox = ROADS[direction]

        canvas.create_rectangle(
            bbox,
            fill=COLORS['R'],
            outline=COLORS['R'],
        )


def draw_farms(canvas, tile):
    # OH MY GOD THIS IS ABSOLUTLY AWEFUL
    # PLEASE DO NOT READ THIS FUNCTION CODE.
    fields = (tile.north, tile.est, tile.south, tile.west)

    # all connections at first
    conn = set(((0, 1), (0, 2), (0, 3), (1, 2), (1, 3), (2, 3)))

    for i, f1 in enumerate(fields):
        for j, f2 in enumerate(fields):
            if i >= j or f1 != f2 or f1 not in 'RC' or not has_connection(tile, i, j):
                continue

            if i % 2 == j % 2:  # straight
                conn.discard((0, 2))
                conn.discard((1, 3))
                if i % 2 == 0:
                    conn.discard((0, 1))
                    conn.discard((2, 3))
                else:
                    conn.discard((1, 2))
                    conn.discard((0, 3))
            else:  # curved
                for k in (0, 1, 2, 3):
                    conn.discard((max(i, j), k))
                    conn.discard((k, max(i, j)))

    for i, j in conn:
            canvas.create_line(
                CORNERS[i] + CORNERS[j],
                fill='green',
            )


def has_connection(tile, i, j):
    return [CODING[i], CODING[j]] in tile.connnections or [CODING[j], CODING[i]] in tile.connnections


def draw_connections(canvas, tile):
    fields = (tile.north, tile.est, tile.south, tile.west)

    for i, f1 in enumerate(fields):
        for j, f2 in enumerate(fields):
            if i >= j or f1 != f2 or not has_connection(tile, i, j):
                continue

            if f1 == 'R':  # roads
                canvas.create_polygon(
                    (ROAD_INNER[i], ROAD_INNER[j]),
                    fill=COLORS['R'],
                    outline=COLORS['R'],
                )
            else:
                canvas.create_line(
                    CENTERS[i] + CENTERS[j],
                    fill='red',
                )


def draw_deco(canvas, tile):
    if tile.type == 'A':
        # photo = PhotoImage(file='abbey.gif')
        # canvas.create_image((TILE_SIZE / 2 - 20, TILE_SIZE / 2 - 25), image=photo)
        canvas.create_text((TILE_SIZE / 2, TILE_SIZE / 2), text="ABBEY",
            fill='red')

    if tile.bonus > 0:
        canvas.create_text((11, 30), text="+%d" % tile.bonus, fill='black')


def main(tiles):
    master = Tk()

    for i, tile in enumerate(tiles):

        c = Canvas(master, width=TILE_SIZE, height=TILE_SIZE, background="green")
        c.grid(row=i / PER_ROW, column=i % PER_ROW, padx=PADDING, pady=PADDING)

        draw_field(c, tile.north, 0)
        draw_field(c, tile.est, 1)
        draw_field(c, tile.south, 2)
        draw_field(c, tile.west, 3)

        draw_connections(c, tile)
        # draw_farms(c, tile)
        draw_deco(c, tile)

    master.mainloop()


if __name__ == '__main__':
    import sys
    tiles = parse_tiles(sys.argv[1])
    print "total number of tiles:", sum(_.count for _ in tiles)
    pprint(tiles)
    main(tiles)
