#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <utils.h>
#include <glib.h>


void
randomSeed(void) {
	srand(time(NULL));
}


int
randomInt(int a, int b) {
	return rand() % (b - a + 1) + a;
}


size_t
randomSizet(size_t a, size_t b) {
	return rand() % (b - a + 1) + a;
}


int
modulo(int x, int m) {
	// équivalent à (x % m + m) % m
	int r = x % m;
	return r < 0 ? r + m : r;
}


int
powerToSup(int i) {
	double logbase2 = log(i) / log(2);
	return (int)round(pow(2.0, ceil(logbase2)));
}


int
intMin(int a, int b) {
	return (a < b) ? a : b;
}


int
intMax(int a, int b) {
	return (a > b) ? a : b;
}
