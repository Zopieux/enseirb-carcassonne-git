#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <gui/utils.h>
#include <gui/ressource.h>
#include <utils.h>


bool
guiResLoadTexture(GLuint* pt_texture, const char* fname, const bool transparency) {
	SDL_Surface* surface = IMG_Load(fname);

	// Bind the texture object
	glBindTexture(GL_TEXTURE_2D, *pt_texture);

	// Set the texture's stretching properties
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Edit the texture object's image data using the information SDL_Surface gives us
	if(transparency)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h,
			0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);
	else
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, surface->w, surface->h,
			0, GL_RGB, GL_UNSIGNED_BYTE, surface->pixels);

	// Free the SDL_Surface only if it was successfully created
	if(surface) {
		SDL_FreeSurface(surface);
		fprintf(stderr, "[INFO] Loaded ressource %s\n", fname);
		return true;
	}
	return false;
}


TTF_Font*
guiResLoadFont(const char* fontName, int ptSize) {
	return TTF_OpenFont(fontName, ptSize);
}


void
guiResFreeFont(TTF_Font* font) {
	TTF_CloseFont(font);
}


void
guiResRenderText(TTF_Font* font, const SDL_Color color, int* w, int* h, const char* text) {
	// Creation de l'image du texte avec la police associee
	SDL_Surface* textLabel = TTF_RenderText_Blended(font, text, color);
	if(NULL == textLabel) {
		fprintf(stderr, "[ERROR] Unable to render text with font %p.\n%s\n",
			font, TTF_GetError());
	}

	// Parametrage
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Detection du codage des pixels
	GLenum pixCode;
	if(textLabel->format->Rmask == 0x000000ff) {
		pixCode = GL_RGBA;
	} else {
		#ifndef GL_BGRA
			#define GL_BGRA 0x80E1
		#endif
		pixCode = GL_BGRA;
	}

	// Chargement de la texture du texte precedament creee
	glTexImage2D(GL_TEXTURE_2D, 0, 4, powerToSup(textLabel->w), powerToSup(textLabel->h), 0, pixCode, GL_UNSIGNED_BYTE, NULL);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, textLabel->w, textLabel->h, pixCode, GL_UNSIGNED_BYTE, textLabel->pixels);

	//Recuperation des dimentions du texte
	*w = textLabel->w;
	*h = textLabel->h;

	// Liberation de l'image du texte du bouton
	SDL_FreeSurface(textLabel);
}
