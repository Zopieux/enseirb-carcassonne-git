#include <GL/gl.h>

#include <utils.h>
#include <gui/utils.h>

void
drawGrid(int resolution, int width, int height) {
	glBegin(GL_LINES);
	for(int i = -resolution; i < width + resolution; i += resolution) {
		glVertex2f(i, -resolution);
		glVertex2f(i, height + resolution);
		if(i < height + resolution) {
			glVertex2f(-resolution       , i);
			glVertex2f(width + resolution, i);
		}
	}
	glEnd();
}


void
guiDrawQuad(int width, int height, float texture_tl, float texture_tr, float texture_bl, float texture_br) {
	glBegin(GL_QUADS);
		glTexCoord2f(texture_tl , texture_bl);
		glVertex2i(0, 0);

		glTexCoord2f(texture_tr, texture_bl);
		glVertex2i(width, 0);

		glTexCoord2f(texture_tr, texture_br);
		glVertex2i(width, height);

		glTexCoord2f(texture_tl, texture_br);
		glVertex2i(0, height);
	glEnd();
}


void
guiDrawText(int textW, int textH) {
	glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(0, textH);

		glTexCoord2f(0, textH / (float)powerToSup(textH));
		glVertex2f(0, 0);

		glTexCoord2f(textW / (float)powerToSup(textW), textH / (float)powerToSup(textH));
		glVertex2f(textW, 0);

		glTexCoord2f(textW / (float)powerToSup(textW), 0);
		glVertex2f(textW, textH);
	glEnd();
}
