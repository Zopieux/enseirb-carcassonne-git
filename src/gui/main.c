#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <SDL/SDL_gfxPrimitives_font.h>
#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <gui/main.h>
#include <gui/ressource.h>
#include <gui/utils.h>

#include <tad/board.h>
#include <tad/player.h>
#include <tad/tile.h>
#include <tad/tiledeck.h>
#include <utils.h>


// Helpers
static const float guiTileCellColors[AREA_TYPE_LAST][3] = {
	{.3921f, .5882f, .0f}, {.7843f, .2549f, .0f}, {.5f, .5f, .5f}, {.9f, .0f, .0f}, {0, 0, 0.9f}, {0, 0, 0}
};

static const SDL_Color SDLWhiteColor = {255, 255, 255};

enum GAME_PHASES {
	PHASE_SPLASH, PHASE_TILE, PHASE_PAWN, PHASE_END, GAME_PHASES_LAST
};

// On doit utiliser des "globales" pour les callbacks de glib
// static Board* board;
static SDL_Surface* screen;
static size_t nbPlayers;
static size_t nbPawns[NB_PLAYERS_MAX];
static size_t nbPoints[NB_PLAYERS_MAX];
static bool winners[NB_PLAYERS_MAX];
static size_t playerId;
static enum GAME_PHASES gamePhase;

static GLuint textures[TEXTURE_LAST];
static bool guiTileDebug = false;

static double blinkAmpl = M_PI_2; // kikoo effect
static double blinkValue = 0;


static enum CELL_GEO
guiCellGeoFromPosition(int x, int y) {
	static const enum CELL_GEO geos[3][3] = {
		{NORTHWEST, WEST, SOUTHWEST},
		{NORTH, CELL_GEO_LAST, SOUTH}, // FIXME: hack.
		{NORTHEAST, EAST, SOUTHEAST}};

	if(0 <= x && x <= 2 && 0 <= y && y <= 2)
		return geos[x][y];
	else {
		return CELL_GEO_LAST;
	}
}


static void guiRestoreGlEnv(void) {
	// Restore everything
	glColor4f(1, 1, 1, 1);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


static SDL_Surface*
guiInit(int width, int height) {
	if(SDL_Init(SDL_INIT_VIDEO) != 0) {
		fprintf(stderr, "[ERROR] Unable to initialize SDL: %s\n", SDL_GetError());
		exit(2);
	}

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_Surface* screen = SDL_SetVideoMode(width, height, 16, SDL_OPENGL /*| SDL_RESIZABLE*/ );
	if(!screen) {
		fprintf(stderr, "[ERROR] Unable to set video mode: %s\n", SDL_GetError());
		exit(2);
	}

	glClearColor(0, 0, 0, 0);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);

	glViewport(0, 0, width, height);

	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glGenTextures(TEXTURE_LAST, textures);

	guiRestoreGlEnv();

	if(TTF_Init() == -1) {
		fprintf(stderr, "[ERROR] TTF_Init error: %s\n", TTF_GetError());
		exit(2);
	}

	return screen;
}


static void
guiDrawPawn(bool hl, bool small) {
	if(small) {
		guiDrawQuad(
			UI_PAWN_SIZE, UI_PAWN_SIZE,
			1 - (hl ? 0 : 1) * UI_PAWN_SIZE / SPRITE_PAWN_WIDTH,
			1 - (hl ? 1 : 2) * UI_PAWN_SIZE / SPRITE_PAWN_WIDTH,
			0,
			UI_PAWN_SIZE / SPRITE_PAWN_HEIGHT);
	} else {
		guiDrawQuad(
			UI_PAWN_SIZE, UI_PAWN_SIZE,
			(hl ? 1 : 0) * SPRITE_PAWN_HEIGHT / SPRITE_PAWN_WIDTH,
			(hl ? 2 : 1) * SPRITE_PAWN_HEIGHT / SPRITE_PAWN_WIDTH,
			0, 1);
	}
}


static void
guiDrawTile(const Tile* tile, int x, int y, bool withPawns, bool debugEnabled) {
	int spriteno = tile->shape->uid;
	int angle = 90 * tile->rotation;

	int line = spriteno / SPRITE_COUNT_BY_LINE;

	float \
		text_tl = (float)(spriteno % SPRITE_COUNT_BY_LINE) / (float)(SPRITE_COUNT_BY_LINE),
		text_tr = (float)((spriteno % SPRITE_COUNT_BY_LINE) + 1) / (float)(SPRITE_COUNT_BY_LINE),
		text_bl = (float)(line) / (float)(SPRITE_NB_LINES),
		text_br = (float)(line + 1) / (float)(SPRITE_NB_LINES);

	glPushMatrix();
	glLoadIdentity();

	glTranslatef(x, y, 0);
	glPushMatrix();
	glTranslatef(GRID_SIZE / 2, GRID_SIZE / 2, 0);
	if(angle != 0)
		glRotatef(angle, 0, 0, 1);

	glTranslatef(-GRID_SIZE / 2, -GRID_SIZE / 2, 0);

	guiDrawQuad(GRID_SIZE, GRID_SIZE, text_tl, text_tr, text_bl, text_br);
	glPopMatrix();

	const float cellsize = GRID_SIZE / 3.0f * DEBUG_CELL_SIZE;
	const float celloffset = cellsize / 2 / DEBUG_CELL_SIZE * (2 + DEBUG_CELL_SIZE);
	int geo;
	enum AREA_TYPE type;

	if(withPawns) {
		glTranslatef(GRID_SIZE / 2, GRID_SIZE / 2, 0);

		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_PAWN]);
		float r, g, b;
		TileCell* cell;

		for(geo = 0; geo <= CELL_GEO_LAST; geo++) {
			if(geo == CELL_GEO_LAST) {
				// special case: center, abbaye
				if(tile->playerOnSpecial == PLAYER_EMPTY) // there is no one
					continue;
				playerGetColor(tile->playerOnSpecial, &r, &g, &b);
			} else {
				cell = tileGetCellForGeo(tile, geo);
				if(cell->hasPawnOfPlayer >= NB_PLAYERS_MAX) // there is no one
					continue;
				playerGetColor(cell->hasPawnOfPlayer, &r, &g, &b);
			}

			glColor4f(r, g, b, 1);
			glPushMatrix();
				glTranslatef(
					guiTileCellOffset[geo][0] * GRID_SIZE / 3.0f - UI_PAWN_SIZE / 2,
					guiTileCellOffset[geo][1] * GRID_SIZE / 3.0f - UI_PAWN_SIZE / 2,
					0);
				guiDrawPawn(true, true);
			glPopMatrix();
		}
		glColor4f(1, 1, 1, 1);
		glTranslatef(-GRID_SIZE / 2, -GRID_SIZE / 2, 0);
	}

	// DEBUG
	if(!(debugEnabled && guiTileDebug)) {
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_TILES]);
		return;
	}

	glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_RAW]);

	glPushMatrix();
	glLoadIdentity();
	glTranslatef(x + celloffset, y + celloffset, 0);

	for(geo = 0; geo < CELL_GEO_LAST; geo++) {
		glPushMatrix();
		glTranslatef(
			guiTileCellOffset[geo][0] * GRID_SIZE / 3.0f,
			guiTileCellOffset[geo][1] * GRID_SIZE / 3.0f,
			0
		);

		if(tileGetCellForGeo(tile, geo)->hasPawnOfPlayer < NB_PLAYERS_MAX) {
			glColor4f(.3, .3, .3, 1);
			glBegin(GL_QUADS);
			glVertex2f(0, 0);
			glVertex2f(cellsize, 0);
			glVertex2f(cellsize, cellsize);
			glVertex2f(0, cellsize);
			glEnd();
		}

		if(!tileCanAddPawnAtGeo(tile, nbPlayers, geo)) {
			type = tileGetFieldForGeo(tile, geo);

			glColor4f(
				guiTileCellColors[type][0],
				guiTileCellColors[type][1],
				guiTileCellColors[type][2], DEBUG_TRANSPARENCY
			);

			glBegin(GL_QUADS);
			glVertex2f(0, 0);
			glVertex2f(cellsize, 0);
			glVertex2f(cellsize, cellsize);
			glVertex2f(0, cellsize);
			glEnd();
		}

		glPopMatrix();
	}
	glPopMatrix();

	// center
	type = (tileGetShape(tile)->type == TILE_ABBEY) ? ABBEY : CROSS; // arbitrary
	glColor4f(
		guiTileCellColors[type][0],
		guiTileCellColors[type][1],
		guiTileCellColors[type][2], DEBUG_TRANSPARENCY
	);
	glPushMatrix();
	glTranslatef(celloffset, celloffset, 0);
	glBegin(GL_QUADS);
	glVertex2f(0, 0);
	glVertex2f(cellsize, 0);
	glVertex2f(cellsize, cellsize);
	glVertex2f(0, cellsize);
	glEnd();
	glPopMatrix();

	glPopMatrix(); // pop tile matrix
	guiRestoreGlEnv();
	glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_TILES]);
}


static void
guiDrawTileFromIter(gpointer index_, gpointer tile_, gpointer more_) {
	int index = (int)GPOINTER_TO_INT(index_);
	Tile* tile = (Tile*) tile_;
	Board* board = *((Board**) more_);

	int x, y;
	boardIndexToPos(board, index, &x, &y);
	x *= GRID_SIZE;
	y *= GRID_SIZE;

	guiDrawTile(tile, x, y, true, true);
}


static void
guiDrawAvailablePositionsAux(gpointer index_, gpointer flags_, gpointer bat_) {
	int index = (int)GPOINTER_TO_INT(index_);
	int flags = (int)GPOINTER_TO_INT(flags_);
	UArgBaT* bat = (UArgBaT*) bat_;
	Board* board = bat->board;
	Tile* tile = bat->tile;

	int x, y;
	boardIndexToPos(board, index, &x, &y);

	if(flags & (1 << (tile->rotation))) {
		guiDrawTile(tile, x * GRID_SIZE, y * GRID_SIZE, false, false);
	}
}


static void
guiDrawAvailablePositions(Board* board, Tile* currentTile) {
	UArgBaT bat = {board, currentTile};
	glColor4f(0, .8f, 0, blinkValue);
	g_hash_table_foreach(board->currentAvailablePos,
		(GHFunc)guiDrawAvailablePositionsAux, (gpointer)&bat);
	glColor4f(1, 1, 1, 1);
}


void
guiFieldIsOver(Board* b,Tile* tile, int field){

	bool flagFieldOver = false;
	int j;
	bool suppPawn = false;
	enum CELL_GEO geo[TILE_NB_NEIGHBOURS];
	int dir;
	for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++) {
		geo[dir] = indexToSideGeo[dir];
		if(tileGetFieldForGeo(tile, geo[dir]) != field) {
			geo[dir] = 10;
		} else {
			j = dir;
		}
	}

	for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++) {
		if(geo[dir] != 10) {
			if(tileGeoAreConnected(tile, indexToSideGeo[j], geo[dir])) {
				if(j == dir)
					continue;
				geo[dir] = 10;
			}
		}
	}

	for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++) {
		if(geo[dir] != 10) {
			int points = 0;
			GHashTable* visited;
			visited  = g_hash_table_new(g_direct_hash, g_direct_equal);

			suppPawn = false;
			flagFieldOver = fieldIsOver(b, tile, dir, visited, &points, suppPawn, field);
			g_hash_table_destroy(visited);

			if(flagFieldOver) {
				suppPawn = true;
				playerCalculatePoints(tileGetCellForGeo(tile, geo[dir]), nbPawns, nbPoints, points);
				visited = g_hash_table_new(g_direct_hash, g_direct_equal);
				flagFieldOver = fieldIsOver(b, tile, dir, visited, &points, suppPawn, field);
				g_hash_table_destroy(visited);
			}
		}
	}
}


void
guiOverFieldAbbey(Board* board, Tile* tile){
	guiFieldIsOver(board, tile, 1);
	guiFieldIsOver(board, tile, 2);
	checkUnfinishedAbbey(board, nbPoints, nbPawns, false);
}


void
guiActualGame(Board* board, Tile* tile){
	GHashTable* visited = g_hash_table_new(g_direct_hash, g_direct_equal);
	g_hash_table_add(visited, tile);
	propag(board, tile, visited);
	g_hash_table_destroy(visited);
}


void
guiActualTile(Board* board, Tile* tile){
	actuTile(board,tile);
}


void
guiPrePawnUpdate(Board* board, Tile* currentTile) {
	// Updates currentTile to prepare for pawn insertion
	guiActualTile(board, currentTile);
	guiActualGame(board, currentTile);
}


void
guiCptEnd(Board* board) {
	GList* tile_list = g_hash_table_get_keys(board->pawnPositions);
	Tile* tile;
	int i;
	TileCell* cell;

	GFOREACH(tile, tile_list) {
		if(tile->playerOnSpecial != PLAYER_EMPTY)
			checkUnfinishedAbbey(board, nbPoints, nbPawns, true);

		for(i = 0; i < CELL_GEO_LAST; i++) {
			cell = tileGetCellForGeo(tile, i);

			if(cell->hasPawnOfPlayer != PLAYER_EMPTY) {

				if(tileGetFieldForGeo(tile, i) == FARM)
					nbPoints[cell->hasPawnOfPlayer] += 6;

				else if(tileGetFieldForGeo(tile, i) == ROAD) {
					GHashTable* visited = g_hash_table_new(g_direct_hash, g_direct_equal);
					bool suppPawn = false;
					bool flagRoadOver = false;
					int points = 0;
					flagRoadOver = fieldIsOver(board, tile, i, visited, &points, suppPawn, 2);
					g_hash_table_destroy(visited);
					nbPoints[cell->hasPawnOfPlayer] += points;
				}

				else if(tileGetFieldForGeo(tile, i) == CASTLE) {
					GHashTable* visited = g_hash_table_new(g_direct_hash, g_direct_equal);
					bool suppPawn = false;
					bool flagCastleOver = false;
					int points = 0;
					flagCastleOver = fieldIsOver(board, tile, i, visited, &points, suppPawn, 1);
					g_hash_table_destroy(visited);
					nbPoints[cell->hasPawnOfPlayer] += points / 2;
				}
			}
		}
		g_hash_table_remove(board->pawnPositions, tile);
	}
	g_list_free(tile_list);
}


static void
guiPhaseInitTile(Board* board, TileDeck* tiledeck, Tile** currentTile) {

	if(tileDeckEmpty(tiledeck)) {
		// no more tiles, the end!
		// compute max
		guiCptEnd(board);
		size_t ptsMax = 0, playerIter;

		for(playerIter = 0; playerIter < nbPlayers; playerIter++)
			ptsMax = (size_t)intMax((int)nbPoints[playerIter], (int)ptsMax);

		// compute winners (ie. players reaching the max)
		for(playerIter = 0; playerIter < nbPlayers; playerIter++)
			winners[playerIter] = nbPoints[playerIter] == ptsMax;

		gamePhase = PHASE_END;
		return;
	}

	*currentTile = tileDeckPick(tiledeck);

	assert(*currentTile == tileDeckSeek(tiledeck));
	boardUpdateCurrentAvailablePos(board, *currentTile);

	bool canAddCurrentTile = false;
	GList* flagList = g_hash_table_get_values(board->currentAvailablePos);

	gpointer iterator;
	GFOREACH(iterator, flagList) {
		if(GPOINTER_TO_INT(iterator) > 0) {
			canAddCurrentTile = true;
			break;
		}
	}

	if(!canAddCurrentTile) {
		guiPhaseInitTile(board, tiledeck, currentTile);
	}
}


static void
nextPlayer(void) {
	playerId += 1;
	playerId %= nbPlayers;
}


void
guiMain(const char* tileDefFname) {
	// Init game logic
	TileShape* shapes;
	Tile* tiles;
	size_t shapeCount, tileCount;

	Board* board;
	TileDeck* tiledeck;
	Tile* currentTile;

	gamePhase = PHASE_TILE;
	playerId = 0;

	// -- Parse tiledefs
	FILE* file = fopen(tileDefFname, "r");
	if(file == NULL) {
		fprintf(stderr, "[ERROR] Bad tile definition file: %s\n", tileDefFname);
		exit(3);
	}

	tileParseDef(file, &shapeCount, &tileCount, &shapes, &tiles);
	fclose(file);

	// -- Init main TAD
	board = boardInit(tileCount);
	tiledeck = tileDeckInit(tiles, tileCount);
	// -- Init gameplay
	randomSeed();
	tileDeckShuffle(tiledeck, 1); // leave first tile untouched
	// Add init tile
	currentTile = tileDeckSeek(tiledeck);
	assert(boardAddTile(board, currentTile, 0, 0));

	// Choose next tile
	guiPhaseInitTile(board, tiledeck, &currentTile);

	// Init UI window and settings
	SDL_Rect camera = {0, 0, WIN_WIDTH, WIN_HEIGHT};

	// Load UI ressources (images, fonts)
	guiResLoadTexture(&textures[TEXTURE_INFOBAR], RESSOURCE_PATH "mainbar.jpg", false);
	guiResLoadTexture(&textures[TEXTURE_AVATARFRAME], RESSOURCE_PATH "ui_avatar.png", true);
	guiResLoadTexture(&textures[TEXTURE_TILES], RESSOURCE_PATH "tileset.jpg", false);
	guiResLoadTexture(&textures[TEXTURE_BACKSIDE], RESSOURCE_PATH "backside.png", true);
	guiResLoadTexture(&textures[TEXTURE_AVATARS], RESSOURCE_PATH "avatars.png", false);
	guiResLoadTexture(&textures[TEXTURE_PAWN], RESSOURCE_PATH "pawns.png", true);
	guiResLoadTexture(&textures[TEXTURE_CURSOR], RESSOURCE_PATH "cursors.png", true);
	guiResLoadTexture(&textures[TEXTURE_TROPHY], RESSOURCE_PATH "trophy.png", true);
	TTF_Font* textFont = guiResLoadFont(RESSOURCE_PATH "celtic.ttf", 25);

	SDL_ShowCursor(SDL_DISABLE);

	// Init UI variables
	size_t playerIter;

	char fpsbuff[128], scoreStr[20];

	size_t frameCount;
	int numPressedKeys,
	rawMouseX, rawMouseY,
	cameraMoveX, cameraMoveY,
		mouseX, mouseY, // scene mouse X, Y
		roundedX, roundedY,
		tileX, tileY, cellX, cellY, currentTileX, currentTileY;
	bool flagAddBlock, flagAddPawn, flagCameraMoving, flagExit;
	bool canAddBlock, canAddPawn;
	int startTimer, endTimer, deltaTime, timeAtStart;
	float cameraZ, scaledX, scaledY;

	Tile* underMouseTile = NULL;
	TileCell* underMouseCell = NULL;
	enum CELL_GEO underMouseCellGeo, geoIter;

	SDL_Event event;

	timeAtStart = SDL_GetTicks();
	Uint8 *keys = SDL_GetKeyState(&numPressedKeys);
	frameCount = 0;
	cameraZ = 1;

	// Main loop
	flagCameraMoving = false;
	flagExit = false;
	flagAddPawn = false;
	while(!flagExit) {
		flagAddBlock = false;
		startTimer = SDL_GetTicks();

  		// user input
		while(SDL_PollEvent(&event)) {
			if(event.type == SDL_QUIT || keys[SDLK_ESCAPE]) {
				flagExit = true;
				break;
			}

			if(event.type == SDL_MOUSEBUTTONDOWN) {
				switch(event.button.button) {
					// Mouse wheel (rotation)
					case SDL_BUTTON_WHEELUP:
						if(keys[SDLK_LCTRL] || keys[SDLK_RCTRL])
							cameraZ *= UI_ZOOM_FACTOR;
						else if(gamePhase == PHASE_TILE) {
							tileSetRotation(currentTile, -1, true);
						}
					break;

					case SDL_BUTTON_WHEELDOWN:
						if(keys[SDLK_LCTRL] || keys[SDLK_RCTRL])
							cameraZ /= UI_ZOOM_FACTOR;
						else if(gamePhase == PHASE_TILE) {
							tileSetRotation(currentTile, 1, true);
						}
						break;

					case SDL_BUTTON_LEFT:
						if(gamePhase == PHASE_TILE)
							flagAddBlock = true;
						else if(gamePhase == PHASE_PAWN && (underMouseTile == currentTile || underMouseTile == NULL))
							flagAddPawn = true;
						break;
				}
			}

			// Debug
			if(keys[SDLK_d])
				guiTileDebug = !guiTileDebug;

			if(keys[SDLK_z])
				cameraZ = 1;

			// Camera release
			if(event.type == SDL_MOUSEBUTTONUP
				&& event.button.button == SDL_BUTTON_RIGHT
				&& flagCameraMoving) {
				flagCameraMoving = false;
			}

			scaledX = event.motion.x / cameraZ;
			scaledY = event.motion.y / cameraZ;

			// Camera hold
			if(event.type == SDL_MOUSEBUTTONDOWN
				&& event.button.button == SDL_BUTTON_RIGHT) {
				if(!flagCameraMoving) {
					flagCameraMoving = true;
					cameraMoveX = scaledX;
					cameraMoveY = scaledY;
				}
			}

			if(event.type == SDL_MOUSEMOTION) {
				if(flagCameraMoving) {
					camera.x += scaledX - cameraMoveX;
					camera.y += scaledY - cameraMoveY;
					cameraMoveX = scaledX;
					cameraMoveY = scaledY;
				}
			}
			mouseX = scaledX - camera.x;
			mouseY = scaledY - camera.y;
			rawMouseX = event.motion.x;
			rawMouseY = event.motion.y;
		}

		// Some calculations
		roundedX = mouseX / GRID_SIZE * GRID_SIZE;
		roundedY = mouseY / GRID_SIZE * GRID_SIZE;
		if(mouseX < 0) roundedX -= GRID_SIZE; // fix negatives
		if(mouseY < 0) roundedY -= GRID_SIZE; // fix negatives

		tileX = roundedX / GRID_SIZE;
		tileY = roundedY / GRID_SIZE;
		underMouseTile = boardGetTileAtPos(board, tileX, tileY);

		if(underMouseTile == NULL) {
			cellX = -1;
			cellY = -1;
			underMouseCell = NULL;
			underMouseCellGeo = 0;
		} else {
			cellX = (mouseX - roundedX) / (GRID_SIZE / 3);
			cellY = (mouseY - roundedY) / (GRID_SIZE / 3);
			cellX = modulo((cellX < 0) ? -cellX : cellX, 3);
			cellY = modulo((cellY < 0) ? -cellY : cellY, 3);
			underMouseCellGeo = guiCellGeoFromPosition(cellX, cellY);
			underMouseCell = tileGetCellForGeo(underMouseTile, underMouseCellGeo);
		}

		if(gamePhase == PHASE_TILE) {
			canAddBlock = boardPositionIsAvailable(board, tileX, tileY)
			&& boardPieceFits(board, tileX, tileY, currentTile);

			// Ajout effectif d'une tuile
			if(flagAddBlock && canAddBlock) {
				flagAddBlock = false;
				assert(boardAddTile(board, currentTile, tileX, tileY));
				currentTileX = roundedX;
				currentTileY = roundedY;
				guiPrePawnUpdate(board, currentTile);
				// check if can add pawn (counter = 0 or no possibility on this cell)
				canAddPawn = \
					nbPawns[playerId] > 0
					&& tileCanAddPawn(currentTile, nbPlayers);

				if(canAddPawn) {
					guiActualGame(board,currentTile);
					gamePhase = PHASE_PAWN;
				} else {
					guiOverFieldAbbey(board, currentTile);
					nextPlayer();
					guiPhaseInitTile(board, tiledeck, &currentTile);
				}
			}

		} else if(gamePhase == PHASE_PAWN && flagAddPawn) {
			flagAddPawn = false;
			gamePhase = PHASE_TILE;

			if(underMouseTile != NULL
				&& tileCanAddPawnAtGeo(currentTile, nbPlayers, underMouseCellGeo)) {
				// Actually add pawn!
				if(underMouseCellGeo == CELL_GEO_LAST) {
					// special case: center of tile (abbaye)
					underMouseTile->playerOnSpecial = playerId;
					g_hash_table_add(board->pawnPositions, underMouseTile);
				} else {
					// normal case: a classic cell
					underMouseCell->playerPawnCount[playerId] += 1;
					underMouseCell->hasPawnOfPlayer = playerId;
					g_hash_table_add(board->pawnPositions, underMouseTile);
					propagIntern(currentTile, underMouseCell);

				} // endif special case abbaye

				nbPawns[playerId] -= 1;

			} else {
			 	// not possible to add pawn
			}
			guiOverFieldAbbey(board, currentTile);
			guiActualGame(board,currentTile);
			nextPlayer();
			guiPhaseInitTile(board, tiledeck, &currentTile);
		} // endif phases

		// Draw things!
		glClear(GL_COLOR_BUFFER_BIT);

		// Projection for fixed features
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, WIN_WIDTH, WIN_HEIGHT, 0, -1, 1);
		glScalef(cameraZ, cameraZ, 1);

		// We then move the camera
		glTranslatef(camera.x, camera.y, 0);

		// Modelview for real scene objects
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		// The grid
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_RAW]);
		glPushMatrix();
		glTranslatef(
			-camera.x + camera.x % GRID_SIZE,
			-camera.y + camera.y % GRID_SIZE, 0);
		glColor3f(.2f, .2f, .2f);
		drawGrid(GRID_SIZE, WIN_WIDTH / cameraZ, WIN_HEIGHT / cameraZ);
		glPopMatrix();

		// Reset for texture use
		glClearDepth(0);
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_TILES]);
		glColor4f(1, 1, 1, 1);

		// Draw the tiles on board
		g_hash_table_foreach(board->tileMap,
			(GHFunc)guiDrawTileFromIter, (gpointer)&board);

		if(gamePhase == PHASE_TILE) {
			// Available tiles
			guiDrawAvailablePositions(board, currentTile);

			// Draw the current tile
			if(canAddBlock) {
				// Snap to grid
				glColor4f(1, 1, 1, 1);
				guiDrawTile(currentTile, roundedX, roundedY, false, false);
			} else {
				// Floating tile
				glColor4f(1, 1, 1, 0.6f);
				guiDrawTile(currentTile,
					mouseX - GRID_SIZE / 2, mouseY - GRID_SIZE / 2, false, false);
			}

		} else if(gamePhase == PHASE_PAWN) {

			glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_PAWN]);
			float r, g, b;

			if(underMouseTile == currentTile) {
				// Draw the pawns!
				playerGetColor(playerId, &r, &g, &b);
				glColor4f(r, g, b, 1);
				glPushMatrix();
				glLoadIdentity();
				glTranslatef(
					currentTileX + GRID_SIZE / 2 - UI_PAWN_SIZE / 2,
					currentTileY + GRID_SIZE / 2 - UI_PAWN_SIZE / 2,
					0
				);
				bool linkedToUnderMouse, displayPawn;
				enum AREA_TYPE areaA, areaB, areaCurrent;

				for(geoIter = 0; geoIter <= CELL_GEO_LAST; geoIter++) {
					if(tileCanAddPawnAtGeo(currentTile, nbPlayers, geoIter)) {
						linkedToUnderMouse = \
							geoIter == underMouseCellGeo || (
								underMouseCellGeo < CELL_GEO_LAST
								&& geoIter < CELL_GEO_LAST
								&& tileGeoAreConnected(currentTile, geoIter, underMouseCellGeo));

						glPushMatrix();
						glTranslatef(
							guiTileCellOffset[geoIter][0] * GRID_SIZE / 3.0f,
							guiTileCellOffset[geoIter][1] * GRID_SIZE / 3.0f,
							0
						);
						displayPawn = true;
						// special case: corners
						if(geoIter % 2 == 1) {
							areaCurrent = tileGetFieldForGeo(currentTile, geoIter);
							areaA = tileGetFieldForGeo(currentTile,
								modulo(geoIter - 1, CELL_GEO_LAST));
							areaB = tileGetFieldForGeo(currentTile,
								modulo(geoIter + 1, CELL_GEO_LAST));

							// don't show if FARM, FARM, CASTLE or CASTE, FARM, FARM
							if(areaCurrent == FARM) {
								if((areaA == CASTLE && areaB == CASTLE) || (areaA == CASTLE && areaB == FARM) || (areaA == FARM && areaB == CASTLE)) {
									displayPawn = false;
								} else {
									// small if some castle
									if(areaA == CASTLE || areaB == CASTLE) {
										glTranslatef(
											UI_PAWN_SIZE * UI_SMALL_PAWN,
											UI_PAWN_SIZE * UI_SMALL_PAWN,
											0);
										glScalef(UI_SMALL_PAWN, UI_SMALL_PAWN, 1);
										glTranslatef(
											-UI_PAWN_SIZE * UI_SMALL_PAWN,
											-UI_PAWN_SIZE * UI_SMALL_PAWN,
											0);
									}
								}
							}
						}
						if(displayPawn) {
							guiDrawPawn(linkedToUnderMouse, true);
						}
						glPopMatrix();
					}
				}
				glPopMatrix();
			}
			if(underMouseTile != NULL) {
				// Draw player influence near cursor
				int i = 0;
				glPushMatrix();
				glLoadIdentity();
				glTranslatef(mouseX + 36, mouseY + 14, 0);
				for(playerIter = 0; playerIter < nbPlayers; playerIter++) {
					if((underMouseCellGeo != CELL_GEO_LAST
						&& underMouseCell->playerPawnCount[playerIter] > 0)
						|| (underMouseCellGeo == CELL_GEO_LAST && underMouseTile->playerOnSpecial == playerIter)) {
						glPushMatrix();
						playerGetColor(playerIter, &r, &g, &b);
						glTranslatef(
							i * UI_PAWN_SIZE,
							22 * fmin(0, sin(blinkAmpl + M_PI_2 / 2 * i)), // jump!
							0
						);
						glColor4f(r, g, b, 1);
						guiDrawPawn(true, true);
						glPopMatrix();
						i++;
					}
				}
				glPopMatrix();
			}
		} // end phase ifelse

		// INTERFACE
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, WIN_WIDTH, WIN_HEIGHT, 0, -1, 1);
		glScalef(1, 1, 1);
		glTranslatef(0, 0, 0);
		glMatrixMode(GL_MODELVIEW);
		glColor4f(1, 1, 1, 1);
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_INFOBAR]);
		glPushMatrix();
		glLoadIdentity();
		glTranslatef(0, WIN_HEIGHT - UI_BAR_HEIGHT, 0);
		guiDrawQuad(WIN_WIDTH, UI_BAR_HEIGHT, 0, 1, 0, 1);
		glPopMatrix();

		// Draw player avatars
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_AVATARS]);
		glPushMatrix();
		glLoadIdentity();
		glTranslatef(
			UI_PLAYER_MARGIN_LEFT,
			WIN_HEIGHT - UI_BAR_HEIGHT + UI_PLAYER_MARGIN_TOP,
			0
		);

		for(playerIter = 0; playerIter < nbPlayers; playerIter++) {
			float \
				text_tl = (float)(playerIter % NB_PLAYERS_MAX) / (float)(NB_PLAYERS_MAX),
				text_tr = (float)((playerIter % NB_PLAYERS_MAX) + 1) / (float)(NB_PLAYERS_MAX);
			glPushMatrix();
			glTranslatef(
				(WIN_WIDTH - UI_PLAYER_MARGIN_LEFT) / nbPlayers * playerIter,
				0, 0
			);
			glScalef(UI_AVATAR_FACTOR, UI_AVATAR_FACTOR, 1);
			guiDrawQuad(UI_AVATAR_SIZE, UI_AVATAR_SIZE, text_tl, text_tr, 0, 1);
			if(((gamePhase == PHASE_TILE || gamePhase == PHASE_PAWN) && playerIter == playerId)
				|| (gamePhase == PHASE_END && winners[playerIter])) {
				glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_RAW]);
				glColor4f(1, 1, 1, fmax(0, sin(blinkAmpl)));
				guiDrawQuad(UI_AVATAR_SIZE, UI_AVATAR_SIZE, 0, 1, 0, 1);
				glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_AVATARS]);
				glColor4f(1, 1, 1, 1);
			}
			glPopMatrix();
		}

		// Draw player avatar frames
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_AVATARFRAME]);
		for(playerIter = 0; playerIter < nbPlayers; playerIter++) {
			glPushMatrix();
			glTranslatef(
				(WIN_WIDTH - UI_PLAYER_MARGIN_LEFT) / nbPlayers * playerIter,
				0, 0);
			glScalef(UI_AVATAR_FACTOR, UI_AVATAR_FACTOR, 1);
			glTranslatef(
				-(UI_AVATARCACHE_SIZE - UI_AVATAR_SIZE) / 2,
				-(UI_AVATARCACHE_SIZE - UI_AVATAR_SIZE) / 2,
				0);
			guiDrawQuad(UI_AVATARCACHE_SIZE, UI_AVATARCACHE_SIZE, 0, 1, 0, 1);
			glPopMatrix();
		}
		glPopMatrix();

		// Draw player names
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_RAW_FONT]);
		glPushMatrix();
		glLoadIdentity();
		glTranslatef(
			UI_PLAYER_MARGIN_LEFT,
			WIN_HEIGHT - UI_BAR_HEIGHT + UI_PLAYER_MARGIN_TOP + UI_MARGIN_NAMES,
			0
		);
		glScalef(1, -1, 1); // black magic

		for(playerIter = 0; playerIter < nbPlayers; playerIter++) {
			int textW, textH;
			float r, g, b;
			playerGetTextColor(playerIter, &r, &g, &b);
			glPushMatrix();
			glTranslatef(
				(WIN_WIDTH - UI_PLAYER_MARGIN_LEFT) / nbPlayers * playerIter,
				0, 0);
			// shadow
			guiResRenderText(textFont, SDLWhiteColor, &textW, &textH, playerGetName(playerIter));
			glColor4f(.1, .1, .1, 1);
			guiDrawText(textW, textH);
			// real text
			glTranslatef(-1, 1, 0);
			glColor4f(r, g, b, 1);
			guiDrawText(textW, textH);
			glPopMatrix();
		}
		glPopMatrix();

		glColor4f(.1, .1, .1, 1);
		glPushMatrix();
		glLoadIdentity();
		glTranslatef(
			UI_PLAYER_MARGIN_LEFT + UI_AVATARCACHE_SIZE * UI_AVATAR_FACTOR + 5,
			WIN_HEIGHT - UI_BAR_HEIGHT + UI_PLAYER_MARGIN_TOP + UI_MARGIN_POINTS,
			0
		);
		glScalef(1, -1, 1); // black magic
		for(playerIter = 0; playerIter < nbPlayers; playerIter++) {
			glPushMatrix();
			glTranslatef(
				(WIN_WIDTH - UI_PLAYER_MARGIN_LEFT) / nbPlayers * playerIter,
				0, 0);
			int textW, textH;

			sprintf(scoreStr, "%lu pts", nbPoints[playerIter]);
			guiResRenderText(textFont, SDLWhiteColor, &textW, &textH, scoreStr);
			guiDrawText(textW, textH);
			glPopMatrix();
		}
		glPopMatrix();

		// Draw player pawns
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_PAWN]);
		glPushMatrix();
		glLoadIdentity();
		glTranslatef(
			UI_PLAYER_MARGIN_LEFT + UI_AVATARCACHE_SIZE * UI_AVATAR_FACTOR + 5,
			WIN_HEIGHT - UI_BAR_HEIGHT + UI_PLAYER_MARGIN_TOP + UI_MARGIN_PAWNS,
			0
		);

		size_t i;
		for(playerIter = 0; playerIter < nbPlayers; playerIter++) {
			glPushMatrix();
			glTranslatef(
				(WIN_WIDTH - UI_PLAYER_MARGIN_LEFT) / nbPlayers * playerIter,
				0, 0
			);
			float r, g, b;
			playerGetTextColor(playerIter, &r, &g, &b);
			glColor4f(r, g, b, 1);
			for(i = 0; i < nbPawns[playerIter]; i++) {
				glPushMatrix();
				glTranslatef(
					((WIN_WIDTH - UI_PLAYER_MARGIN_LEFT) / nbPlayers - UI_AVATARCACHE_SIZE * UI_AVATAR_FACTOR - 30) / nbPawns[playerIter] * i,
					0, 0
				);
				guiDrawPawn(true, true);
				glPopMatrix();
			}
			glPopMatrix();
		}

		// Draw winner trophies
		if(gamePhase == PHASE_END) {
			glColor4f(1, 1, 1, 1);
			glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_TROPHY]);
			glPushMatrix();
			glTranslatef(UI_AVATAR_SIZE * UI_AVATAR_FACTOR / 2, -UI_TROPHY_Y, 0);
			glTranslatef(
				-UI_TROPHY_SIZE / 2,
				-UI_TROPHY_SIZE / 2,
				0);
			for(playerIter = 0; playerIter < nbPlayers; playerIter++) {
				if(!winners[playerIter])
					continue;

				glPushMatrix();
				glTranslatef(
					(WIN_WIDTH - UI_PLAYER_MARGIN_LEFT) / nbPlayers * playerIter,
					40 * fmin(0, sin(blinkAmpl + M_PI_2 / 2 * playerIter)), 0);
				guiDrawQuad(UI_TROPHY_SMALL, UI_TROPHY_SMALL, 0, 1, 0, 1);
				glPopMatrix();
			}
			glPopMatrix();
		}

		glPopMatrix();

		// draw deck
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_BACKSIDE]);
		glColor4f(1, 1, 1, 1);
		glPushMatrix();
		glLoadIdentity();
		glTranslatef(
			UI_TILEDECK_X,
			WIN_HEIGHT - UI_BAR_HEIGHT + UI_TILEDECK_Y,
			0
		);
		size_t deckNbTiles = UI_TILEDECK_HEIGHT * tileDeckSize(tiledeck) / tileDeckCapacity(tiledeck) / UI_TILEDECK_MARGIN_Y;
		for(size_t ss = 0; ss < deckNbTiles; ss++) {
			glPushMatrix();
			glTranslatef(
				UI_TILEDECK_MARGIN_X * (ss % 2),
				UI_TILEDECK_MARGIN_Y * (deckNbTiles - ss),
				0
			);
			glScalef(UI_BACKSIDE_FACTOR, UI_BACKSIDE_FACTOR, 1);
			guiDrawQuad(
				UI_BACKSIDE_WIDTH,
				UI_BACKSIDE_HEIGHT,
				0, 1, 0, 1
			);
			glPopMatrix();
		}
		glPopMatrix();

		// Draw cursor
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_CURSOR]);
		glColor4f(1, 1, 1, 1);
		glPushMatrix();
		glLoadIdentity();
		glTranslatef(rawMouseX, rawMouseY, 0);
		guiDrawQuad(32, 32,
			(gamePhase == PHASE_TILE) ? 0 : 0.5,
			(gamePhase == PHASE_TILE) ? 0.5 : 1,
			0, 1
		);
		glPopMatrix();

		// Draw on screen!
		glFlush();
		SDL_GL_SwapBuffers();

		// Update kikoo
		blinkAmpl += BLINK_SPEED;
		blinkValue = fmin(BLINK_AMPL, BLINK_PRE_AMPL * (1 + cos(blinkAmpl)));

		// Computes FPS and wait if needed
		++frameCount;
		// if(frameCount % 100 == 0)
		//  fprintf(stderr, ".");

		endTimer = SDL_GetTicks();

		sprintf(fpsbuff, "Carcassonneirb  ★  %lu joueurs  ★  %.1f FPS",
			nbPlayers,
			(frameCount / (float)(endTimer - timeAtStart)) * 1000);
		SDL_WM_SetCaption(fpsbuff, NULL);

		deltaTime = endTimer - startTimer;

		if(deltaTime < (1000 / FRAMERATE))
			SDL_Delay((1000 / FRAMERATE) - deltaTime);
		//else { fprintf(stderr, "[WARNING] Cannot keep up the pace!\n"); }
	}

	// Cleanup
	guiResFreeFont(textFont);
	boardFree(board);
}


int
main(int argc, char const *argv[]) {
	if(argc != 3) {
		printf("Usage: %s <tile defs> <nb joueurs>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	int nb_players = atoi(argv[2]);
	if(!(NB_PLAYERS_MIN <= nb_players && nb_players <= NB_PLAYERS_MAX)) {
		printf("Le nombre de joueurs doit se situer entre %d et %d.\n",
			NB_PLAYERS_MIN, NB_PLAYERS_MAX);
		exit(2);
	}

	nbPlayers = (size_t)nb_players;
	for(int j = 0; j < NB_PLAYERS_MAX; j++){
		nbPoints[j] = 0;
		nbPawns[j] = 7;
		winners[j] = false;
	}

	screen = guiInit(WIN_WIDTH, WIN_HEIGHT);

	guiMain(argv[1]);

	return EXIT_SUCCESS;
}
