#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <glib.h>

#include <common.h>
#include <utils.h>
#include <tad/tile.h>
#include <tad/board.h>


/**
 * @brief  Obtient le pointeur vers la tuile d'index donné dans la map.
 * @param  Board* Plateau de jeu.
 * @param  int    Index dans la map.
 * @return Tile*  Pointeur vers la tuile d'index index dans la map.
 **/
static Tile* boardGetTileAtIndex(const Board*, int);

/**
 * @brief  Calcule l'index dans la map correspondant aux coordonnées (x,y).
 * @param  Board* Plateau de jeu.
 * @param  int    Abscisse des coordonnées à convertir.
 * @param  int    Ordonnée des coordonnées à convertir.
 * @return int    Index dans la map.
 **/
static int boardPosToIndex(const Board* b, int x, int y);


Board*
boardInit(size_t maxTiles) {
	Board* b = malloc(sizeof(Board));

	b->tileMap = g_hash_table_new(g_direct_hash, g_direct_equal);
	b->currentAvailablePos = g_hash_table_new(g_direct_hash, g_direct_equal);
	b->availablePos = g_hash_table_new(g_direct_hash, g_direct_equal);
	b->unfinishedAbbey = g_hash_table_new(g_direct_hash, g_direct_equal);
	b->pawnPositions = g_hash_table_new(g_direct_hash, g_direct_equal);

	b->maxTiles = maxTiles;

	return b;
}


static int
boardPosToIndex(const Board* b, int x, int y) {
	// X * M + Y where M = 2m and X = x + m, Y = y + m
	return (x + b->maxTiles) * (b->maxTiles * 2) + y + b->maxTiles;
}


void
boardIndexToPos(const Board* b, int index, int* x, int* y) {
	*x = index / 2 / b->maxTiles - b->maxTiles;
	*y = index % (2 * b->maxTiles) - b->maxTiles;
	assert(boardPosToIndex(b, *x, *y) == index);
}


Tile*
boardGetTileAtPos(const Board* b, int x, int y) {
	return boardGetTileAtIndex(b, boardPosToIndex(b, x, y));
}


unsigned int
boardPlayedTileCount(const Board* b) {
	unsigned int s = (unsigned int)g_hash_table_size(b->tileMap);
	return s;
}


bool
boardAddTile(Board* b, Tile* tile, int x, int y) {
	if(!boardPieceFits(b, x, y, tile))
		return false;

	tile->x = x;
	tile->y = y;

	int index = boardPosToIndex(b, x, y);
	gpointer key = GINT_TO_POINTER(index);

	g_hash_table_insert(b->tileMap, key, (gpointer)tile);
	assert(g_hash_table_lookup(b->tileMap, key) == tile);
	assert(boardGetTileAtIndex(b, index) == tile);

	// Supprime cette position des coups possibles (si nécessaire)
	g_hash_table_remove(b->availablePos, key);

	// Si la tuile contient une abbaye, on l'insère dans le tableau
	// des abbayes inachevées

	if(tileGetShape(tile)->type == TILE_ABBEY)
		g_hash_table_add(b->unfinishedAbbey, tile);

	boardUpdateAvailablePosAround(b, x, y);

	return true;
}


static Tile*
boardGetTileAtIndex(const Board* b, int index) {
	gpointer key = GINT_TO_POINTER(index);
	gpointer tileptr = g_hash_table_lookup(b->tileMap, key);

	if(tileptr == NULL)
		return NULL;

	return (Tile*) tileptr;
}


void
boardUpdateAvailablePosAround(Board* b, int x, int y) {
	int posTiles[TILE_NB_NEIGHBOURS];
	boardGetAdjacentTilesPos(posTiles, b, x, y);

	for(int i = 0; i < TILE_NB_NEIGHBOURS; i++) {
		// Si y'a pas de tuile à cette position
		if(boardGetTileAtIndex(b, posTiles[i]) == NULL) {
			g_hash_table_add(b->availablePos, GINT_TO_POINTER(posTiles[i]));
			assert(g_hash_table_lookup(b->availablePos, GINT_TO_POINTER(posTiles[i])));
		}
	}
}


bool
boardPositionIsAvailable(const Board* b, int x, int y) {
	if(boardPlayedTileCount(b) == 0) // first one
		return true;

	int index = boardPosToIndex(b, x, y);
	gpointer key = GINT_TO_POINTER(index);
	return (bool)g_hash_table_lookup(b->availablePos, key);
}


void
boardGetAdjacentTilesPos(int* posTiles, const Board* b, int x, int y) {
	posTiles[0] = boardPosToIndex(b, x    , y - 1); // north
	posTiles[1] = boardPosToIndex(b, x + 1, y    ); // east
	posTiles[2] = boardPosToIndex(b, x    , y + 1); // south
	posTiles[3] = boardPosToIndex(b, x - 1, y    ); // west
}


void
boardGetAdjacentTiles(Tile** ptTiles, const Board* b, Tile* tile) {
	int posTiles[TILE_NB_NEIGHBOURS];
	boardGetAdjacentTilesPos(posTiles, b, tile->x, tile->y);

	for(int i = 0; i < TILE_NB_NEIGHBOURS; i++)
		ptTiles[i] = boardGetTileAtIndex(b, posTiles[i]);
}


void
boardGetDiagonalTilesPos(int* posTiles, const Board* b, int x, int y) {
	posTiles[0] = boardPosToIndex(b, x + 1, y - 1); // northeast
	posTiles[1] = boardPosToIndex(b, x + 1, y + 1); // southeast
	posTiles[2] = boardPosToIndex(b, x - 1, y + 1); // southwest
	posTiles[3] = boardPosToIndex(b, x - 1, y - 1); // northwest
}


void
boardGetDiagonalTiles(Tile** ptTiles, const Board* b, Tile* tile) {
	int posTiles[TILE_NB_NEIGHBOURS];
	boardGetDiagonalTilesPos(posTiles, b, tile->x, tile->y);

	for(int i = 0; i < TILE_NB_NEIGHBOURS; i++)
		ptTiles[i] = boardGetTileAtIndex(b, posTiles[i]);
}


bool
boardPieceFits(const Board* b, int x, int y, Tile* tile) {
	if(!boardPositionIsAvailable(b, x, y))
		return false;

	tile->x = x;
	tile->y = y;

	Tile* adjacentTiles[TILE_NB_NEIGHBOURS];
	boardGetAdjacentTiles(adjacentTiles, b, tile);

	for(int i = 0; i < TILE_NB_NEIGHBOURS; i++) {
		if(!tileSideFits(tile, adjacentTiles[i], indexToSideGeo[i]))
			return false;
	}

	return true;
}


static int
priority(enum AREA_TYPE area) {
	switch(area) {
		case CASTLE:
		case FARM:
			return 1;

		case ROAD:
			return 3;

		default:
			fprintf(stderr, "[ERROR] Unknown AREA_TYPE: %d\n", area);
			exit(6);
	}
}


void
propag(const Board* b, Tile* tile, GHashTable* visited) {
	if(tile == NULL)
		return;

	enum CELL_GEO geo, oppositeGeo;
	TileCell* c[TILE_SIDE_NB_CELLS];
	TileCell* oc[TILE_SIDE_NB_CELLS];
	int i, j, dir;
	int test = 0;
	Tile* neighbours[TILE_NB_NEIGHBOURS];

	boardGetAdjacentTiles(neighbours, b, tile);

	for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++) {
		if(neighbours[dir] == NULL
			|| g_hash_table_lookup(visited, neighbours[dir]) != NULL)
			continue;

		geo = indexToSideGeo[dir];
		oppositeGeo = tileGetOppositeGeo(geo);

		if(priority(tileGetFieldForGeo(neighbours[dir], oppositeGeo)) == 1) {

			c[0] = tileGetCellForGeo(tile, geo);
			oc[0] = tileGetCellForGeo(neighbours[dir], oppositeGeo);
			for(i = 0; i < NB_PLAYERS_MAX; i++) {
				if(c[0]->playerPawnCount[i] <= oc[0]->playerPawnCount[i])
					continue;

				test++;
				oc[0]->playerPawnCount[i] = c[0]->playerPawnCount[i];
			}

			if(test != 0)
				propagIntern(neighbours[dir], oc[0]);

		} else {
			// Priorité = TILE_SIDE_NB_CELLS

			tileGetSideCellsForGeo(c, tile, geo, false);
			tileGetSideCellsForGeo(oc, neighbours[dir], oppositeGeo, true);

			for(i = 0; i < TILE_SIDE_NB_CELLS; i++) {
				if(neighbours[dir]->shape->type == TILE_ABBEY && i > 1)
					break;

				for(j = 0; j < NB_PLAYERS_MAX; j++) {
					if(c[i]->playerPawnCount[j] <= oc[i]->playerPawnCount[j])
						continue;

					oc[i]->playerPawnCount[j] = c[i]->playerPawnCount[j];
					test++;
				}

				if(test != 0)
					propagIntern(neighbours[dir], oc[i]);
			}
		}
	} // end for neighboors

	if(test == 0)
		return;

	for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir ++){
		if(neighbours[dir] == NULL
			|| g_hash_table_lookup(visited,neighbours[dir]) != NULL)
			continue;

		g_hash_table_add(visited, neighbours[dir]);
		propag(b,neighbours[dir],visited);
		g_hash_table_remove(visited,neighbours[dir]);
	}
}


/**
 * @brief  Met à jour les possibilités d'ajout pour chaque rotation d'une
 *         position donnée, en utilisant des flags.
 **/
static void
boardUpdateCurrentAvailablePosAux(gpointer index_, gpointer _, gpointer bat_) {
	int index = (int)GPOINTER_TO_INT(index_);
	UArgBaT* bat = (UArgBaT*) bat_;
	Board* board = bat->board;
	Tile* tile = bat->tile;

	int x, y;
	boardIndexToPos(board, index, &x, &y);

	int rot, flags = 0;
	for(rot = 0; rot < TILE_NB_NEIGHBOURS; rot++) {
		tileSetRotation(tile, rot, false); // absolute rotation
		if(boardPieceFits(board, x, y, tile))
			flags |= 1 << rot;
	}

	g_hash_table_replace(board->currentAvailablePos,
		GINT_TO_POINTER(index), GINT_TO_POINTER(flags));
}


void
boardUpdateCurrentAvailablePos(Board* board, Tile* currentTile) {
	g_hash_table_remove_all(board->currentAvailablePos);
	UArgBaT bat = {board, currentTile};

	// save rotation
	int rot_save = currentTile->rotation;
	g_hash_table_foreach(board->availablePos,
		(GHFunc)boardUpdateCurrentAvailablePosAux, (gpointer)&bat);
	// restore rotation
	tileSetRotation(currentTile, rot_save, false);
}


void
checkUnfinishedAbbey(Board* b, size_t* nbPoints, size_t* nbPawns, bool endGame) {
	GList* tile_list = g_hash_table_get_keys(b->unfinishedAbbey);
	Tile* tile;
	int j = 0;
	Tile* tilesSide[TILE_NB_NEIGHBOURS];
	Tile* tilesDiag[TILE_NB_NEIGHBOURS];

	GFOREACH(tile, tile_list) {
		j = 0;
		if(tile->playerOnSpecial == PLAYER_EMPTY)
			continue;

		boardGetAdjacentTiles(tilesSide, b, tile);
		boardGetDiagonalTiles(tilesDiag, b, tile);

		for(int i = 0 ; i < TILE_NB_NEIGHBOURS ; i++) {
			if(tilesSide[i] != NULL)
				j++;

			if(tilesDiag[i] != NULL)
				j++;
		}

		if(endGame)
			nbPoints[tile->playerOnSpecial] += j + 1;

		else if(j == CELL_GEO_LAST) {
			nbPoints[tile->playerOnSpecial] += 9;
			nbPawns[tile->playerOnSpecial] += 1;
			tile->playerOnSpecial = PLAYER_EMPTY;
			g_hash_table_remove(b->unfinishedAbbey, tile);
			g_hash_table_remove(b->pawnPositions, tile);
		}
	}

	g_list_free(tile_list);
}


void
propagIntern(Tile* tile, TileCell* c) {
	int position, i, j;

	for(i = 0; i < CELL_GEO_LAST; i++)
		if(tile->cells[i].position == c->position)
			position = i;

	for(i = 0; i < CELL_GEO_LAST; i++) {
		if(tile->cells[i].position == c->position) {
			continue;

		} else if(ugraphPathExists(tile->shape->connections, position, i)) {
			for(j = 0; j < NB_PLAYERS_MAX; j++)
				tile->cells[i].playerPawnCount[j] = c->playerPawnCount[j];

			tile->cells[i].cptPoint = c->cptPoint;

		} else {
			continue;
		}
	}
}


void
actuTile(const Board* b, Tile* tile) {
	enum CELL_GEO geo;
	enum CELL_GEO oppositeGeo;
	TileCell* c[TILE_SIDE_NB_CELLS];
	TileCell* oc[TILE_SIDE_NB_CELLS];
	int i, j, dir;

	Tile* neighbours[TILE_NB_NEIGHBOURS];

	boardGetAdjacentTiles(neighbours, b, tile);

	for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++) {
		if(neighbours[dir] != NULL) {
			geo = indexToSideGeo[dir];
			oppositeGeo = tileGetOppositeGeo(geo);

			if(priority(tileGetFieldForGeo(neighbours[dir], oppositeGeo)) == 1) {

				c[0] = tileGetCellForGeo(tile, geo);
				oc[0] = tileGetCellForGeo(neighbours[dir], oppositeGeo);

				for(i = 0; i < NB_PLAYERS_MAX; i++) {
					c[0]->playerPawnCount[i] += oc[0]->playerPawnCount[i];
				}

				c[0]->cptPoint += oc[0]->cptPoint;
				propagIntern(tile, c[0]);

			} else {

				tileGetSideCellsForGeo(c, tile, geo, false);
				tileGetSideCellsForGeo(oc, neighbours[dir], oppositeGeo, true);

				for(i = 0; i < TILE_SIDE_NB_CELLS; i++) {
					for(j = 0; j < NB_PLAYERS_MAX; j++) {
						c[i]->playerPawnCount[j] += oc[i]->playerPawnCount[j];
					}
					propagIntern(tile,c[i]);
				}

			} // endif priority cases
		}
	}
}


void
playerCalculatePoints(TileCell* cell, size_t* nbPawns, size_t* nbPoints, int points) {
	int i;
	int j[NB_PLAYERS_MAX];
	int max = 1;

	for(i = 0; i < NB_PLAYERS_MAX; i++)
		j[i] = -1;

	for(i = 0; i < NB_PLAYERS_MAX; i++) {
		if(cell->playerPawnCount[i] >= (unsigned int)max) {
			max = cell->playerPawnCount[i];
			j[i] = 1;
		}
	}

	for(i = 0; i < NB_PLAYERS_MAX; i++) {
		if(j[i] != -1) {
			nbPoints[i] += points;
			nbPawns[i] += max;
		}
	}
}


bool
fieldIsOver(Board* b, Tile* tile, int position, GHashTable* visited, int* points, bool suppPawn, int field) {
	enum CELL_GEO geo;
	enum CELL_GEO oppositeGeo;
	int dir;
	int i = 1, j;
	bool end = true;
	int indConnect[4] = {position, -1,-1,-1};

	g_hash_table_add(visited,tile);

	if(field == CASTLE) {
		*points += 2;
		if(tile->shape->bonus == 1)
			*points += 2;
	} else {
		*points += 1;
	}

	for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++) {
		if(dir == position)
			continue;

		if(tileGeoAreConnected(tile, indexToBothGeo[dir], indexToBothGeo[position])) {
			indConnect[i] = dir;
			if(field == CASTLE)
				i++;
		}
	}

	for(i = 0; i < TILE_NB_NEIGHBOURS; i++) {
		if(indConnect[i] == -1)
			continue;

		if(suppPawn) {
			if(tileGetCellForGeo(tile, indexToBothGeo[indConnect[i]])->hasPawnOfPlayer != PLAYER_EMPTY)
				g_hash_table_remove(b->pawnPositions, tile);

			tileGetCellForGeo(tile, indexToBothGeo[indConnect[i]])->hasPawnOfPlayer = PLAYER_EMPTY;

			for(j = 0; j < NB_PLAYERS_MAX; j++)
				tileGetCellForGeo(tile, indexToBothGeo[indConnect[i]])->playerPawnCount[j] = 0;

			if(field == CASTLE)
				suppressPawn(b, tile);
		}

		Tile* oppositeTile = tileGetOppositeTile(b, tile, indexToBothGeo[indConnect[i]]);

		if(g_hash_table_lookup(visited, oppositeTile))
			continue;

		if(oppositeTile == NULL)
			return false;

		geo = indexToBothGeo[indConnect[i]];
		oppositeGeo = tileGetOppositeGeo(geo);
		end = fieldIsOver(b,oppositeTile, oppositeGeo / 2, visited, points, suppPawn,field);

		if(!end)
			return false;
	}

	return end;
}


void
suppressPawn(Board* b, Tile* tile) {
	int i, j;

	for(i = 0; i < CELL_GEO_LAST; i++) {
		if(tileGetFieldForGeo(tile,i) == CASTLE && (i + 1) % 2 == 0) {
			g_hash_table_remove(b->pawnPositions, tile);
			tileGetCellForGeo(tile,i)->hasPawnOfPlayer = PLAYER_EMPTY;

			for(j = 0; j < NB_PLAYERS_MAX; j++)
				tileGetCellForGeo(tile,i)->playerPawnCount[j] = 0;
		}
	}
}


void
boardFree(Board* b) {
	g_hash_table_destroy(b->tileMap);
	g_hash_table_destroy(b->currentAvailablePos);
	g_hash_table_destroy(b->availablePos);
	g_hash_table_destroy(b->unfinishedAbbey);
	g_hash_table_destroy(b->pawnPositions);
	free(b);
}
