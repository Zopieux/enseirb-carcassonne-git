#include <stdbool.h>
#include <common.h>
#include <utils.h>
#include <tad/tile.h>
#include <tad/tiledeck.h>


TileDeck*
tileDeckInit(Tile* ptTiles, size_t size) {
	TileDeck* td = malloc(sizeof(TileDeck));

	td->stack = malloc(size * sizeof(Tile*));
	td->size = size;
	td->head = 0;

	for(size_t i = 0; i < size; i++)
		td->stack[i] = &ptTiles[i];

	return td;
}


size_t
tileDeckCapacity(const TileDeck* td) {
	return td->size;
}


size_t
tileDeckSize(const TileDeck* td) {
	return td->size - td->head - 1;
}


// Simple implémentation de l'algo. de Fish-Yates
void
tileDeckShuffle(TileDeck* td, size_t upTo) {
	size_t size = tileDeckSize(td);
	if(size <= 1 || size <= upTo + 1)
		return;

	size_t i, j;
	Tile* tmp;

	for(i = td->size - 1; i >= upTo + 1; i--) {
		j = randomSizet(upTo, i);
		tmp = td->stack[j];
		td->stack[j] = td->stack[i];
		td->stack[i] = tmp;
	}
}


Tile*
tileDeckPick(TileDeck* td) {
	td->head++;
	Tile* tile = tileDeckSeek(td);
	return tile;
}


Tile*
tileDeckSeek(const TileDeck* td) {
	return td->stack[td->head];
}


void
tileDeckFree(TileDeck* td) {
	free(td->stack);
	free(td);
}


bool
tileDeckEmpty(TileDeck* td){
  return tileDeckSize(td) == 0;
}
