#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <common.h>
#include <utils.h>
#include <tad/tile.h>
#include <tad/ugraph.h>
#include <tad/board.h>

// Arbitrary; a bit longer than necessary length (5)
#define _TILE_PARSE_LINE_LENGTH 20
#define _TILE_PARSE_ERROR_CODE 5
#define _DEFAULT_FIELD_FOR_CORNERS FARM


static int
tileGetGeoIndex(const Tile* tile, enum CELL_GEO geo) {
	return (geo + CELL_GEO_LAST - tile->rotation * 2) % CELL_GEO_LAST;
}


static enum CELL_GEO
geoFromChar(const char direction) {
	static const char parserGeoChars[] = "NnEeSsWw";

	char* found = strchr(parserGeoChars, direction);

	if(found == NULL) {
		fprintf(stderr, "Unknown direction code: %c\n", direction);
		exit(_TILE_PARSE_ERROR_CODE);
	}
	// Pointer arithmetic! Yay!
	return found - parserGeoChars;
}


static enum TILE_TYPE
tileTypeFromChar(const char type) {
	switch(type) {
		case 'N':
			return TILE_NORMAL;

		case 'A':
			return TILE_ABBEY;

		default:
			fprintf(stderr, "Unknown tile type: %c\n", type);
			exit(_TILE_PARSE_ERROR_CODE);
	}
}


static enum AREA_TYPE
areaTypeFromChar(const char type) {
	switch(type) {
		case 'F':
			return FARM;

		case 'C':
			return CASTLE;

		case 'R':
			return ROAD;

		default:
			fprintf(stderr, "Unknown area type: %c\n", type);
			exit(_TILE_PARSE_ERROR_CODE);
	}
}


static void
malformedAbort(const char step) {
	fprintf(stderr, "Tile definition file is malformed (parsing step: %d)\n",
		step);
	exit(_TILE_PARSE_ERROR_CODE);
}


Tile*
tileInit(Tile* tile, TileShape* shape) {
	size_t c, p;

	tile->shape = shape;
	tile->rotation = 0;
	tile->x = tile->y = 0;
	tile->playerOnSpecial = PLAYER_EMPTY;

	// Initialisation des cells
	for(c = 0; c < CELL_GEO_LAST; c++) {
		for(p = 0; p < NB_PLAYERS_MAX; p++)
			tile->cells[c].playerPawnCount[p] = 0;

		tile->cells[c].hasPawnOfPlayer = PLAYER_EMPTY;
		tile->cells[c].cptPoint = 1;
		tile->cells[c].position = c;
	}

	return tile;
}


TileShape*
tileGetShape(const Tile* tile) {
	return tile->shape;
}


/**
 * @brief  Remplace une ferme par une ville lorsque c'est nécessaire.
 * @param  TileShape*
 */
static void
shapeComputeCornerFields(TileShape* shape) {
	unsigned int i, j;
	bool edge_exists;
	enum AREA_TYPE geo_a, geo_b, geo_corner;

	for(i = 0; i < TILE_NB_NEIGHBOURS; i++) {
		j = (i + 1) % TILE_NB_NEIGHBOURS;

		geo_corner = shape->areas[indexToDiagGeo[i]];
		if(geo_corner != _DEFAULT_FIELD_FOR_CORNERS)
			continue;

		geo_a = shape->areas[indexToSideGeo[i]];
		geo_b = shape->areas[indexToSideGeo[j]];
		edge_exists = ugraphPathExists(shape->connections,
			indexToSideGeo[i], indexToSideGeo[j]);

		if(geo_a == CASTLE && geo_b == CASTLE && edge_exists)
			shape->areas[indexToDiagGeo[i]] = CASTLE;
	}
}


void
tileParseDef(FILE* file, size_t* ptShapeCount, size_t* ptTileCount, TileShape** ptShapes, Tile** ptTiles) {
	TileShape* shapes;
	Tile* tiles;

	size_t currentShapeId;
	size_t shapeCount, tileCount;
	size_t currentTileCount, currentConnCount, currentConn;
	unsigned int currentBonus;
	char currentAreaChars[TILE_NB_NEIGHBOURS], currentConnChars[_TILE_PARSE_LINE_LENGTH], currentTypeChar;

	size_t i, j;
	char line[_TILE_PARSE_LINE_LENGTH];

	size_t index;

	bool newlineFound = true;
	char step = 0;
	tileCount = 0;

	while(fgets(line, _TILE_PARSE_LINE_LENGTH, file) != NULL) {
		if(strchr(line, '\n') == NULL) {
			// discard lines where there is no \n (too long lines)
			newlineFound = false;
			continue;
		} else {
			if(!newlineFound) {
				// discard continuation of no-\n line.
				newlineFound = true;
				continue;
			}
			newlineFound = true;
		}

		if(strcmp(line, "\n") == 0 || line[0] == '#') {
			// skip empty lines and comments
			continue;
		}

		switch(step) {
			// first line: number of shapes
			case 0:
				if(sscanf(line, "%zu", &shapeCount) < 1)
					malformedAbort(step);

				shapes = malloc(shapeCount * sizeof(TileShape));

				currentShapeId = 0;
				step = 1;
				break;

			// first line of shape: number of tiles of this shape
			case 1:
				if(currentShapeId >= shapeCount) {
					// no more shapes to parse
					goto whilend;
				}

				if(sscanf(line, "%zu", &currentTileCount) < 1)
					malformedAbort(step);

				tileCount += currentTileCount;

				// it is garuanteed that size_t is an unsigned int
				shapes[currentShapeId].uid = currentShapeId;
				shapes[currentShapeId].tileCount = currentTileCount;
				step = 2;
				break;

			// shape type, shape bonus
			case 2:
				if(sscanf(line, "%c %u", &currentTypeChar, &currentBonus) < 2)
					malformedAbort(step);

				shapes[currentShapeId].type = tileTypeFromChar(currentTypeChar);
				shapes[currentShapeId].bonus = currentBonus;
				step = 3;
				break;

			// shape areas
			case 3:

				if(sscanf(line, "%4s", &currentAreaChars) < 1)
					malformedAbort(step);

				for(i = 0; i < TILE_NB_NEIGHBOURS; i++) {
					shapes[currentShapeId].areas[
						indexToSideGeo[i]
					] = areaTypeFromChar(currentAreaChars[i]);

					shapes[currentShapeId].areas[
						indexToDiagGeo[i]
					] = _DEFAULT_FIELD_FOR_CORNERS;
				}

				step = 4;
				break;

			// shape connection init (count)
			case 4:
				if(sscanf(line, "%zu", &currentConnCount) < 1)
					malformedAbort(step);

				shapes[currentShapeId].connections = ugraphInit(CELL_GEO_LAST);
				shapes[currentShapeId].farmAndCity = ugraphInit(CELL_GEO_LAST);

				if(currentConnCount == 0) {
					// current shape parsing is done; parse next shape!
					currentShapeId++;
					step = 1;
				} else {
					currentConn = 0;
					step = 5;
				}
				break;

			// shape connections
			case 5:
				if(sscanf(line, "%2s", &currentConnChars) < 1)
					malformedAbort(step);

				ugraphAddEdge(
					shapes[currentShapeId].connections,
					geoFromChar(currentConnChars[0]),
					geoFromChar(currentConnChars[1])
				);

				currentConn++;

				if(currentConn >= currentConnCount) {
					// current shape parsing is done; parse next shape!
					currentShapeId++;
					step = 1;
				}

				break;
		} // switch
	} // while

whilend:
	fprintf(stderr, "[INFO] Tile parsing done (got %lu shapes for %lu tiles).\n",
		shapeCount, tileCount);

	// now we instanciate each tile
	tiles = malloc(tileCount * sizeof(Tile));
	index = 0;

	for(i = 0; i < shapeCount; i++) {
		shapeComputeCornerFields(&shapes[i]);

		for(j = 0; j < shapes[i].tileCount; j++) {
			tileInit(&tiles[index], &shapes[i]);
			index++;
		}
	}

	*ptShapeCount = shapeCount;
	*ptTileCount = tileCount;
	*ptShapes = shapes;
	*ptTiles = tiles;
}


enum CELL_GEO
tileGetOppositeGeo(enum CELL_GEO geo) {
	// La fonction n'a de sens que pour les points cardinaux
	// (donc pour les geo pairs)
	if(geo % 2 == 0) {
		return (geo + 4) % 8;
	}
	return geo;
}


Tile * tileGetOppositeTile(Board* b, Tile* tile, enum CELL_GEO geo) {
	Tile* neighbours[TILE_NB_NEIGHBOURS];
	boardGetAdjacentTiles(neighbours, b, tile);
	/*
	neighbours est un tableau d'indices allant de 0 à 3, chacun
	correspondant à une direction cardinale ; dans CELL_GEO,
	on a aussi NORTHEAST, etc ; on peut diviser un  CELL_GEO
	par deux pour obtenir juste les directions cardinales.
	*/
	return neighbours[geo/2];
}


void
tileSetRotation(Tile* tile, int rot, bool relative) {
	tile->rotation = modulo(
		((relative) ? tile->rotation : 0) + rot,
		4
	);
}


enum AREA_TYPE
tileGetFieldForGeo(const Tile* tile, enum CELL_GEO geo) {
	return tileGetShape(tile)->areas[tileGetGeoIndex(tile, geo)];
}


TileCell*
tileGetCellForGeo(const Tile* tile, enum CELL_GEO geo) {
	return &(tile->cells[tileGetGeoIndex(tile, geo)]);
}


void
tileGetSideCellsForGeo(TileCell** cells, const Tile* opposite, enum CELL_GEO geo, bool mirrorCells) {
	cells[mirrorCells ? 2 : 0] = tileGetCellForGeo(
		opposite,
		modulo(geo - 1, CELL_GEO_LAST));

	cells[1] = tileGetCellForGeo(opposite, geo);

	cells[mirrorCells ? 0 : 2] = tileGetCellForGeo(
		opposite,
		modulo(geo + 1, CELL_GEO_LAST));
}


bool
tileSideFits(const Tile* first, const Tile* other, enum CELL_GEO geo) {
	if(first == NULL || other == NULL)
		return true;

	return \
		tileGetFieldForGeo(first, geo) == \
		tileGetFieldForGeo(other, tileGetOppositeGeo(geo));
}


bool
tileCanAddPawnAtGeo(Tile* tile, size_t nbPlayers, enum CELL_GEO geo) {
	if(geo == CELL_GEO_LAST)
		return \
			tileGetShape(tile)->type == TILE_ABBEY
			&& tile->playerOnSpecial == PLAYER_EMPTY;

	size_t playerIter;
	for(playerIter = 0; playerIter < nbPlayers; playerIter++) {
		if(tileGetCellForGeo(tile, geo)->playerPawnCount[playerIter] > 0)
			return false;
	}
	return true;
}


bool
tileCanAddPawn(Tile* tile, size_t nbPlayers) {
	enum CELL_GEO geoIter;
	for(geoIter = 0; geoIter <= CELL_GEO_LAST; geoIter++) {
		if(tileCanAddPawnAtGeo(tile, nbPlayers, geoIter))
			return true;
	}
	return false;
}


bool
tileGeoAreConnected(Tile* tile, enum CELL_GEO a, enum CELL_GEO b) {
	return ugraphPathExists(
		tileGetShape(tile)->connections,
		tileGetGeoIndex(tile, a),
		tileGetGeoIndex(tile, b)
	);
}


bool
tileGeoTabConnections(Tile* tile, enum CELL_GEO tabGeo[TILE_NB_NEIGHBOURS], enum CELL_GEO geo) {
	int dir;
	for(dir = 0; dir < TILE_NB_NEIGHBOURS; dir++) {
		if(tabGeo[dir] == geo)
			continue;

		if(tileGeoAreConnected(tile,tabGeo[dir], geo))
			return true;
	}
	return false;
}
