#include <assert.h>
#include <stdlib.h>
#include <common.h>
#include <colors.h>
#include <tad/tile.h>
#include <tad/player.h>

#define PLAYER_COL_SAT 99
#define PLAYER_COL_SAT_TEXT 80
#define PLAYER_COL_LEVEL 70
#define PLAYER_COL_LEVEL_TEXT 60

static const char* playerNames[NB_PLAYERS_MAX] = {
	"Myriam", "Denis", "David", "Georges", "Lena", "Justin"
};

static const unsigned int playerColors[NB_PLAYERS_MAX] = {
	0, 60, 120, 180, 240, 300
};


static void
convertColor(size_t playerId, unsigned int sat, unsigned int level, float* r, float* g, float *b) {
	unsigned int hue = playerColors[playerId];
	unsigned int color = colorHSLtoRGB(hue, sat, level);
	*r = colorGetRValue(color) / 255.0;
	*g = colorGetGValue(color) / 255.0;
	*b = colorGetBValue(color) / 255.0;
}


char*
playerGetName(size_t playerId) {
	assert(playerId < NB_PLAYERS_MAX);
	return playerNames[playerId];
}


void
playerGetColor(size_t playerId, float* r, float* g, float* b) {
	assert(playerId < NB_PLAYERS_MAX);
	convertColor(playerId, PLAYER_COL_SAT, PLAYER_COL_LEVEL, r, g, b);
}


void
playerGetTextColor(size_t playerId, float* r, float* g, float* b) {
	assert(playerId < NB_PLAYERS_MAX);
	convertColor(playerId, PLAYER_COL_SAT_TEXT, PLAYER_COL_LEVEL_TEXT, r, g, b);
}

