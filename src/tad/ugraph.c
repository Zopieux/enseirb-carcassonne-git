#include <assert.h>
#include <stdbool.h>
#include <glib.h>

#include <tad/ugraph.h>


/**
 * @brief  S'assure que le premier paramètre est inférieur au second. Inverse
 *         par effet de bord si nécessaire.
 * @param  Vertex* Pointeur vers un premier sommet.
 * @param  Vertex* Pointeur vers un second sommet.
 */
static void
ensure_lower(Vertex* pta, Vertex* ptb) {
	if(*pta > *ptb) {
		int tmp;
		tmp = *pta;
		*pta = *ptb;
		*ptb = tmp;
	}
}


/**
 * @brief  S'assure que les sommets donnés sont cohérents avec le graphe et
 *         applique `ensure_lower`. Crashe si ce n'est pas le cas.
 * @param  UGraph* Le graphe concerné.
 * @param  Vertex* Pointeur vers un premier sommet.
 * @param  Vertex* Pointeur vers un second sommet.
 */
static void
check_integrity(UGraph* g, Vertex* pta, Vertex* ptb) {
	assert(*pta < g->vertexCount);
	assert(*ptb < g->vertexCount);
	ensure_lower(pta, ptb);
}


/**
 * @brief  Calcule l'index représentatif de l'arête entre deux sommets.
 * @param  UGraph* Le graphe concerné.
 * @param  Vertex  Un premier sommet.
 * @param  Vertex  Un second sommet.
 */
static int
array_index(UGraph* g, Vertex a, Vertex b) {
	// return a * g->vertexCount + b;
	return b + a * (g->vertexCount - 1) - a * (a - 1) / 2;
}


UGraph*
ugraphInit(size_t vertexCount) {
	// Sum of k for for k in [1, vertexCount]
	unsigned int arraySize = vertexCount * (vertexCount + 1) / 2;
	bool* edgeMatrix = malloc(arraySize * sizeof(bool));
	for(Vertex i = 0; i < arraySize; i++)
		edgeMatrix[i] = false;

	UGraph* g = malloc(sizeof(UGraph));
	g->edgeMatrix = edgeMatrix;
	g->vertexCount = vertexCount;
	return g;
}


size_t
ugraphSize(UGraph* g) {
	return g->vertexCount;
}


void
ugraphFree(UGraph* g) {
	free(g);
}


void
ugraphAddEdge(UGraph* g, Vertex a, Vertex b) {
	check_integrity(g, &a, &b);
	g->edgeMatrix[array_index(g, a, b)] = true;
}


void
ugraphRemoveEdge(UGraph* g, Vertex a, Vertex b) {
	check_integrity(g, &a, &b);
	g->edgeMatrix[array_index(g, a, b)] = false;
}


bool
ugraphEdgeExists(UGraph* g, Vertex a, Vertex b) {
	check_integrity(g, &a, &b);
	return g->edgeMatrix[array_index(g, a, b)];
}


void
ugraphConnectAll(UGraph* g) {
	for(Vertex i = 0; i < g->vertexCount; i++)
		for(Vertex j = i; j < g->vertexCount; j++)
			ugraphAddEdge(g, i, j);
}


void
ugraphDisconnectAll(UGraph* g) {
	for(Vertex i = 0; i < g->vertexCount; i++)
		for(Vertex j = i; j < g->vertexCount; j++)
			ugraphRemoveEdge(g, i, j);
}


void
ugraphConnectVertex(UGraph* g, Vertex v) {
	for(Vertex i = 0; i < g->vertexCount; i++)
		ugraphAddEdge(g, i, v);
}


void
ugraphDisconnectVertex(UGraph* g, Vertex v) {
	for(Vertex i = 0; i < g->vertexCount; i++)
		ugraphRemoveEdge(g, i, v);
}


bool
ugraphPathExists(UGraph* g, Vertex from, Vertex to) {
	if(ugraphEdgeExists(g, from, to))
		return true;

	Vertex v, c;
	gpointer \
		ptr_from = GINT_TO_POINTER(from),
		ptr_c;
	GQueue* waiting = g_queue_new();
	GHashTable* visited = g_hash_table_new(g_direct_hash, g_direct_equal);

	g_queue_push_head(waiting, ptr_from);
	g_hash_table_add(visited, ptr_from);

	while(!g_queue_is_empty(waiting)) {
		v = GPOINTER_TO_INT(g_queue_pop_head(waiting));
		if(v == to) {
			g_queue_free(waiting);
			g_hash_table_destroy(visited);
			return true;
		}

		for(c = 0; c < g->vertexCount; c++) {
			ptr_c = GINT_TO_POINTER(c);

			if(ugraphEdgeExists(g, c, v) && !g_hash_table_contains(visited, ptr_c)) {
				g_queue_push_head(waiting, ptr_c);
				g_hash_table_add(visited, ptr_c);
			}
		}
	}

	g_queue_free(waiting);
	g_hash_table_destroy(visited);
	return false;
}
