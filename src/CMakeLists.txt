cmake_minimum_required(VERSION 2.6)
project(carcassonne_base)

# Directory for library header files.
include_directories("${PROJECT_SOURCE_DIR}/../headers")

find_package(SDL REQUIRED)
find_package(SDL_image REQUIRED)
find_package(SDL_ttf REQUIRED)
find_package(OpenGL REQUIRED)
find_package(Glib2 REQUIRED)

include_directories(
    ${OPENGL_INCLUDE_DIRS}
    ${SDL_INCLUDE_DIRS}
    ${GLIB2_INCLUDE_DIRS}
)

add_library(carcassonne_base utils.c colors.c)

add_executable(carcassonne gui/main.c)

target_link_libraries(carcassonne_base
    m
)

target_link_libraries(carcassonne
    m
    carcassonne_base
    carcassonne_tad
    carcassonne_gui
    ${GLIB2_LIBRARIES}
        ${SDL_LIBRARY} ${SDLIMAGE_LIBRARY} 
        ${OPENGL_LIBRARIES}
	${SDLTTF_LIBRARY}

)
